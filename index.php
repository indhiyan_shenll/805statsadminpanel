<?php 
include_once('session_check.php'); 
include_once('header.php'); 

$AlertMessage = '';
$AlertClass = '';
$AlertFlag = false;
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 1) {
        $AlertMessage = "You have successfully logged in";
        $AlertClass = "alert-success";
        $AlertFlag = true;
    }
}

?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <?php if ($AlertFlag == true) { ?>
            <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $AlertMessage; ?> </p>
            </div>
            <?php } ?>

            <!-- <h1 class="page-title"> Fluid Page Layout
                <small>boxed page layout</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Page Layouts</span>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">
                                    <i class="icon-bell"></i> Action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-shield"></i> Another action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i> Something else here</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bag"></i> Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> -->
            <!-- END PAGE HEADER-->
            <div class="note note-info">
                <p> Welcome To 805 STATS Admin-Panel ! </p>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>
   
<?php
include_once('session_check.php');
include_once('connect.php');
error_reporting(E_ALL);


if(isset($_GET['seasonnamenew']) && !empty($_GET['seasonnamenew'])){		
	$test = '';
	$SeasonArr  = array();
	$seasonnamenew  = $_GET['seasonnamenew'];	
	$existsseason   = $_GET['existsseason'];

	$createdate  = date('Y-m-d H:i:s');
	$seasonorder = 1;
	$stmtExe		 = $conn->prepare("SELECT MAX( season_order ) as seasonorder FROM customer_season order by season_order desc");
    $stmtExe->execute();
	$QryCntconf = $stmtExe->rowCount();
									
	if ($QryCntconf > 0) {
		$rowMaxId = $stmtExe->fetch(PDO::FETCH_ASSOC);		
		$seasonorder = $rowMaxId['seasonorder'];
	}
	

	$stmt		 = $conn->prepare("INSERT INTO customer_season (custid, name,season_order)  VALUES (:customer_id, :name, :season_order)");
    $stmt->execute(array(':customer_id' => $customerid, ':name' => $seasonnamenew,":season_order"=>$seasonorder));	
	$SeasonId = $conn->lastInsertId();

	if(isset($_GET['season'])){
		if(is_array($_GET['season'])){
			$SeasonArr  = array_filter($_GET['season']);
		}
	}
	
	if(count($SeasonArr)>0){
		foreach($SeasonArr as $Key=>$Value){
			
			if(is_array($Value)){		
				$ConfArr  = array_values( $Value);
				
				$ConfArrVal = explode("###",$ConfArr[0]);				
				$ConferenId  = $ConfArrVal[1];
		
				$stmt1		 = $conn->prepare("INSERT INTO customer_season_conference (season_id, conference_id,customer_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:status,:created_date)");
				$stmt1->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' => $customerid,':status' => 1, ':created_date'=>$createdate));

				foreach($Value as $Key1=>$Value1){
					$DivisionArr = explode("###",$Value1);
					$DivisionId  = $DivisionArr[0];
					$ConferenId  = $DivisionArr[1];					

					$stmt2		 = $conn->prepare("INSERT INTO customer_conference_division (season_id, conference_id,customer_id,division_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:division_id,:status,:created_date)");
					$stmt2->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' => $customerid,':division_id'=> $DivisionId,':status' =>1, ':created_date'=>$createdate));
					
					if(!empty($existsseason)){
						$stmt3		 = $conn->prepare("INSERT INTO customer_division_team (customer_id,season_id,conference_id,division_id,team_id,team_order,status,created_date,modified_date) SELECT customer_id,$SeasonId,conference_id,division_id,team_id,team_order,status,now(),now() from customer_division_team where customer_id=:customer_id and season_id=:season_id and conference_id=:conference_id and division_id=:division_id");			
						
						$QryCond  = array(':customer_id'=>$customerid, ':season_id'=>$existsseason,':conference_id'=>$ConferenId,':division_id'=> $DivisionId);
						
						$stmt4		 = $conn->prepare("INSERT INTO customer_team_player (customer_id,season_id,conference_id,division_id,team_id,player_id,player_order,status,created_date,modified_date,isdelete) SELECT customer_id,$SeasonId,conference_id,division_id,team_id,player_id,player_order,status,now(),now(),isdelete from customer_team_player where customer_id=:customer_id and season_id=:season_id and conference_id=:conference_id and division_id=:division_id");				

						$stmt3->execute($QryCond);
						$stmt4->execute($QryCond);
					}
				}

			}else{			
				$ConferenceArr = explode("###",$Value);
				$ConferenId    = $ConferenceArr[0];
					
				$stmt3		   = $conn->prepare("INSERT INTO customer_season_conference (season_id, conference_id,customer_id,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:status,:created_date)");
				$stmt3->execute(array(':season_id'=>$SeasonId,':conference_id'=>$ConferenId,':customer_id' =>$customerid,':status'=> 1,':created_date'=>$createdate));						
			}
		}
	}	

	$Qry		= $conn->prepare("select * from customer_season where custid=:custid ORDER BY id desc");
	$Qryarr		= array(":custid"=>$customerid);
	$Qry->execute($Qryarr);
	$QryCntSeason = $Qry->rowCount();

	$DivisionWrapHtml= $AddNewSeasonTree='';
	$Inc =0;
	if ($QryCntSeason > 0) {
		while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){
			
			$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
			$Qryarr = array(":season_id"=>$row['id']);
			$QryExe->execute($Qryarr);
			$QryCntSeasonconf	= $QryExe->rowCount();											
			$SeletedArrConf		= array();
			$SeletedArrDiv		= array();
			$AddNewSeason = $AddSeasontbl = $Conferencetbl= '';
			
			if ($QryCntSeasonconf > 0) {
				while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
					$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
					$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
					$QryExeDiv->execute($QryarrCon);
					$QryCntSeasonconf = $QryExeDiv->rowCount();
					$Divisiontbl = $AddNewSeason='';
					while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){													
						$Selected = ($rowSeasonDiv['status'])?'checked':'';
						$Divisiontbl .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' data-divisionid='".$rowSeasonDiv['id']."' data-conferenceid='".$rowSeason['id']."'><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."&conferenceid=".$rowSeason['id']."&seasonid=".$row['id']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table>	";	
						$SeletedArrDiv[] = $rowSeasonDiv['id'];
						
						$AddNewSeason .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label></td></tr></table>";	
					}

					$Selected = ($rowSeason['status'])?'checked':'';
					$Conferencetbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division' data-toggle='modal' data-target='#managedivModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='fa fa-plus'></i></a></td></tr></table>".$Divisiontbl;	

					$AddSeasontbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label></td></tr></table>".$AddNewSeason;	

					$SeletedArrConf[] = $rowSeason['id'];

					$QryExeDivMan		= $conn->prepare("SELECT * FROM customer_division where custid=:customer_id");
					$Qryarr		= array(":customer_id"=>$customerid);
					$QryExeDivMan->execute($Qryarr);
					$QryCntDiv = $QryExeDivMan->rowCount();
					$divisions='';
					if ($QryCntDiv > 0) {
						while ($rowDivns = $QryExeDivMan->fetch(PDO::FETCH_ASSOC)){
							$DivId = $rowDivns['id'];
							$Selectedchk  = (in_array($DivId,$SeletedArrDiv))?"checked":"";
							$divisions .="<p class='managepopuplistDivision'><label class='mt-checkbox'><input type='checkbox' name='divisionlist[]' value='$DivId' class='divisionlistchk' $Selectedchk>".$rowDivns['name']."<span></span></label></p>";
						}
					}
					$DivisionWrapHtml .= '<div id="divisionwrap_'.$rowSeason['conference_id'].'" class="hide">'.$divisions.'</div>';
					$SeletedArrDiv		= array();
				}				
			}			

			$QryExeManage		= $conn->prepare("SELECT * FROM customer_conference where customer_id=:customer_id");
			$Qryarr		= array(":customer_id"=>$customerid);
			$QryExeManage->execute($Qryarr);
			$QryCntconf = $QryExeManage->rowCount();
			$conference='';
			if ($QryCntconf > 0) {
				while ($rowConfs = $QryExeManage->fetch(PDO::FETCH_ASSOC)){
					$ConfId = $rowConfs['id'];
					$Selectedchk  = (in_array($ConfId,$SeletedArrConf))?"checked":"";
					$conference .="<p class='managepopuplist'><label class='mt-checkbox'><input type='checkbox' name='conferencelist[]' value='$ConfId' $Selectedchk>".$rowConfs['conference_name']."<span></span></label></p>";
				}
			}

			$SeasonTree = "<table class='table  ms_seasontble'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."'> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;' data-toggle='modal' data-target='#ManageConferenceModal' data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a>$Conferencetbl</td></tr></table>";

			$AddNewSeasonTree .= "<div class='mt-radio-list clearfix'><label class='mt-radio'><input type='radio' name='seasonnamesnewid' value='".$row['id']."' class='seasonnamesnew'>".$row['name']."<span></span></label></div><table class='table  ms_seasontble popupmodelseason'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='seasonnames[]' value='".$row['id']."'> ".$row['name']."<span></span></label>$AddSeasontbl</td></tr></table>";		
			
			if($Inc==0){				
				$DispCond    = "";
			}else{				
				$DispCond    = "display:none;";
			}
			echo '<div class="col-md-12 col-sm-12 seasonwrapcont" id="seasonmaincont_'.$row['id'].'"><div class="portlet box grey seasontbltogglewrap">
				<div class="portlet-title">
					<div class="caption tools" style="width: 98%;">
						<a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> '.$row['name'].'</a>
					</div>												
					<div class="tools">
						<a href="javascript:;" class="expand" style=""></a>
					</div>
				</div>
				<div class="portlet-body seasontbltoggle" style="'.$DispCond.'">'.$SeasonTree.'	
				<div id="conferewrap_'.$row['id'].'" class="hide">'.$conference.'</div>'.$DivisionWrapHtml.'
				</div>
			</div></div>';
			$conference='';
			$Inc++;
		} 				
	}
	exit;
}

?>	

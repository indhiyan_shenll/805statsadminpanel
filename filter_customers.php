<?php 
include("connect.php");
if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	$HdnMode=$_REQUEST["HdnMode"];
	$HdnPage=$_REQUEST["HdnPage"];
	//$Page=$HdnMode;
	//$Page=$HdnPage;
	$Page=1;
}

$HiddenSearch =  (isset($_REQUEST['searchbyorganization'])) ? $_REQUEST['searchbyorganization'] : "" ;

?>
<div class="table-responsive">
<form id="customer_list" name="customer_list" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $HiddenSearch; ?>">

<!-- <table class="table table-striped table-hover customerlist-tbl " id="sample_1"> -->
<table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_5" sytle="border: 1px solid #CCC;border-collapse: collapse;">
<thead>
	<tr>
		<th nowrap> Customer&nbsp;Id&nbsp; </th>
        <th nowrap> Organization&nbsp;Name&nbsp; </th>
        <th nowrap> Contact&nbsp;name&nbsp; </th>
        <th nowrap> Organization&nbsp;Type </th>
        <th nowrap> Website&nbsp;URL </th>
        <th nowrap> Scoring&nbsp;platform&nbsp; </th>
        <th nowrap> Site&nbsp;template&nbsp; </th>
        <th nowrap> Livescoring&nbsp;Template&nbsp; </th>
        <th nowrap> Renewal&nbsp;date&nbsp; </th>
        <th nowrap> Actions </th>
	</tr>
</thead>
<tbody>
<?php

if(isset($_REQUEST['searchbyorganization']))
{
	$OrganizationName     =  $_REQUEST['searchbyorganization'];

	$res = '';
	if($OrganizationName != ''){	
		$res = "select * from customer_info where name like '%$OrganizationName%' and user_type='user'";
	} else {
		// $res = "select * from customer_info where user_type='user'";
		$res = "select * from customer_info where user_type='user'";
	}
    
    $getResQry      =   $conn->prepare($res);
    $QryArr = array(":organizationame"=>$OrganizationName, ":user_type"=>"user");
    $getResQry->execute($QryArr);
    $getResCnt      =   $getResQry->rowCount();
    $getResQry->closeCursor();
    	$TotalPages = '';
	    if($getResCnt>0){
	        $TotalPages=ceil($getResCnt/$RecordsPerPage);
	        $Start=($Page-1)*$RecordsPerPage;
	        $sno=$Start+1;
	            
	        $res.=" limit $Start,$RecordsPerPage";
	                
	        $getResQry      =   $conn->prepare($res);
	        $getResQry->execute($QryArr);
	        $getResCnt      =   $getResQry->rowCount();
	    if($getResCnt>0){
	        $getResRows     =   $getResQry->fetchAll();
	        $getResQry->closeCursor();
	        $s=1;
	        // print_r($getResRows );
        foreach($getResRows as $customer){
		?>
			<tr>
                <td nowrap><?php echo $customer['id'] ?></td>
                <td nowrap><?php echo $customer['name'] ?></td>
                <td nowrap><?php echo $customer['contact_name'] ?></td>
                <td nowrap>
                <?php 
                $orgid= $customer['customer_type'];
                $orgQry = $conn->prepare("select * from customer_organization_type where id=:orgid");        
                $QryArr = array(":orgid"=>$orgid);        
                $orgQry->execute($QryArr);                                        
                $org_type = $orgQry->fetch(PDO::FETCH_ASSOC);
                echo $org_type['name'];
                ?></td>
                <td nowrap><?php echo $customer['site_url'] ?></td>
                <td nowrap><?php echo $customer['scoring_platform'] ?></td>
                <td nowrap><?php echo $customer['site_template'] ?></td>
                <td nowrap><?php echo $customer['livescoring_template'] ?></td>
                <td nowrap><?php echo $customer['renewal_date'] ?></td>
                <td>
                    <table>
                        <tr>
                            <td nowrap>
                            <a href="addcustomer.php?id=<?php echo base64_encode($customer['id']); ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Edit
                                
                            </a>
                            </td>
                            <td nowrap>   
                            <a href="#myModalDelete" id="openBtn" weburl="<?php echo $customer['site_url']; ?>" customerid="<?php echo $customer['id']; ?>" data-toggle="modal" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete
                                
                            </a>                                                   
                            
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

		<?php
		$s++;
		}
	} 
    // else{
    //         echo "<tr><td colspan='10' style='text-align:center;'>No Customer(s) found.</td></tr>";
    //     }
    }
    // else{
    //     echo "<tr><td  colspan='10' style='text-align:center;'>No Customer(s) found.</td></tr>";
    // }
	
}?>
 </tbody>
 </table>
<?php
	if($TotalPages > 1){

	echo "<tr><td style='text-align:center;' colspan='10' valign='middle' class='pagination'>";
	$FormName = "customer_list";
	require_once ("paging.php");
	echo "</td></tr>";

	}
?>
</form>
</div>
<script src="assets/custom/js/addcustomer.js" ></script>

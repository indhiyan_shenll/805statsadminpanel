<?php
include_once('session_check.php');
include_once('connect.php');
error_reporting(0);

if(isset($_GET['adddivisionnew']) && !empty($_GET['adddivisionnew'])){	
	$conferenceId= $_GET['conferenceid'];
	$division    = $_GET['division'];
	$SeasonId    = $_GET['seasonid'];
	$divisionrule= $_GET['divisionrule'];
	$Status		 = $_GET['activedivision'];
	$createdate  = date('Y-m-d H:i:s');
	$sql		= "select * from customer_division_rule where id='$divisionrule'";			
	$stmt = $conn->prepare($sql); 
	$stmt->execute();
	$QryCntRuleRow = $stmt->fetch();
	//print_r($QryCntRuleRow);
	$rulelist=$QryCntRuleRow['name'];
	$con_division_rulelist=$division. ' - ' .$rulelist;
   	$stmt		 = $conn->prepare("INSERT INTO customer_division (custid, name) 
    VALUES (:customer_id, :division_name)");
    $stmt->execute(array(':customer_id' => $customerid, ':division_name' => $con_division_rulelist));
	
	$divisionId = $conn->lastInsertId();

	$stmt2		 = $conn->prepare("INSERT INTO customer_conference_division (season_id, conference_id,customer_id,division_id,division_ruleid,status,created_date) VALUES (:season_id,:conference_id, :customer_id,:division_id,:division_ruleid,:status,:created_date)");

    $stmt2->execute(array(':season_id'=>$SeasonId,':conference_id'=>$conferenceId,':customer_id' => $customerid,':division_id'=> $divisionId,':division_ruleid'=>$divisionrule, ':status' => $Status, ':created_date'=>$createdate));
	
								
	$Qry		= $conn->prepare("select * from customer_season where custid=:custid and id=:season_id");
	$Qryarr		= array(":custid"=>$customerid,":season_id"=>$SeasonId);
	$Qry->execute($Qryarr);
	$QryCntSeason = $Qry->rowCount();
	$DivisionWrapHtml='';
	$Inc =0;
	if ($QryCntSeason > 0) {
		while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){

			$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
			$Qryarr = array(":season_id"=>$row['id']);
			$QryExe->execute($Qryarr);
			$QryCntSeasonconf	= $QryExe->rowCount();
			$Conferencetbl		= '';
			$SeletedArrConf		= array();
			$SeletedArrDiv		= array();

			if ($QryCntSeasonconf > 0) {
				while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
					$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
					$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
					$QryExeDiv->execute($QryarrCon);
					$QryCntSeasonconf = $QryExeDiv->rowCount();
					$Divisiontbl ='';
					while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){													
						$Selected = ($rowSeasonDiv['status'])?'checked':'';
						$Divisiontbl .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' data-divisionid='".$rowSeasonDiv['id']."' data-conferenceid='".$rowSeason['id']."'><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table>	";	
						$SeletedArrDiv[] = $rowSeasonDiv['id'];

					}				

					$Selected = ($rowSeason['status'])?'checked':'';
					$Conferencetbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division' data-toggle='modal' data-target='#managedivModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='fa fa-plus'></i></a></td></tr></table>".$Divisiontbl;	

					$SeletedArrConf[] = $rowSeason['id'];

					$QryExeDivMan		= $conn->prepare("SELECT * FROM customer_division where custid=:customer_id");
					$Qryarr		= array(":customer_id"=>$customerid);
					$QryExeDivMan->execute($Qryarr);
					$QryCntDiv = $QryExeDivMan->rowCount();
					$divisions='';
					if ($QryCntDiv > 0) {
						while ($rowDivns = $QryExeDivMan->fetch(PDO::FETCH_ASSOC)){
							$DivId = $rowDivns['id'];
							$Selectedchk  = (in_array($DivId,$SeletedArrDiv))?"checked":"";
							$divisions .="<p class='managepopuplistDivision'><label class='mt-checkbox'><input type='checkbox' name='divisionlist[]' class='divisionlistchk' value='$DivId' $Selectedchk>".$rowDivns['name']."<span></span></label></p>";
						}
					}
					$DivisionWrapHtml .= '<div id="divisionwrap_'.$rowSeason['conference_id'].'" class="hide">'.$divisions.'</div>';
					$SeletedArrDiv		= array();
				}										
				
			}
			
			$QryExeManage		= $conn->prepare("SELECT * FROM customer_conference where customer_id=:customer_id");
			$Qryarr		= array(":customer_id"=>$customerid);
			$QryExeManage->execute($Qryarr);
			$QryCntconf = $QryExeManage->rowCount();
			$conference='';
			if ($QryCntconf > 0) {
				while ($rowConfs = $QryExeManage->fetch(PDO::FETCH_ASSOC)){
					$ConfId = $rowConfs['id'];
					$Selectedchk  = (in_array($ConfId,$SeletedArrConf))?"checked":"";
					$conference .="<p class='managepopuplist'><label class='mt-checkbox'><input type='checkbox' name='conferencelist[]' value='$ConfId' $Selectedchk>".$rowConfs['conference_name']."<span></span></label></p>";
				}
			}

			$SeasonTree = "<table class='table  ms_seasontble'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."'> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;' data-toggle='modal' data-target='#ManageConferenceModal' data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a>$Conferencetbl</td></tr></table>";
		

			echo '<div class="portlet box grey seasontbltogglewrap">
				<div class="portlet-title">
					<div class="caption">
						'.$row['name'].'
					</div>												
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					</div>
				</div>
				<div class="portlet-body seasontbltoggle">'.$SeasonTree.'	
				<div id="conferewrap_'.$row['id'].'" class="hide">'.$conference.'</div>'.$DivisionWrapHtml.'
				</div>
			</div>';
			$conference='';
			$Inc++;
		} 									
	}							   
	exit;						
}

?>
<?php
include_once("connect.php");
include_once('session_check.php');
include_once('usertype_check.php');  
include_once('header.php');
if (isset($_GET['sport'])) {
    $SportName = $_REQUEST['sport'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
           $SportId = $QrySportVal['sportcode'];
        }
    }    
}


include('gameposition.php');
?>
<link href="assets/custom/css/addbulkentrygame.css" rel="stylesheet" type="text/css">
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTENT -->
    <input type="hidden" id="sportid" value="<?php echo $SportId; ?>" name="sportid">
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER--> 
			<div id="addplayermaincont">

				<div id="addmoreplayercont">
				<?php
					echo $signlegameformentry;
				?>
				</div>
				<div class="col-md-12 left-right-padding">
                    <div class="portlet light info-caption">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <i class="icon-settings font-red-sunglo"></i>
                                <span class="caption-subject bold uppercase"> Game Information</span>
                            </div>
                        </div>
                    </div>
                </div>

	            <div class="col-md-12 bulkplayerparent">
					<div id="bultplayentrycont" class="col-md-12 col-sm-12 col-xs-12 bulkplayerupload">	

						<?php
							for($i=1;$i<=1;$i++){			
								echo $signlegameformentry;	
							}
						?>
						
					</div>
					<div class="addmorebtncont">
						<div class="pull-right">
							<button type="button" class="btn btn-danger deleteplayerbtn customredbtn">Remove</button>
							<button type="button" class="btn btn-success addmoreplayerbtn customgreenbtn">Add more</button>
						</div>
					</div>
					<div class="submitbtncont">
						<div class="pull-left">			
							<button type="button" class="btn btn-danger customredbtn" onclick="window.location='game_list.php'">Back</button>
							<button type="button" class="btn btn-success submitallplayerbtn customgreenbtn">Submit</button>
						</div>
					</div>
				</div>

				<div id="myModal" class="modal fade updateplayerform" role="dialog" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Update Player Information</h4>
					  </div>
					  <div class="modal-body" style="text-align:center;">
						<div class="loadingimgcont">
							<img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg">
							<div class="alert alert-success" id="updatemsg" style="display: block;">Details updated successfully</div>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>

				  </div>
				</div>
			</div>
			<!-- end #content -->
		</div>
	</div>
</div>
<div class="modal fade " id="SeasonModal" role="dialog">
    <div class="modal-dialog">                                                    
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >Add new season</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">    
                    <label for="addssntext">Enter New Season</label>
                    <input type="text" id="addssntext" class="form-control" name="addssntext" />
                </div>
                <input type="button" name="addssnbtn" class="btn btn-success" value="Submit" id="addssnbtn">
                <button class="btn btn-danger customredbtn cancelbtn" type="button" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<!-- Division Modal -->
<div class="modal fade " id="DivisionModal" role="dialog">
    <div class="modal-dialog">                                                    
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add new division</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">    
                    <label for="adddivtext">Enter New Division</label>
                    <input type="text" id="adddivtext" class="form-control" name="adddivtext" />
                </div>
                <input type="button" name="adddivbtn" class="btn btn-success" value="Submit" id="adddivbtn">
                <button class="btn btn-danger customredbtn cancelbtn" type="button" data-dismiss="modal">Cancel</button>                                    
                
            </div>
        </div>
    </div>
</div>     
<?php include('footer.php');   ?>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<script>
function dateloadfunction(){
	$( function() {
		$( ".dategame" ).datepicker({
			format: "mm/dd/yyyy",
		    autoclose: true,
		});

	});
	$('.timepicker').timepicker({defaultTime: false,});
}

$(document).on('click','.addmoreplayerbtn',function(){
	var $this  = $(this);
	var AddPlayerHTML  = $('#addmoreplayercont').html();
	var FormEntryLength = $('#bultplayentrycont').length+1;	
	$('#bultplayentrycont').append(AddPlayerHTML);
	dateloadfunction();
});

$(document).on('click','.deleteplayerbtn',function(){
	var $this  = $(this);	
	if($('#bultplayentrycont form').length>1){
	 $('#bultplayentrycont form:last-child').remove();
	}else{
		alert('Minimum one row is required');
		return false;
	}
});

$(document).on('click','.submitallplayerbtn',function(){	
	
	var $thisbtn = $(this);

	var RegExpression = /^[a-zA-Z\s]*$/;  
	var AddPlayerChk = true;
	var FormCount  = $('#bultplayentrycont .multipleplayerformgrp').length;
	var Inc= 0;	
	var allvalid=true;
	
	$('#bultplayentrycont .multipleplayerformgrp').each(function(){	
		var formchk  = true;
		var $thisform = $(this);	
		$thisform.css('border','0px solid red');
		$thisform.find('input').css('border','1px solid #d6d6d6');
		$thisform.find('select').css('border','1px solid #d6d6d6');		
		$thisform.find(".playerimg").css('border','1px solid #d6d6d6');	

		var gamename     	= $thisform.find("#gamename").val();
		var gamedate     	= $thisform.find("#gamedate").val();
		var gametime     	= $thisform.find("#gametime").val();
		var visitor       	= $thisform.find("#visitor").val();
		var home          	= $thisform.find("#home").val();
		var divisionlist    = $thisform.find("#divisionlist").val();
		var seasonlist      = $thisform.find("#seasonlist").val();
		
		if(gamename==''){
			$thisform.find("#gamename").focus();
			$thisform.find("#gamename").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		/*if(gamedate==''){
			$thisform.find("#gamedate").focus();
			$thisform.find("#gamedate").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}*/
	
		/*if(gametime==''){
			$thisform.find("#gametime").focus();
			$thisform.find("#gametime").css('border','1px solid red');
			allvalid=false;
		}*/
		if(visitor==''){
			$thisform.find("#visitor").focus();
			$thisform.find("#visitor").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(divisionlist==''){
			$thisform.find("#divisionlist").focus();
			$thisform.find("#divisionlist").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(seasonlist==''){
			$thisform.find("#seasonlist").focus();
			$thisform.find("#seasonlist").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		
		if(!formchk)
		{
			$thisform.css('border','1px solid red');
		}else{
			$thisform.css('border','0px solid red');
		}

	});	
	if(!allvalid)
	{
		return false;
	}

	$thisbtn.hide();

	$('#bultplayentrycont .multipleplayerformgrp').each(function(){		
		 var $thisform = $(this);  
		 var formData = new FormData($(this)[0]);
		 $.ajax({
			type : "POST",
			url : "addgamebulkentry.php",
			data : formData,	
			async:false,
			contentType: false,
			processData: false,
			success : function(response) {
				var response = $.parseJSON(response);
				console.log(response.playerstatus);				
				if(response.playerstatus=='success'){
					$thisform.find('input, textarea, button, select').attr('disabled','disabled');
					$thisform.find('.uploadstatus').empty().append('<img src="images/yes.gif" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg">');	
				}				
			},
			error: function(jqXHR, textStatus, errorThrown){
				 alert(textStatus, errorThrown);
			}
		});	
	});
		
});

$(document).ready(function() {
	$('#rulelist').prop("disabled", true);
});

$(document).on("change","#divisionlist",function(){
    if ((this.value) == 'addnew') {
		$('#DivisionModal').modal('show');  
		$('#rulelist').prop('disabled', false);        
	}
});
$(document).on("change","#seasonlist",function(){
	if ((this.value) == 'addnew') {
		$('#SeasonModal').modal('show');         
	}
});
$(document).on('click','#adddivbtn',function(){

        var newvalue=  $("#adddivtext").val();
	     
		if(newvalue !=""){  
			//alert(newvalue);
		    $('.divisionlist').append($('<option/>', { 
				value: newvalue,
				text : newvalue,
				selected:'selected' 
			}));

		$("#divisionlist option[value='']").removeAttr("selected","selected");
		} else {
            alert('Field can not be left blank');
            return false;
            $("#divisionlist option[value='']").attr("selected","selected");
	    }    
	   
	  	$('#DivisionModal').modal('hide');
		$('#rulelist').prop('disabled', false); 
	});
	$(".closemodal").click(function(){
		$("#divisionlist option[value='']").attr("selected","selected");
		$('#DivisionModal').modal('hide');
		$('#rulelist').prop("disabled", true);
	});
	$("#addssnbtn").click(function(){
	   	var newvalue=  $("#addssntext").val();
		if(newvalue !=""){  
		    $('.seasonlist').append($('<option/>', { 
		        value: newvalue,
		        text : newvalue,
				selected:'selected' 
			}));
		    $("#seasonlist option[value='']").removeAttr("selected","selected");
		} else {
		    alert('Field can not be left blank');
		    return false;
		    $("#seasonlist option[value='']").attr("selected","selected");
		}   
	     
	  	$('#SeasonModal').modal('hide');
	});
	$(".closesmodal").click(function(){
		$("#seasonlist option[value='']").attr("selected","selected");
		$('#SeasonModal').modal('hide');
	});

	$(".visitorteam").keyup(function(event){
		event.preventDefault();
		search_ajax_wayvisitor();
	});

	$(".hometeam").keyup(function(event){
		event.preventDefault();
		search_ajax_wayhome();
	});

	function search_ajax_wayvisitor(){
	var search_this = $(".visitorteam").val();
	//var search_this=$(this).val();
	console.log(search_this);
	var sportid = $("#sportid").val();
    // alert(sportid);
	$.post("find_visitorid.php", {searchit : search_this, sportval: sportid}, function(data){
		console.log(data);
		$(".display_visitor_result").html(data);
	});
}

function search_ajax_wayhome() {
	var search_this = $(".hometeam").val();
	var sportid = $("#sportid").val();
	
	$.post("find_homeid.php", {searchit : search_this, sportval: sportid}, function(data) {
		
		$(".display_home_result").html(data);
	});
}

function showv(p)
{
	document.getElementById("visitor").value = p;
	document.getElementById("visitor-append").style.display = "none";
}

function showh(p)
{
document.getElementById("home").value = p;
document.getElementById("home-append").style.display = "none";
}

</script>
var gamemonth = '';
var initialloadmonth = '';
$(document).ready(function() {

    initialloadmonth = $('.item.active').attr('month-name');
    
    if (initialloadmonth != "")
        $('.gamemonth strong').text(initialloadmonth);
    $( ".right" ).click(function() {
        var nextMonthName = $('.item.active').next().attr('month-name');        
        $('.gamemonth strong').text(nextMonthName);
        if (nextMonthName == undefined) {
            $('.gamemonth strong').text($('div.item').first().attr('month-name'));
        }
    });

    $( ".left" ).click(function() {          
        var prevMonthName = $('.item.active').prev().attr('month-name');
        $('.gamemonth strong').text(prevMonthName); 
        if (prevMonthName == undefined) {
            $('.gamemonth strong').text($('div.item').last().attr('month-name'));
        }    
    });
    
 
});

function btnPreNextHideShow()
{
    var itemLength = $("div.item").length;    
    if (itemLength == 1) {
        $(".fa-chevron-right").hide();
        $(".fa-chevron-left").hide();
        $(".nogamescaption").show();
    }

}


$( "#resetbtn" ).click(function() {        
    document.location='game_list.php';

});

function ajaxGameList() {


    $(".loadingsection").show();
    $('#game-carousal').hide();
    var Searchbydiv   = ($('#searchtext').val()) ? $('#searchtext').val() : "" ;
    var Searchbyseason = ($('#searchbyseasonid').val()) ? $('#searchbyseasonid').val() : "";              
    var CustomerID     = ($('#customerid').val()) ? $('#customerid').val() : "";
    var SportID        = ($('#sportid').val()) ?$('#sportid').val() : "";   
    var ls             = ($("#tableid").val()) ? $("#tableid").val() : "";
    var gamedate = ($("#gamedate").val()) ? $("#gamedate").val() : "";
    
    $.ajax({
        url:"ajax_game_list.php",  
        method:'GET',
        async: false,
        data:"sportid="+SportID+"&searchbyseason="+Searchbyseason+"&searchbydiv="+Searchbydiv+"&cid="+CustomerID+"&ls="+ls+"&gamedate="+gamedate,
        success:function(data) {
            
            $('#game-carousal').html('');
            $('#game-carousal').html(data); 
            $(".loadingsection").hide();
            $('#game-carousal').show();
            // btnPreNextHideShow()               
        }
    });
} 

function deleteGame(id,sportname){

    var answer = confirm('Are you sure you want to delete?');
    if(answer) {
        window.location = "delete_game.php?action=delete&gid="+id+"&sportname="+sportname;
    } else {
        alert("Cancelled the delete!")
    }
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

var options = {
    selectedYear: 2016,
    startYear: 2008,
    finalYear: 2020,
    openOnFocus: false
};

$('#datepicker').monthpicker(options);
$('#datepicker-button').bind('click', function () {
    $('#datepicker').monthpicker('show');
});

$('#datepicker').monthpicker().bind('monthpicker-click-month', function (e, month) {

}).bind('monthpicker-change-year', function (e, year) {

}).bind('monthpicker-show', function () {

}).bind('monthpicker-hide', function () {
    $('#gamedate').val($(this).val());
    ajaxGameList();

});
// Combine Player

$('.addcombinebtn').on('click', function(evt) {
	if (!$("#combineplayerfrm").validate()) { 
		return false;
	} 	
	$("#combineplayerfrm").submit();		
});

$(document).ready(function() {
	$(document).on("click", ".combineplayer", function () {       	
 		var game_id=$(this).attr('data-id');
		 $('#combine_gameid').val(game_id);
		 $("#game_view_id").text(game_id);
		 $( "#CombineModal" ).modal("show");
	 });

	$("#combineplayerfrm").validate({
        rules: {
          playerid:{required:true,number: true},
		  duplicateplayerid:{required:true,number: true,uniquedivision:true},
		  
         },
        messages: {
             playerid:{required:"Please enter actual player id",number:"Please enter actual player id in number"},
			 duplicateplayerid:{required:"Please enter duplicate player id",number:"Please enter duplicate player id in number",uniquedivision:"This Player Id is not in this game"},
        },

    });

	 var response; 
	 $.validator.addMethod('uniquedivision',function(value){
		var game_id=$('input[name="combine_gameid"]').val();
        var post_type="checkpalyer";
		var playerid=$("#playerid").val();
		var duplicateplayerid=$("#duplicateplayerid").val();
		
		   $.ajax({
            method: "POST",
            url: "checkgamewithplayer-ajax.php",
            async:false,
            data:{"post_type":post_type,"gameid":game_id,"playerid":playerid,"duplicateplayerid":duplicateplayerid},
      
            }).success(function(msg) {
                response = msg;                   
                
            });
	
            if(response == "exists")
                return true;
            else
                return false;    
   });


});

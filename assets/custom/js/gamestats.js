$(document).ready(function() {   

    $("#gamestatsaddplayer").validate({       
        rules: {
            firstname:{
                required:true,
            },
            lastname:{
                required:true,
            }, 
            uniformno:{
                required:true,
                number: true,
            },
            age:{
                number: true,
            },           
        }, 
        messages: {
            firstname:{
                required: "Please enter first name",            
            },
            lastname:{
                required: "Please enter last name",           
            },
            uniformno:{
                required: "Please enter uniform number",   
            },
            age:{
                number: "Please enter valid age",   
            },        
        },
        submitHandler: function (form) { 

            var $form = $(form);
            var FormArr  = $form.serialize();
            $.ajax({  
                type: 'POST',
                url: "game_stats_addplayer.php?"+FormArr,
                success: function(data) {
                    var myArray = JSON.parse(data);
                    var countPlayer = myArray.length;
                    $(".playerexist").empty();
                    for (var j=0;j<myArray.length;j++) {
                        if (myArray[j]["status"] == "playeralreadyexists") {
                            var playerHTML = '<div class="plyerexistsstatus_'+j+'"><div class="col-md-12 col-sm-12 col-xs-12 alert-danger"><div class="col-md-12 col-sm-12 col-xs-12"><p style="margin:10px 0px;text-align:center;">Player '+myArray[j]["lastname"]+', '+myArray[j]["firstname"]+' already Exist with ID: '+myArray[j]["id"]+'</p></div><div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;text-align:center;"><button type="button" class="btn customredbtn addasnewplayertoteam_'+j+'" id="" player-id="'+myArray[j]["id"]+'" team-type="'+myArray[j]["teamname"]+'" mode="update" appendpalyer="'+j+'">Update this player</button></div></div></div></div>';
                            var newPlayerBtn = '<div class="col-md-12 col-sm-12 col-xs-12 alert-success"><div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;padding-right:0px;padding-top:10px;padding-bottom:10px;"><button type="button" class="btn customgreenbtn addasnewplayertoteam_'+j+'" id="" player-id="'+myArray[j]["id"]+'" team-type="'+myArray[j]["teamname"]+'" mode="insert">Add as new player</button></div></div>';
                            $(".playerexist").append(playerHTML);
                            if (j==countPlayer-1)
                                $(".playerexist").append(newPlayerBtn);

                            $('.addasnewplayertoteam_'+j+'').click(function() {

                                var mode = $(this).attr("mode");
                                var updateplayerID = $(this).attr("player-id");
                                var appendPlayerEle = $(this).attr("appendpalyer");
                                var $form = $(form);
                                var FormArr  = $form.serialize();
                                $.ajax({
                                    url:"add_new_update_player.php?"+FormArr+"&mode="+mode+"&playerid="+updateplayerID,
                                    method: "GET",
                                    success:function(result){
                                        var childArray = JSON.parse(result);
                                        if (childArray[0]["status"] == "success") {
                                            if (childArray[0]["mode"] == "insert")
                                                $(".playerexist").empty().append("<div class='col-md-12 col-sm-12 col-xs-12 alert-success'><p>Inserted successfully</p></div>");
                                            else
                                                $(".plyerexistsstatus_"+appendPlayerEle).empty().append("<div class='col-md-12 col-sm-12 col-xs-12 alert-success'><p>Updated successfully</p></div>"); 

                                            var playerId = childArray[0]["id"];
                                            var checkname = childArray[0]["lastname"]+", "+childArray[0]["firstname"];
                                            var childVisitorPlayerTableHTML = '<tr><input type="hidden" name="visitor_id[]" value="'+playerId+'"><td nowrap >'+checkname+'</td><td><input type="text" style="width:35px;" class="visitor_gp" data-val="int" name="visitor_gp['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_gs" data-val="int" name="visitor_gs['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptmade" data-val="int" name="visitor_2ptmade['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptatt" data-val="int" name="visitor_2ptatt['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fgm" data-val="int" name="visitor_3fgm['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fga" data-val="int" name="visitor_3fga['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ftm"data-val="int" name="visitor_ftm['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_fta" data-val="int" name="visitor_fta['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tp" data-val="int" name="visitor_tp['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_oreb" data-val="int" name="visitor_oreb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_dreb" data-val="int" name="visitor_dreb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_treb" data-val="int" name="visitor_treb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_pf" data-val="int" name="visitor_pf['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tf" data-val="int" name="visitor_tf['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ast" data-val="int" name="visitor_ast['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_to" data-val="int" name="visitor_to['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_blk" data-val="int" name="visitor_blk['+playerId+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="visitor_stl" data-val="int" name="visitor_stl['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_min" data-val="int" name="visitor_min['+playerId+']" value=""></td></tr>';
                                            var childHomePlayerTableHTML = '<tr><input type="hidden" name="home_id[]" value="'+playerId+'"><td nowrap >'+checkname+'</td><td><input type="text" style="width:35px;" class="home_gp" data-val="int" name="home_gp['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_gs" data-val="int" name="home_gs['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptmade" data-val="int" name="home_2ptmade['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptatt" data-val="int" name="home_2ptatt['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fgm" data-val="int" name="home_3fgm['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fga" data-val="int" name="home_3fga['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_ftm"data-val="int" name="home_ftm['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_fta" data-val="int" name="home_fta['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_tp" data-val="int" name="home_tp['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_oreb" data-val="int" name="home_oreb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_dreb" data-val="int" name="home_dreb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_treb" data-val="int" name="home_treb['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_pf" data-val="int" name="home_pf['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_tf" data-val="int" name="home_tf['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_ast" data-val="int" name="home_ast['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_to" data-val="int" name="home_to['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_blk" data-val="int" name="home_blk['+playerId+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="home_stl" data-val="int" name="home_stl['+playerId+']" value=""></td><td><input type="text" style="width:35px;" class="home_min" data-val="int" name="home_min['+playerId+']" value=""></td></tr>';
                                            if (childArray[0]["mode"] != "update") {
                                                if (childArray[0]["teamname"] == "visitorteam")
                                                    $("#search_result").find('tr:last').prev().after(childVisitorPlayerTableHTML);
                                                else
                                                    $("#search_result_home").find('tr:last').prev().after(childHomePlayerTableHTML);
                                            }
                                        }
                                    }
                                });
                            });

                        } else if (myArray[j]["status"] == "success") {
                            $("#addplayermodal").modal("hide");
                            var player_id = myArray[j]["id"];
                            var playername = myArray[j]["lastname"]+", "+myArray[j]["firstname"];
                            var visitoPlayerTableHTML = '<tr><input type="hidden" name="visitor_id[]" value="'+player_id+'"><td nowrap >'+playername+'</td><td><input type="text" style="width:35px;" class="visitor_gp" data-val="int" name="visitor_gp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_gs" data-val="int" name="visitor_gs['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptmade" data-val="int" name="visitor_2ptmade['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_2ptatt" data-val="int" name="visitor_2ptatt['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fgm" data-val="int" name="visitor_3fgm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_3fga" data-val="int" name="visitor_3fga['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ftm"data-val="int" name="visitor_ftm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_fta" data-val="int" name="visitor_fta['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tp" data-val="int" name="visitor_tp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_oreb" data-val="int" name="visitor_oreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_dreb" data-val="int" name="visitor_dreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_treb" data-val="int" name="visitor_treb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_pf" data-val="int" name="visitor_pf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_tf" data-val="int" name="visitor_tf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_ast" data-val="int" name="visitor_ast['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_to" data-val="int" name="visitor_to['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_blk" data-val="int" name="visitor_blk['+player_id+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="visitor_stl" data-val="int" name="visitor_stl['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="visitor_min" data-val="int" name="visitor_min['+player_id+']" value=""></td></tr>';
                            var homePlayerTableHTML = '<tr><input type="hidden" name="home_id[]" value="'+player_id+'"><td nowrap >'+playername+'</td><td><input type="text" style="width:35px;" class="home_gp" data-val="int" name="home_gp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_gs" data-val="int" name="home_gs['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptmade" data-val="int" name="home_2ptmade['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_2ptatt" data-val="int" name="home_2ptatt['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fgm" data-val="int" name="home_3fgm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_3fga" data-val="int" name="home_3fga['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_ftm"data-val="int" name="home_ftm['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_fta" data-val="int" name="home_fta['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_tp" data-val="int" name="home_tp['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_oreb" data-val="int" name="home_oreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_dreb" data-val="int" name="home_dreb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_treb" data-val="int" name="home_treb['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_pf" data-val="int" name="home_pf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_tf" data-val="int" name="home_tf['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_ast" data-val="int" name="home_ast['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_to" data-val="int" name="home_to['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_blk" data-val="int" name="home_blk['+player_id+']" value=""></td><td><input type="text" style="width:35px;" style="width:35px;" class="home_stl" data-val="int" name="home_stl['+player_id+']" value=""></td><td><input type="text" style="width:35px;" class="home_min" data-val="int" name="home_min['+player_id+']" value=""></td></tr>';
                            if (myArray[j]["teamname"] == "visitorteam")
                                $("#search_result").find('tr:last').prev().after(visitoPlayerTableHTML);
                            else
                                $("#search_result_home").find('tr:last').prev().after(homePlayerTableHTML);
                        }
                    }
                }
            });
        }
    });

    $('#visitor_gs_total').val(sumMatchStats('visitor_gs'));
    $('#visitor_2ptmade_total').val(sumMatchStats('visitor_2ptmade'));
    $('#visitor_2ptatt_total').val(sumMatchStats('visitor_2ptatt'));
    $('#visitor_3fgm_total').val(sumMatchStats('visitor_3fgm'));
    $('#visitor_3fga_total').val(sumMatchStats('visitor_3fga'));
    $('#visitor_ftm_total').val(sumMatchStats('visitor_ftm'));
    $('#visitor_fta_total').val(sumMatchStats('visitor_fta'));
    $('#visitor_tp_total').val(sumMatchStats('visitor_tp'));
    $('#visitor_oreb_total').val(sumMatchStats('visitor_oreb'));
    $('#visitor_dreb_total').val(sumMatchStats('visitor_dreb'));
    $('#visitor_treb_total').val(sumMatchStats('visitor_treb'));
    $('#visitor_pf_total').val(sumMatchStats('visitor_pf'));
    $('#visitor_tf_total').val(sumMatchStats('visitor_tf'));
    $('#visitor_ast_total').val(sumMatchStats('visitor_ast'));
    $('#visitor_to_total').val(sumMatchStats('visitor_to'));
    $('#visitor_blk_total').val(sumMatchStats('visitor_blk'));
    $('#visitor_stl_total').val(sumMatchStats('visitor_stl'));
    $('#visitor_min_total').val(sumMatchStats('visitor_min'));

    $('#home_gs_total').val(sumMatchStats('home_gs'));
    $('#home_2ptmade_total').val(sumMatchStats('home_2ptmade'));
    $('#home_2ptatt_total').val(sumMatchStats('home_2ptatt'));
    $('#home_3fgm_total').val(sumMatchStats('home_3fgm'));
    $('#home_3fga_total').val(sumMatchStats('home_3fga'));
    $('#home_ftm_total').val(sumMatchStats('home_ftm'));
    $('#home_fta_total').val(sumMatchStats('home_fta'));
    $('#home_tp_total').val(sumMatchStats('home_tp'));
    $('#home_oreb_total').val(sumMatchStats('home_oreb'));
    $('#home_dreb_total').val(sumMatchStats('home_dreb'));
    $('#home_treb_total').val(sumMatchStats('home_treb'));
    $('#home_pf_total').val(sumMatchStats('home_pf'));
    $('#home_tf_total').val(sumMatchStats('home_tf'));
    $('#home_ast_total').val(sumMatchStats('home_ast'));
    $('#home_to_total').val(sumMatchStats('home_to'));
    $('#home_blk_total').val(sumMatchStats('home_blk'));
    $('#home_stl_total').val(sumMatchStats('home_stl'));
    $('#home_min_total').val(sumMatchStats('home_min'));

    // $(".matchstats_tble tr td input").blur(function(){
    $(document).on("keyup",".matchstats_tble tr td input", function(){
        
        var getClassName = $(this).attr('class');       
        if ((getClassName != 'visitor_gp') && (getClassName != 'home_gp')) {            
            $('#'+getClassName+'_total').val(sumMatchStats(getClassName));
        }       
    });
    $(".team-pts-stats tr td .visitor_pts_w").blur(function(){
        var getWvalue = $('.visitor_pts_w').val();
        var getLvalue = $('.visitor_pts_w').val();
        if (getWvalue == '1') {         
            $('.visitor_pts_l').attr('value', '0');
            $('.home_pts_l').attr('value', '1');
            $('.home_pts_w').attr('value', '0');
        } else {            
            $('.visitor_pts_l').attr('value', '1');
            $('.home_pts_l').attr('value', '0');
            $('.home_pts_w').attr('value', '1');            
        }       
    });

    $('.visitor_pts_w').on("keypress", function(event){     
        var keyCode = event.which;      
        if((keyCode > 49)){
            event.preventDefault();
        }
    }); 
    $("input[data-val='int']").keypress(function(event) {
        if ( (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190) ) {

        }
        else {      
            if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
                event.preventDefault(); 
            } 
        }
    });
    function sumMatchStats(classname){
        var values = [];        
        $("input[class="+classname+"]").each(function() {
            values.push($(this).val());
        });
        var total = 0;
        for (var i = 0; i < values.length; i++) {
            total += values[i] << 0;           
        }       
        return total;
    }   

    $(".modify_game .modify_button").click(function ()
    {       
        var game_id = $('input[name=check]:checked').val();
        document.location = 'match_stats.php?gid='+game_id;     
    }); 
});
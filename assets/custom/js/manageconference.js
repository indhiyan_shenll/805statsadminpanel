 $(document).ready(function() {

     $('#sample_1').DataTable({
            "retrieve": true,
            "paging": false,
            "bInfo": false,
           "bFilter":false,
            "bLengthChange":false,
            "bPaginate":false,
            "aaSorting": [[0,'desc']],
            "aoColumnDefs": [ { "bSortable": false, "aTargets": [2] } ],
            "language": {
            "zeroRecords": "No Conference(s) found.",
            "infoEmpty": "No records available"
        }, 
    });

    function confirmation(id,sportname){
        var check = confirm('Are you want to sure delete this conference?');
        if (check == true) {
                window.location="conference_list.php?cid="+id+"&sport="+sportname;
                return true;
        } else {
            return false;
        }    
    }

    $(document).on("click",".popup",function(){
        var popupname = $(this).text();
        $('#confere_name').val('');
        $('#mode').empty().append('Add');
		$('#mode_btn').empty().append('Submit');
        $('#static').modal({show: 'true'});
		
        
    });

    $("#frm_conference").validate({
        rules: {
          confere_name:{required:true,unique: true},
         
        },
        messages: {
             confere_name:{required:"Please enter conference name",unique:"Conference name already exists"},
        },
    });

    var response; 
    $.validator.addMethod('unique',function(value){

        var confere_name=$("#confere_name").val();
        var con_id=$('input[name="hnd_conferenceid"]').val();
        var customer_id=$('input[name="hndcoustid"]').val();
        var post_type="conference_name_check";

    
        $.ajax({
            method: "POST",
            url: "manageconference-ajax.php",
            async:false,
            data:{"post_type":post_type,"confere_name":confere_name,"con_id":con_id,"customer_id":customer_id},
      
            }).success(function(msg) {
                response = msg;                   
                
            });
            if(response == "not-exists")
                return true;
            else
                return false;    
    });

    $( "#conferencename" ).keyup(function() {
        conferenceName();         
    });

    $( ".resetbtn" ).click(function() {
        $( "#conferencename" ).val('');
        $('#mode').append('');
        conferenceName();
    });

    $(document).on("click",".edit_popup",function(){
        var conferenceid =$(this).attr("data-id");
        var conferencename =$(this).attr("data-name");
        var datasport =$(this).attr("data-sport");
        var datacid =$(this).attr("data-cid");

        if(conferenceid!="" && conferencename!="" && datasport!="" && datacid!=""){
            $("#confere_name").val(conferencename);
            $("#hnd_conferenceid").val(conferenceid);
            $("#sport_name").val(datasport);
            $("#hnd_cid").val(datacid);
            $('#mode').empty().append('Edit');
			$('#mode_btn').empty().append('Update');
            $('#static').modal({show: 'true'});   
        }
    });

    function conferenceName() {

        $(".loadingsection").show();
        $('#sample').hide();

        var conferencename = $("#conferencename").val();
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
        $("#hdnsearch").val(conferencename);
         $.ajax({
            url:"filter_conference.php",  
            method:'POST',
            data:{conferencename: conferencename, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
            success:function(data) {
                
                $(".loadingsection").hide();
                $('#sample').show();
                
                $('#sample').html(''); 
                $('#sample').html(data);
            }
          });
    }


});

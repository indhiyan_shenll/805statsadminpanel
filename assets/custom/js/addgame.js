$(document).ready(function() {

	$('#rulelist').prop("disabled", true);
	$("#cancelbtn").click(function(){
	var sportnam = $("#sportn").val();
		// window.location = "gameentrylist.php?sport="+sportnam ;
		window.location = "game_list.php?sport="+sportnam;
	});	

	$('#divisionlist').change(function(){
		if ((this.value) == 'addnew') {
			$('#DivisionModal').modal('show');  
			$('#rulelist').prop('disabled', false);        
	    }
	});

	$("#adddivbtn").click(function(){
	   	var newvalue=  $("#adddivtext").val();
	   	// alert(newvalue);
		if(newvalue !=""){  
		    $('#divisionlist').append($('<option/>', { 
				value: newvalue,
				text : newvalue,
				selected:'selected' 
			}));

		$("#divisionlist option[value='']").removeAttr("selected","selected");
		} else {
            alert('Field can not be left blank');
            return false;
            $("#divisionlist option[value='']").attr("selected","selected");
	    }    
	   
	  	$('#DivisionModal').modal('hide');
		$('#rulelist').prop('disabled', false); 
	});
	$(".closemodal").click(function(){
		$("#divisionlist option[value='']").attr("selected","selected");
		$('#DivisionModal').modal('hide');
		$('#rulelist').prop("disabled", true);
	});


	$('#seasonlist').change(function(){
		if ((this.value) == 'addnew') {
			$('#SeasonModal').modal('show');         
		}
	});

	$("#addssnbtn").click(function(){
	   	var newvalue=  $("#addssntext").val();
		if(newvalue !=""){  
		    $('#seasonlist').append($('<option/>', { 
		        value: newvalue,
		        text : newvalue,
				selected:'selected' 
			}));
		    $("#seasonlist option[value='']").removeAttr("selected","selected");
		} else {
		    alert('Field can not be left blank');
		    return false;
		    $("#seasonlist option[value='']").attr("selected","selected");
		}   
	     
	  	$('#SeasonModal').modal('hide');
	});
	$(".closesmodal").click(function(){
		$("#seasonlist option[value='']").attr("selected","selected");
		$('#SeasonModal').modal('hide');
	});


	$("#visitor").keyup(function(event){
		event.preventDefault();
		search_ajax_wayvisitor();
	});

	$("#home").keyup(function(event){
		event.preventDefault();
		search_ajax_wayhome();
	});

});

function search_ajax_wayvisitor(){
	var search_this = $("#visitor").val();
	var sportid = $("#sportid").val();

	$.post("find_visitorid.php", {searchit : search_this, sportval: sportid}, function(data){
		$("#display_visitor_result").html(data);
	});
}

function search_ajax_wayhome() {
	var search_this = $("#home").val();
	var sportid = $("#sportid").val();
	
	$.post("find_homeid.php", {searchit : search_this, sportval: sportid}, function(data) {
		
		$("#display_home_result").html(data);
	});
}

function showv(p)
{
	document.getElementById("visitor").value = p;
	document.getElementById("visitor-append").style.display = "none";
}

function showh(p)
{
document.getElementById("home").value = p;
document.getElementById("home-append").style.display = "none";
}


$(document).ready(function(){
	
	
	jQuery.validator.addMethod("lettersonly", function(value, element) {	
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Do not add space or special character or number"); 
	
	jQuery.validator.addMethod("integer", function(value, element) {
	return this.optional(element) || /^\d+$/.test(value);
}, "A positive or negative non-decimal number please");

	jQuery.validator.addMethod("uploadFile", function (val, element) {

		var size = element.files[0].size;	
		if (size > 300000) {
			return false
		} else {
			return true
		}

	}, "file size is large than 300kb");

});


$(document).ready(function(){
	$("#gameform").validate({
		rules: {
			 gamename:{
				required:true,				
			 },
			gamedate:{
				required:true,
				
			 },	
			gametime:{
				required:true,			
			 },	
			visitor:{
				required:true,			
			 },
			home:{
				required:true,			
			 },
			divisionlist:{
				required:true,			
			 },
			rulelist:{
				required: true,               
			 },
			seasonlist:{
				required: true,
			},	
	    }, 
		messages: {
			gamename:{
			   required: "Please enter game name",			  
			},
			gamedate:{
			   required: "Please enter date",			  
			},
			gametime:{
			   required: "Please enter time",		 
			},
			visitor:{
			   required: "Please enter visitor name",		 
			},
			home:{
			   required: "Please enter home",		 
			},
			divisionlist:{
			   required: "Please select division",		 
			},
			rulelist:{
			   required: "Please select rule",            
			},
			seasonlist:{
			 	required: "Please select season",  
			},			
	   },
	   errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gamedate") { // for uniform radio buttons, insert the after the given container
                error.insertAfter(".minicalender");
            } else if (element.attr("name") == "gametime") { // for uniform checkboxes, insert the after the given container
                error.insertAfter(".minitime");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

	});

});


$(document).ready(function(){
	$("#eventform").validate({
		rules: {
			 ename:{
				required:true,				
			 },
			date:{
				required:true,
				
			 },	
			time:{
				required:true,			
			 },	
			descp:{
				required:true,			
			 },
			
	    }, 
		messages: {
			ename:{
			   required: "Please enter value",			  
			},
			gamedate:{
			   required: "Please enter date",			  
			},
			time:{
			   required: "Please enter time",		 
			},
			descp:{
			   required: "Please enter  description",		 
			},
						
	   },
	   errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gamedate") { // for uniform radio buttons, insert the after the given container
                error.insertAfter(".minicalender");
            } else if (element.attr("name") == "gametime") { // for uniform checkboxes, insert the after the given container
                error.insertAfter(".minitime");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

	});

});
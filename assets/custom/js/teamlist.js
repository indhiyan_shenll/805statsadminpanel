$(document).ready(function() {

$('#sportslist').change(function() {
		var sportnm = this.value;
		if(sportnm !=""){
			$('#addnewbtn').show();         
		} else {
			$('#addnewbtn').hide(); 
		} 

	});

	$("#addnewbtn").click(function(){
		var sportnam = $("#sportslist").val();
		window.location = "addteam.php?sport="+sportnam ;
	});

});

function deleteTeam(id,sportname){
	var answer = confirm('Are you sure you want to delete?');
	if(answer){ 
		window.location = "delete_team.php?action=delete&did="+id+"&sportname="+sportname;
	}
	else{
		alert("Cancelled the delete!")
	}
}

function confirmation(id,sportname){
        var check = confirm('Are you want to sure delete this player?');
        if (check == true) {
            window.location="player_list.php?pid="+id+"&sport="+sportname;
            return true;
        }
        else {
            return false;
        }
    }
    
function filter_players_by_team() {
            var Searchbyname   = $('#searchbynameid').val();
            $("#hnd_team_id").val(Searchbyname);
            var Searchbyseason = $('#searchbyseasonid').val(); 
			$("#hnd_season_id").val(Searchbyseason);  
			var Searchbystatus   = $('#searchbyteamstatus').val();
			$("#searchbyteamstatus").val(Searchbystatus);
			//alert(Searchbystatus);
            var CustomerID     = $('#customerid').val();    
            var SportID        = $('#sportid').val();   
            var ls             = $("#tableid").val();
            var sportname      = $("#sportname").val();
            var HdnPage        = $("#HdnPage").val();
            var HdnMode        = $("#HdnMode").val();
            var RecordsPerPage = $("#RecordsPerPage").val();
            $(".loadingplayersection").show();
            $("#sample").hide();
            $.ajax({
                url:"filter_players.php",  
                method:'GET',
                data:"sportid="+SportID+"&searchbyseason="+Searchbyseason+"&searchbyname="+Searchbyname+"&cid="+CustomerID+"&ls="+ls+"&sportname="+sportname+"&HdnPage="+HdnPage+"&HdnMode="+HdnMode+"&PerPage="+RecordsPerPage+"&Searchbystatus="+Searchbystatus,
                success:function(data) { 
                    $(".loadingplayersection").hide();
                    $("#sample").show();                           
                    document.getElementById('sample').innerHTML = data; 
                    $('.table-header').remove();
                    loading_sorting();
                }
              });                
            
        }
       



$(document).on("click",".addplusicon",function(){
    var AddMoreHtml  = "<img src='images/decrease.png' class='imguploadicons removeimgicon'>";
    var UploadHtml   = $(this).closest(".modal-body").find('.imguploadcont').clone().removeClass("imguploadcont");  
    UploadHtml.find(".addnewheading").remove();
    UploadHtml.find(".profileimgs").multiselect("deselectAll", false);
    UploadHtml.find(".profileimgs").attr("name","seasons["+MultiSelectCnt+"][]");
    UploadHtml.find('.profileimgswrap .multiselect-native-select').find(".multiselect-native-select").next(".btn-group").remove();
    UploadHtml.find('.imgiconcont').append(AddMoreHtml);        
    $(this).closest(".modal-body").find("#moreuploadwrap").append(UploadHtml);      
    MultiSelectCnt++;
    return false;
});

$(document).on("click",".removeimgicon",function(){
    $(this).closest(".playerimguploadwrap").remove();
    return false;
});

$(document).on("click",".uploadimgbtn",function(e){
    $(this).closest("form").submit();
});

$(document).on("click",".deleteprofileimg",function(e){
    
    var $this  = $(this);
    if(!confirm("Are you sure want to remove?")){
            return false;
         }else{
            var PlayerImgId  = $this.attr("date-imgid");             

            $.ajax({
            url:"manage_seasonimg.php",  
            method:'POST',
            data:{profileimgid:PlayerImgId,action:'delete'},
            success:function(data) {
                var DataArr  = data.split("###");
                if(DataArr[0]=='success'){
                    $this.closest("tr").remove();
                    $('#deletestatus').removeClass("alert-failure alert-success").empty().addClass("alert-success").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Details '+DataArr[1]+' successfully</strong>');
                }else{
                     $('#deletestatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                }
            }
          });
    }
});

$(document).on("click",".editplayimg,.saveplayimgbtn",function(){
        var $this  = $(this);       

        if($this.hasClass("editplayimg")){
            $this.addClass("saveplayimgbtn").removeClass("editplayimg").text("Save");
            $this.closest("tr").find('.multiselect-native-select').show();          
            return false;
        }else{  
            $this.next('.loading_img').show();
            var selectedValues = $this.closest("tr").find(".profileimgsedit").val();            
            var PlayerImgId  = $this.attr("date-imgid");
            var PlayerId     = $this.closest(".profileimgform").find('.hidplayerid').val();             
            $.ajax({
            url:"manage_seasonimg.php",  
            method:'POST',
            data:{profileimgid:PlayerImgId,action:'update',selectedvalue:selectedValues,playerid:PlayerId},
            success:function(data) {                
                $this.next('.loading_img').hide();
                var DataArr  = data.split("###");
                if(DataArr[0]=='success'){      
                    $this.closest("tr").find('.seasonsnames').empty().append(DataArr[2]);
                    $('#deletestatus').removeClass("alert-failure alert-success").empty().addClass("alert-success").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Details '+DataArr[1]+' successfully</strong>');
                    $this.removeClass("saveplayimgbtn").addClass("editplayimg").text("Edit");
                    $(".profileimgs").empty().append(DataArr[3]).multiselect('refresh');
                            
                }else{
                    $('#deletestatus').empty().removeClass("alert-failure").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                    $this.removeClass("saveplayimgbtn").addClass("editplayimg").text("Edit");
                }
            }
          });
        }
        return false;
});
$(document).on("click",".status_change",function(){
    var thisBtn   =  $(this);
    var id=$(this).attr("id");
    var num=$(this).attr("rel");
    var poststr="num="+num+"&id="+id;
        $.ajax({
              url:"changestatus_players.php",
              cache:0,
              data:poststr,
              success:function(data){
                var StatusArr = data.split('##^^^##');             
                if(StatusArr[1]=='success'){
                var UpdatedMsg = '';
                    if(StatusArr[0]=='Active'){
                        var status_id=StatusArr[2];                 
                        $("#"+status_id).attr('rel', 'Deactive');
                        $("#" +status_id).find("img").attr("src","./images/active_player.png");     
                        $("#" +status_id).find("img").attr("data-original-title","Click here to Deactive");
                        
                }else{
                    var status_id=StatusArr[2];                 
                    $("#" +status_id).attr('rel', 'Active');
                    $("#" +status_id).find("img").attr("src","./images/deactive_player.png");   
                    $("#" +status_id).find("img").attr("data-original-title","Click here to Active");

                }                   
            }else{
            }
        }
    }); 
});

$(document).ready(function(){
	
	
	// jQuery.validator.addMethod("lettersonly", function(value, element) {	
	// 	return this.optional(element) || /^[a-z]+$/i.test(value);
	// }, "Do not add space or special character or number");

	jQuery.validator.addMethod("lettersonly", function(value, element) {	
		return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
	}, "Do not add special characters or number"); 

	jQuery.validator.addMethod("uploadFile", function (val, element) {

		if (element.files[0]) {
			var size = element.files[0].size;	
			if (size > 300000) {
				return false
			} else {
				return true
			}
		} else {
			return true
		}

		}, "file size is large than 300kb");
	

});

$(document).ready(function(){

    $("#eventform").validate({       
         rules: {
              teamname:{
                 required:true,
                 
              },
				   date:{
                 required:true,
                 
              },
				   time:{
                 required:true,
                 
              },
				   descp:{
                 required:true,
                 
              },
                 
         }, 
         messages: {
          ename:{
            required: "Please enter field",            
          },
			 date:{
            required: "Please enter date",           
          },
			time:{
            required: "Please enter time",           
          },
			 descp:{
            required: "Please enter description",           
          },
         
        }

     });

});
      
<?php
include_once('session_check.php');
include_once('connect.php');
include_once('common_functions.php');
error_reporting(E_ALL);
$customer_id="";
if($_SESSION['loginid']!='')  {
  if($_SESSION['usertype']=='user') {
     $customer_id=$_SESSION['loginid'];
  }
}
$info_file      = '';
$upload_file_db='';
if(isset($_POST['type']) && !empty($_POST['type'])){
	$type=$_POST['type'];
	//$info_file      = $_POST['info_file'];
	$playerid      = $_REQUEST['player_id'];
    $params             =   array();
	parse_str($_POST["form_data"], $params);
	//print_r($params);
	$firstname =  $params["first_name"];
	$lastname    =  $params['last_name'];
	$gender      =  $params['sel_gender'];
	$height      =  $params['player_height'];
	$weight      =  $params['player_weight'];
	$dob         =  $params['dob'];
	$age         =  $params['age'];
	$school      =  $params['school_name'];
	$grade       =  $params['grade'];
	$prevschool  =  $params['pre_school'];
	$hometown    =  preg_replace('/\s+/', '', $params['home_town']);
	$uniform     =  $params['uniform_no'];
	$position    =  $params['sel_position'];
	$team        =  $params['sel_team'];
	$playernotes =  $params['player_note'];
	$sportname   =  $params['sportid'];	


	$playequery = $conn->prepare("select * from player_info where  id='$playerid'");
	$playequery->execute();
	$numrows = $playequery->rowCount();
	if($numrows>0){
    $getplayRow=$playequery->fetch(); 
	$upload_file_db	 =	$getplayRow['image'];		
    }


	if(!empty($_FILES["info_file"]["name"])){
            $extension = pathinfo($_FILES["info_file"]["name"],PATHINFO_EXTENSION);
            $upfilename=md5(time()).'.'.$extension; 
            move_uploaded_file($_FILES["info_file"]["tmp_name"],SYSTEM_ROOT_PATH."/uploads/players/".$upfilename);
            $data = array(
             'width' => "200",
             'height' =>"200",
             'image_source' =>SYSTEM_ROOT_PATH."/uploads/players/".$upfilename,
             'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/players/thumb/",
             'image_name' =>$upfilename
           );     
          $cropsucessfully = cropImage($data);
        } else {
          $upfilename=$upload_file_db;
        }
	
	
	if($type=='addnewplayer'){
	
	$InsertPlayer = $conn->prepare("INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, hometown, player_note, height, weight, image, date_of_birth, sport_id) VALUES ('$customer_id', '$firstname', '$lastname', '$gender', '$age', '$school', '$prevschool', '$grade', '$uniform', '$team', '$position', '$hometown', '$playernotes', '$height', '$weight', '$upfilename',  '$dob', '$sportname')");
    $InsertPlayer->execute();
	if ($InsertPlayer) {
           
			 $status="AddSuccess";
     }
	 }elseif($type=='updateplayer'){
	$updateRes = $conn->prepare("UPDATE player_info SET firstname='$firstname', lastname='$lastname', sex='$gender', age='$age', school='$school', prev_school='$prevschool', grade='$grade', uniform_no='$uniform', team_id='$team', position='$position', hometown='$hometown', player_note='$playernotes',height='$height', weight='$weight',image='$upfilename', date_of_birth='$dob', sport_id='$sportname' WHERE id='$playerid'");
	$updateRes->execute();		
	if($updateRes){
		$status="EditSuccess";
	}
	}
	echo $status;
}
?>
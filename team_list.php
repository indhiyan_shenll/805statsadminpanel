<?php 
include_once('connect.php');
include_once('session_check.php');
include_once('common_functions.php');
include_once('usertype_check.php');

$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
$SportListArr = array(":customer_id"=>$Cid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id']; 
    }

    if ($Sports[0]=='4444') { $tablename='team_stats_bb'; } 
    if ($Sports[0]=='4442' || $Sports[0]=='4441') { $tablename='team_stats_ba'; } 
    if ($Sports[0]=='4443') { $tablename='team_stats_fb'; }
}


// if (isset($_SESSION['sportname'])) {
if (isset($_GET['sport'])) {

    // $SportName = $_SESSION['sportname'];
    $SportName = $_GET['sport'];
    // if ($SportName == $_SESSION['sportname']) {

    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
            $SportId = $QrySportVal['sportcode']; 
        }
    }

} else {
    $SportId = "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446";
}

if($_SESSION['master'] == 1) {
    $children = array($_SESSION['childrens']);
    $ids = join(',',$children);
    $DefaultQry = $conn->prepare("select * from customer_division where custid in ($ids) order by id DESC limit 0,1");
    $DefaultQry->execute();
    $Cntdres = $DefaultQry->rowCount(); 
    $FetchDefaultDivision = $DefaultQry->fetch(PDO::FETCH_ASSOC);
    $DefaultDivision = $FetchDefaultDivision['id'];
    $DefaultDivQryCondn = " and division=".$DefaultDivision;
} else {    
    // echo "select distinct(season) from $tablename where customer_id in ($Cid) and season<>'' order by id DESC limit 0,1";exit;
    $DefaultSesQry = $conn->prepare("select distinct(season) from $tablename where customer_id in ($Cid) and season<>'' order by id DESC limit 0,1");
    $DefaultSesQry->execute();
    $Cntsres = $DefaultSesQry->rowCount(); 
    $FetchDefaultSeason = $DefaultSesQry->fetch(PDO::FETCH_ASSOC);
    $DefaultSeason = $FetchDefaultSeason['season'];
    $DefaultSesQryCondn = " and season=".$DefaultSeason;
}



if(isset($_POST["hdndivid"])){
    $HiddenDivid = $_POST["hdndivid"];
}
if(isset($_POST["hdnsesid"])){
    $HiddenSesid = $_POST["hdnsesid"];
}
if(isset($_POST["hdnsearchteam"])){
    $HiddenSearchtext = $_POST["hdnsearchteam"];
}
if(isset($_POST["hnd_status"])){
    $hnd_status = $_POST["hnd_status"];
} else{
    $status="and teams_info.status='1'";
    $status1="and status='1'";
}

if (isset($_POST['hdndivid']) || isset($_POST['hdnsesid']) ) {
    
    $HiddenDivsion = ( $HiddenDivid )? $HiddenDivid : $_POST['divisionid'] ;
    $HiddenSeson = ( $HiddenSesid )? $HiddenSesid : $_POST['seasonid'] ;
    $HiddenSearchTeam = ( $HiddenSearchtext )? $HiddenSearchtext : $_POST['hdnsearchteam'] ;
	$hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
    if($hdn_status=="active"){
            $status= "and teams_info.status='1'";
            $status1= "and status='1'";
    } if($hdn_status=="Inactive") {
            $status= "and teams_info.status='0'";
            $status1= "and status='0'";
    }
    if (isset($_GET['search'])) {
        $searchelem = $_GET['search'];
        if($_SESSION['master']==1) {
            $children = array($_SESSION['childrens']);
            //$ids = join(',',$children);
			$ids = $_SESSION['loginid'].",".join(',',$children); 
            $res = "select * from teams_info where (customer_id IN ($ids) or customer_id IN ($ids)  $status1 and (sport_id='$SportId') and team_name like '{$searchelem}%' ";
        } else {
          $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";
        }
    } else {
        // if (!empty($HiddenDivsion) && !empty($HIddenSearchTeam)) {
            if ($_SESSION['master'] == 1) { 
				$hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
				if($hdn_status=="active"){
						$status= "and teams_info.status='1'";
						$status1= "and status='1'";
				} if($hdn_status=="Inactive") {
						$status= "and teams_info.status='0'";
						$status1= "and status='0'";
				}
                $children = array($_SESSION['childrens']);
                $ids = $_SESSION['loginid'].",".join(',',$children);  
				
                if (!empty($HiddenDivsion) && !empty($HiddenSearchTeam)) {  

                    $res = "SELECT teams_info.* FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') and division='$HiddenDivsion' and team_name like '%$HiddenSearchTeam%' order by team_name";
                } else if (!empty($HiddenDivsion) && empty($HiddenSearchTeam)) {
                    $res = "SELECT teams_info.* FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') and division='$HiddenDivsion' order by team_name";
                } else if (empty($HiddenDivsion) && !empty($HiddenSearchTeam)) {
                     $res = "SELECT teams_info.* FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') and team_name like '%$HiddenSearchTeam%' order by team_name";
                } else {
                   $res = "SELECT * FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids)) $status1 and (sport_id='$SportId') order by team_name";
                }
            } else {

				$hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
				if($hdn_status=="active"){
						$status= "and teams_info.status='1'";
						$status1= "and status='1'";
				} if($hdn_status=="Inactive") {
						$status= "and teams_info.status='0'";
						$status1= "and status='0'";
				}

                if (!empty($HiddenSeson) && !empty($HiddenSearchTeam)) {   

                    $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where $tablename.season='$HiddenSeson' and teams_info.team_name like '%$HiddenSearchTeam%' and teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";
                } else if (!empty($HiddenSeson) && empty($HiddenSearchTeam)) {
                    $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where $tablename.season='$HiddenSeson' and teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";
                } else if (empty($HiddenSeson) && !empty($HiddenSearchTeam)) {
                    $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.team_name like '%$HiddenSearchTeam%' and teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";
                } else {
                    $res = "select teams_info.* from teams_info LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";

                }

                // $res = "select * from teams_info INNER JOIN $tablename ON teams_info.id=$tablename.teamcode where $tablename.season='$DefaultSeason'";
            }
        // }

    }
} else {
		
    if ($_SESSION['master'] == 1) { 
		$hdn_status = $_POST["hnd_status"]?$_POST['hnd_status']:"active";
		if($hdn_status=="active"){
				$status= "and teams_info.status='1'";
				$status1= "and status='1'";
		} if($hdn_status=="Inactive") {
				$status= "and teams_info.status='0'";
				$status1= "and status='0'";
		}
        $children = array($_SESSION['childrens']);
		
        
        //$ids = join(',',$children);
		 $ids = $_SESSION['loginid'].",".join(',',$children); 

         $res = "SELECT * FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') order by team_name"; 
    } else {
        $res = "select teams_info.* from teams_info  LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($Cid) group by teams_info.id order by teams_info.team_name";
    } 
}
// $res = "select * from teams_info order by team_name";
// echo $res;
if ($_SESSION['master'] == 1) { ?>
<style>
#sample_1_wrapper table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before{
    display:none;
}
</style>
<?php }

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Team has been added successfully";
        $alert_class = "alert-success";
    }  else if ($_GET['msg'] == 2) {
        $alert_message = "Team has been updated successfully";
        $alert_class = "alert-success";
    } else if ($_GET['msg'] == 3) {
        $alert_message = "Team has been deleted successfully";
        $alert_class = "alert-danger";
    }
    else {
        $alert_message = "Something wrong!!";
        $alert_class = "alert-danger";
    }
}

/****Paging ***/
$Page = 1;$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
/*End of paging*/

include_once('header.php'); ?>
<!-- <link href="assets/custom/css/playerlist.css" rel="stylesheet" type="text/css" /> -->
<link href="assets/custom/css/teamlist.css" rel="stylesheet" type="text/css" />
<style type="text/css">

table.dataTable.no-footer {
    border-bottom: 0px solid #111; 
}
table.dataTable{
    border-collapse: collapse;
}
</style>
<div class="page-content-wrapper">
        <div class="page-content">
            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>
            <div class="row searchheder">                
                <form method="post" id="searchteamform">
                <div class="col-md-9 searchbarstyle" >
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group ">
                            <input type="hidden" name="sportid" id="sportid" value="<?php echo $SportId ?>">
                            <input type="hidden" name="customerid" id="customerid" value="<?php echo $Cid ?>">
                            <?php
                            if ($_SESSION['master'] != 1) { 

                                $SeasonRes = $conn->prepare("select distinct(season) from $tablename where customer_id=:cid and season<>''");
                                $SeasonResArr = array(":cid"=>$Cid);
                                $SeasonRes->execute($SeasonResArr);
                                $SeasonResCnt = $SeasonRes->rowCount(); ?> 

                                <select class="form-control border-radius" id="searchbyseasonid" name="seasonid">
                                <?php if ($SeasonResCnt > 0) {?>
                                    <option value="">Select season</option>
                                    <?php
                                    $FetchSeason = $SeasonRes->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($FetchSeason as $SeasonRow) { 
                                        $SeasonName = json_decode(getSeasonName($SeasonRow['season']), true);
                                        ?>                                    
                                    <option <?php echo ($SeasonRow['season'] == $HiddenSesid) ? "selected" :"" ;?> value="<?php echo $SeasonRow['season']; ?>"><?php echo $SeasonName['name']; ?></option>
                                    <?php }
                                }?>
                                </select>
                            <?php } else {
                           
                                $children = array($_SESSION['childrens']);
                                $ids = $_SESSION['loginid'].",".join(',',$children);
                                $Dres = $conn->prepare("select * from customer_division where custid in ($ids)");
                                $Dres->execute();
                                $Cntdres = $Dres->rowCount(); ?>
                                <select class="form-control border-radius" id="searchbydivisionid" name="divisionid">
                                <option value="">Select Division</option>
                                <?php 
                                if ($Cntdres > 0) {
                                    $FetchDivision = $Dres->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($FetchDivision as $drow) { ?>
                                        <option <?php echo ($drow['id'] == $HiddenDivid) ? "selected" :"" ;?> value="<?php echo $drow['id']; ?>"  ><?php echo $drow['name']; ?></option>
                                    <?php }
                                }                                
                            }
                            ?>
                            </select>
                        </div>
                    </div>
					
                    <div class="col-md-4 col-sm-4 col-xs-12 removerightpadding">
                        <form class="search-form search-form-expanded" >
                            <div class="form-group">
                                <input type="text" id="searchtext" class="form-control border-radius" placeholder="Search by Team name" name="query" value="<?php echo $HiddenSearchtext ?>">
                            </div>
                        </form>
                    </div>
					<div class="col-md-2 col-sm-2 col-xs-12 removerightpadding">
                        <form class="search-form search-form-expanded" >
                           <div class="form-group ">
								<div class="form-group">
									<select class="form-control  player_form border-radius" name="active_status" id="searchbystatus"> 
										<option value="all">All Team</option>
										<option value="active">Active</option>
										<option value="Inactive">InActive</option>
									</select>
								   <?php if($hdn_status!=""){$active=$hdn_status;} else{$active="active";} ?>
									<script>$("#searchbystatus").val("<?php echo $active;?>")</script> 
								</div>
							</div> 
                        </form>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 searchrightpadding">
                        <div class="form-group">
                            <!-- <input type="button" id="searchbtn" class="btn btn-secondary " value="Search" name="search"> -->
                            <input type="button" id="resetbtn" class="btn btn-danger " value="Reset" name="reset">
                        </div>
                    </div>
                </div>                
                </form>
            </div>            
            <div class="row">
                <div class="col-md-12">                    
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                            LIST OF TEAMS                       
                            </h3>
                            <div class="pull-right">                                
                                <?php if (isset($_GET['sport'])) { ?>
                                <input type="button" class="btn btn-small addcustomerbtn" onclick="document.location='manage_team.php?sport=<?php echo $SportName; ?>'" value="Add Team" style="margin-right:14px;border-radius: 4px !important;"> 
                                <?php }  else { ?><?php
                                if ($CntSportsLists == 1) {
                                    if($Sports[0]=='4444') { $ls = 'basketball'; } 
                                    if($Sports[0]=='4443') { $ls = 'football'; } 

                                    if($Sports[0]=='4441') { $ls = 'baseball'; } 
                                    if($Sports[0]=='4442') { $ls = 'softball'; } 
                                ?> <input type="button" class="btn btn-small addcustomerbtn" onclick="document.location='manage_team.php?sport=<?php echo $ls; ?>'" value="Add Team" style="margin-right:14px;border-radius: 4px !important;font-size:13px;"> 
                                <?php } } ?>                  
                                
                            </div>
                        </div>
                        <div class="loadingsection">
                            <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
                        </div>

                        <div class="portlet-body">
                            <div class="table-responsive" id="ajaxteamlist" >
                                <form id="team_list" name="team_list" method="post" >
                                <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                                <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                                <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                                <input type="hidden" name="hdndivid" id="hdndivid" value="<?php echo $HiddenDivid ?>">
                                <input type="hidden" name="hdnsesid" id="hdnsesid" value="<?php echo $HiddenSesid ?>">
                                <input type="hidden" name="hdnsearchteam" id="hdnsearchteam" value="<?php echo $HiddenSearchtext ?>">
								<input type="hidden" name="hnd_status" id="hnd_status" value="<?php echo $hnd_status;?>">
                             
                                <table class="table table-striped table-bordered table-hover dataTable no-footer dataTable customerlist-tbl" id="sample_1" sytle="border: 1px solid #CCC;border-collapse: collapse;">
                                    <thead>
                                    <tr>
                                        <th nowrap> Team&nbsp;ID </th>
                                        <th nowrap> Team Name </th>
                                        <th nowrap> Print name </th>
                                        <th nowrap> Abbr </th>
                                        <th nowrap> Division </th>
                                        <th nowrap> Venue </th>
										<th nowrap> Video </th>
                                        <th nowrap> Team&nbsp;Logo </th>
                                        <th nowrap> Action </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $dbQry = $res;
                                            $getResQry      =   $conn->prepare($dbQry);
                                            $getResQry->execute();
                                            $getResCnt      =   $getResQry->rowCount();
                                            $getResQry->closeCursor();
                                            if ($getResCnt > 0) {
                                                $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                                $Start=($Page-1)*$RecordsPerPage;
                                                $sno=$Start+1;                                        
                                                $dbQry.=" limit $Start,$RecordsPerPage";
                                                //echo $HiddenSearchtext;
												//echo $dbQry;
                                                $getResQry      =   $conn->prepare($dbQry);
                                                $getResQry->execute();
                                                $getResCnt      =   $getResQry->rowCount();

                                                if($getResCnt>0){
                                                    $getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
                                                    $getResQry->closeCursor();
                                                    $s=1;
                                                    foreach($getResRows as $team){
                                                    $isSuspended=$team['isSuspended'];
                                                    if($isSuspended=="1"){
                                                        $background="background-color:#D3D3D3 !important;";

                                                    } else {
                                                        $background="";
                                                    }
                                                            
                                        ?>
                                        <tr class="odd gradeX" style="<?php echo $background; ?>">
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['id']; ?></td>
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['team_name']; ?>
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['print_name']; ?></td>
                                            <td nowrap style="<?php echo $background; ?>"><?php echo $team['abbrevation']; ?></td>
                                            <td nowrap style="<?php echo $background; ?>">
                                            <?php 
                                            $divid= $team['division'];
                                            $orgQry = $conn->prepare("select * from customer_division where id=:divid");        
                                            $QryArr = array(":divid"=>$divid);        
                                            $orgQry->execute($QryArr);                                        
                                            $DivName = $orgQry->fetch(PDO::FETCH_ASSOC);
                                            echo $DivName['name'];
                                            ?></td>
											<!-- <td nowrap><?php echo $team['coach']; ?></td>
                                            <td nowrap><?php echo $team['Cphone']; ?></td>
                                            <td nowrap><?php echo $team['CEmail']; ?></td> -->
                                            <td nowrap style="<?php echo $background; ?>" ><?php echo $team['stadium']; ?></td>
											<td nowrap style="<?php echo $background; ?>"><?php 
											$teamid=$team['id'];
											//$custid=$team['customer_id'];
											$custid=$Cid;
											$custvideoQry=$conn->prepare("select * from customer_tv_station where team_id=:team_id and customer_id=:customer_id");
											$custQryArr = array(":team_id"=>$teamid,":customer_id"=>$custid); 
											$custvideoQry->execute($custQryArr);                                        
                                            $Video = $custvideoQry->fetch(PDO::FETCH_ASSOC);
											//echo $Video['team_id'].'-'.$Video['customer_id'];
											$videoactive=$Video['active'];
											if($videoactive=='1'){
											$videostatus='Y';
											}else{
											$videostatus='N';
											}
											echo $videostatus;
											?></td>
                                            <td nowrap style="<?php echo $background; ?>">
                                            <?php if($team['team_image']!=""){ ?>
                                                <img src="uploads/teams/<?php echo $team['team_image']; ?>" alt="" width="25" height="25" />
                                            <?php } ?>
                                            </td>
                                            <td nowrap style="<?php echo $background; ?>">
                                            <?php 
                                            $GameQryArr = array();
                                            $teamid = $team['id'];
                                            $GameQry = $conn->prepare("select * from teams_info where id=:teamid");
                                            $GameQryArr = array(":teamid"=>$teamid);
                                            $GameQry->execute($GameQryArr);
                                            $CntGame = $GameQry->rowCount();
                                            if ($CntGame > 0) {

                                                $GameRows = $GameQry->fetch(PDO::FETCH_ASSOC);
                                                $customer_id = $GameRows['customer_id'];
                                                $sportid = $GameRows['sport_id'];
                                            }

                                            $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
                                            $sportnquery->execute();
                                            $Cntsportnquery = $sportnquery->rowCount();
                                            if ($Cntsportnquery > 0) {
                                                $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);
                                                foreach ($sportrows as $sportnames) {
                                                    $teamsportname = $sportnames['sport_name']; 
                                                    $teamsportname = strtolower($teamsportname);
                                                }
                                            } else {
                                                $teamsportname = "";
                                            }
                                            ?>
                                                <table class="table-hover">
                                                    <tr style="<?php echo $background; ?>">
                                                        <td nowrap class="teamaction" style="<?php echo $background; ?>">
                                                        <a href="manage_team.php?tid=<?php echo base64_encode($team['id']); ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Edit
                                                            
                                                        </a>
                                                        </td>
                                                        <td nowrap class="teamaction" style="<?php echo $background; ?>">   
                                                        <a  class="btn btn-xs btn-danger" onclick="return deleteTeam('<?php echo $team['id']; ?>','<?php echo $teamsportname; ?>');"><i class="fa fa-trash"></i> Delete
                                                            
                                                        </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                        <?php
                                            $s++;
                                           }
                                        } else{
                                           // echo "<td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td>";
                                        }
                                        }
                                        else{
                                            // echo "<tr><td colspan='8' style='text-align:center;line-height:1.7em;'>No Team(s) found.</td></tr>";
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                                <?php
                                    if($TotalPages > 1){

                                    echo "<tr><td style='text-align:center;' colspan='8' valign='middle' class='pagination'>";
                                    $FormName = "team_list";
                                    require_once ("paging.php");
                                    echo "</td></tr>";

                                    }
                               ?>
                            </div>
                        </div>
                    
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>
<script src="assets/custom/js/teamlist.js" ></script>

<script>
$(document).ready(function() {
    
    <?php 
    if ($_SESSION['master'] != 1) { ?>

        $( "#resetbtn" ).click(function() {        
            document.location='team_list.php';
            // $('#searchtext').val('');
            // ajaxTeamList();
        });

        $( "#searchtext" ).keyup(function() {
            ajaxTeamList();
        });

        $( "#searchbyseasonid" ).change(function() {
            ajaxTeamList();
        });
		$( "#searchbystatus" ).change(function() {
            ajaxTeamList();
        });
        
    <?php
    } else { ?>
        
        $( "#resetbtn" ).click(function() {        
            document.location='team_list.php';
            // $('#searchtext').val('');
            // ajaxABATeams();
        });

        $( "#searchtext" ).keyup(function() {
            ajaxABATeams();
        });

        $( "#searchbydivisionid" ).change(function() {
            ajaxABATeams();
        });
		$( "#searchbystatus" ).change(function() {
            ajaxABATeams();
        });
    <?php }?>

    function ajaxTeamList() {

        $(".loadingsection").show();
        $('#ajaxteamlist').hide();
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
        var season = $('#searchbyseasonid').val();
		var status = $('#searchbystatus').val();
        $("#hdnsesid").val(season);  
        var cid             = '<?php echo $Cid ?>';
        var sportid = $("#sportid").val();
        $("#hdnsearchteam").val($("#searchtext").val());

        var searchteam = '';
        // if (season != '') {
            searchteam  = $("#searchtext").val();
            $("#hdnsearchteam").val(searchteam);
        // }        

        $.ajax({
            url:"filter_teams.php",  
            method:'GET',
            data:{season: season, cid:cid, ls:'<?php echo $tablename; ?>', searchbyteam:searchteam, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage, sportid:sportid,status:status},
            success:function(data) {
                $(".loadingsection").hide();
                $('#ajaxteamlist').show();
                $('#ajaxteamlist').html('');
                $('#ajaxteamlist').html(data);

                // document.getElementById('ajaxteamlist').innerHTML = data;                   
                // $('.table-header').remove();
                loading_sorting();
            }
        });
    }

    function ajaxABATeams() {

        $(".loadingsection").show();
        $('#ajaxteamlist').hide();
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
        var division = $('#searchbydivisionid').val();
		var status = $('#searchbystatus').val();
        $("#hdndivid").val(division);
        var cid             = '<?php echo $Cid ?>';
        var sportid = $("#sportid").val();
        $("#hdnsearchteam").val($("#searchtext").val());

        var searchteam = '';
        // if (division != '') {
            searchteam  = $("#searchtext").val();
            // $("#hdnsearchteam").val(searchteam);
        // }        
        $.ajax({
            url:"filter_aba_teams.php",  
            method:'GET',
            data:{division: division, cid:cid, ls:'<?php echo $tablename; ?>', searchbyteam:searchteam, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage, sportid:sportid, status:status},
            success:function(data) {
                $(".loadingsection").hide();
                $('#ajaxteamlist').show();
                $('#ajaxteamlist').html('');
                $('#ajaxteamlist').html(data);

                // document.getElementById('ajaxteamlist').innerHTML = data;                   
                // $('.table-header').remove();

                loading_sorting();
            }
        });
    } 

    $('#sample_1').DataTable({
         "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
        "aaSorting": [[0,'desc']],
        "language": {
            "zeroRecords": "No Team(s) found.",
            "infoEmpty": "No Team(s) found."
        },            
         "aoColumnDefs": [ { "bSortable": false, "aTargets": [8] } ], 
    });   

});

function loading_sorting() {  
    $('#sample_1').DataTable({
         "retrieve": true,
        "paging": false,
        "bInfo": false,
       "bFilter":false,
        "bLengthChange":false,
        "bPaginate":false,
        "aaSorting": [[0,'desc']],
        "language": {
            "zeroRecords": "No Team(s) found.",
            "infoEmpty": "No Team(s) found."
        },
        "aoColumnDefs": [ { "bSortable": false, "aTargets": [8] } ], 
    });
}
</script>


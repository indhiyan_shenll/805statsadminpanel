<?php
include_once('session_check.php');
include_once('connect.php'); 


if(isset($_POST['teamid'])){
	$SeasonId      = $_POST['seasonid'];
	$divisionid    = $_POST['divisionid'];
	$conferenceid  = $_POST['conferenceid'];
	$teamid		   = $_POST['teamid'];
	$PostType      = $_POST['post_type'];
		
	/*echo "select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.conference_id=$conferenceid and season_id=$SeasonId and division_id=$divisionid and seasonplayer.team_id=$teamid and seasonplayer.isdelete=0 and seasonplayer.status=1";
	exit;*/

	$QryExeDiv = $conn->prepare("select * from customer_team_player as seasonplayer LEFT JOIN player_info as playertbl ON  seasonplayer.player_id=playertbl.id where seasonplayer.conference_id=:conference_id and season_id=:season_id and division_id=:division_id and seasonplayer.team_id=:team_id and seasonplayer.isdelete=0 and seasonplayer.status=1");

	$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId,":division_id"=>$divisionid,":team_id"=>$teamid);
	$QryExeDiv->execute($QryarrCon);
	$QryCntSeasonconf = $QryExeDiv->rowCount();
	$responseHtml = '';
	$AssignPlayerArr  = array();

	if($PostType=='selectplayertbl'){
		$responseHtml .= "<table class='table assignplayertbl'>";
		if($QryCntSeasonconf>0){
				
				while ($rowPlayer = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){			
						$AssignPlayerArr  = $rowPlayer['id'];
						
						$src = 'http://localhost/805statsadminpanel/uploads/players/thumb/'.$rowPlayer['image'];
						$PlayerImg='';
						if (@getimagesize($src)) {
							$PlayerImg   = "<img src='$src' style='height:40px;width:40px;'>";
						}else{
							$PlayerImg   = "<img src='images/defaultplayer.png' style='height:40px;width:40px;'>";
						}
						$responseHtml .= "<tr id='player_".$rowPlayer['id']."'><td class='playerimgtbl'>".$PlayerImg."</td><td class='playernamevalgin'>".$rowPlayer['firstname']." ".$rowPlayer['lastname']."</td><td><p class='playeractionwrap'><span date-playerid='".$rowPlayer['id']."' teamid='".$teamid."' seasionid='".$SeasonId."' class='removeplayerteam'>Remove</span><span data-playerid='".$rowPlayer['id']."' teamid='".$teamid."' seasionid='".$SeasonId."'  divisionid='".$divisionid."' conferenceid  = '".$conferenceid."' class='updateplayer'>Update</span><span data-playerid='".$rowPlayer['id']."' class='switchteam'  teamid='".$teamid."'>Switch Team</span></p></td></tr>";		
				}
				

		}else{
			$responseHtml .= "<tr><td>No players for this team</td></tr>";
		}
		
		$Playeroptions =$AssignedOtions = $SelectedTeamPlayer = $PlayeroptionsNot='';
		$Qry		= $conn->prepare("select teamplayertbl.id as tmplid,playertbl.id,playertbl.firstname,playertbl.lastname,teamplayertbl.team_id as assigned,teamplayertbl.isdelete,SUM(teamplayertbl.status) as pstatus from player_info as playertbl left join customer_team_player as teamplayertbl on playertbl.id=teamplayertbl.player_id where playertbl.customer_id=105 and playertbl.isActive=1 and playertbl.lastname!='TEAM' group by playertbl.id ORDER BY `pstatus` DESC");		
		
		$Qryarr		= array(":custid"=>$customerid);
		$Qry->execute($Qryarr);
		$QryCntSeason = $Qry->rowCount();							
		$Inc =0;
		
		if ($QryCntSeason > 0) {
			while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){	
				
				if(($row['assigned']!=NULL)&&($row['assigned']!=$teamid) && ($row['isdelete']!=1)){
					$PlayeroptionsNot .="<option value='".$row['id']."' disabled>".$row['firstname']." ".$row['lastname']."</option>";
				}else if(($row['assigned']==$teamid)&&($row['isdelete']==0)){
					$SelectedTeamPlayer .="<option value='".$row['id']."' selected>".$row['firstname']." ".$row['lastname']."</option>";
				}else{					
					$AssignedOtions .= "<option value='".$row['id']."'>".$row['firstname']." ".$row['lastname']."</option>";
				}
			}
		}else{
			$Playeroptions .= "<option value=''>No season found</option>";
		}
		$responseHtml .= "</table>#####";

		$responseHtml .='<form name="addplayersform" id="playerfrm" method="POST" class="form-horizontal" novalidate="novalidate">
		 
		<input type="hidden" name="teamid" id="teamidhidden" value="'.$teamid.'" /> 
		<div class="col-md-12 formcontainer" style="margin:auto;float:none;">
			<div class="row">
				<div class="col-sm-5">
					<select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">'.$Playeroptions.$AssignedOtions.$PlayeroptionsNot.'</select>
				</div>				
				<div class="col-sm-2" style="margin-top: 110px;">
					<button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
					<button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
					<button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
					<button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
				</div>				
				<div class="col-sm-5">
					<select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">'.$SelectedTeamPlayer.'</select>		 
					<div class="row">
						<div class="col-sm-6">
							<button type="button" id="multiselect_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
						</div>
						<div class="col-sm-6">
							<button type="button" id="multiselect_move_down" class="btn btn-block col-sm-6" style="float: left;"><i class="glyphicon glyphicon-arrow-down" ></i></button>
						</div>
					</div>
				</div>
			</div>			
			
			<div class="form-group col-md-12 ">										
				<input class="btn addnewplayerbtn" type="button" value="Submit">
				<button class="btn cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width="100%" id="loadingplayer"><tr><td align="center"><img src="assets/custom/imgs/loading.gif" style="margin-right: 10px;width: 75px;"></td><tr><td align="center" style="font-size:15px;color:green;">Assign players to team... Please wait...</td></tr></table>

		<table width="100%" id="playerstsmsg"><tr><td align="center" style="font-size:15px;color:green;">Players assigned to team successfully..</td></tr></table><script type="text/javascript">jQuery(document).ready(function($) {$("#multiselect").multiselect({sort:false,search: {
            left: \'<input type="text" name="q" class="form-control searchteambox" placeholder="Search Player" /><label>Select Players</label>\',
            right: \'<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>\',
        },});});</script>';
		
		echo $responseHtml;

	}else if($PostType=='selectplayeroption'){			
		if($QryCntSeasonconf>0){				
			while ($rowPlayer = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){
				echo "<option value='".$rowPlayer['id']."'>".$rowPlayer['firstname']." ".$rowPlayer['lastname']."</option>";					
			}				
		}
	}
	
	exit;
}
?>
<?php
/********************************************
  File Name		: paging.php
  Created Date	: Aug 19, 2009

  Modified Date	: Oct 30, 2009
  Notes
  =====
	1. This file contains the paging code.
********************************************/
?>
<style>
.Pagetext
{
	font-family:Arial;
	font-size:11px;
	color:#FF82FF;

	font-weight:bold;
}
.Pagetext1
{
   	font-family:Arial;
	font-size:10px;
	color: #636363;
	font-weight:bold;
}
a.PageLink:link {font-size:13px;font-weight:bold;font-family:Arial;text-decoration:none; width:20px;}
a.PageLink:visited {font-size:13px;font-weight:bold;font-family:Arial;text-decoration:none; width:20px;}
a.PageLink:active {font-size:13px;font-weight:bold;font-family:Arial;text-decoration:none; width:20px;}
a.PageLink:hover {font-size:13px;font-weight:bold;font-family:Arial;text-decoration:none; width:20px;}

a.PageLink { background:url('images/page_normal_bg.png') no-repeat center center; font-size: 13px; color:#636363; text-decoration: none;width:29px;height:29px;display: inline-block;}
a.PageLink:link { padding: 0 5px;  text-decoration: none;width:20px;}
a.PageLink:hover, a.PageLink:active{ background:url('images/page_hover.png') no-repeat center center; color:#636363; width:20px; }
.currentpage11{background:url('images/current_page.png') no-repeat center center;color:#636363 !important;cursor: default;font-size: 12px; width:28px;height:28px; padding: 0 5px;display: inline-block;}

/*.disablelink{background-color: white;cursor: default;color: #929292;border:1px solid #929292;font-weight:normal !important; font-size:11px; }
*/
.disablelink { background:url('images/page_normal_bg.png') no-repeat center center;padding: 0 5px; width:29px;height:29px;display: inline-block; text-decoration: none;color: #929292;width:20px; font-size:11px;opacity:.5;}
.pagingTable td{border:none !important;background:none;}
</style>
<table border="0" cellpadding="0" cellspacing="0" align='center' class='pagingTable'>
	<tr><td height='10px'></td></tr>
	<tr>
	<!-- Paging code starts here -->
	<?php
	$CurrentPage=$Page;
	if ($TotalPages > 1)
	{	
	?>
		<td class="Pagetext1" align="center" style='padding:0px 3px 0px 5px;'>PAGE  <B>[ <?php echo $CurrentPage?> of <?php echo $TotalPages?> ]</B></td>
	<?php	
		if($CurrentPage==1)
		{
			echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 12px;'><input type='button' class='btn btn-small btn-secondary' value='|<' disabled></td>";
			
		}
		else
		{
			echo "<td class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='|<' onclick=\"pageTransferAllrr(1,'$FormName')\"></td>  ";
			
		}
		$cp=$CurrentPage;

		if($cp<=10)
		{
			echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='<<' disabled></td>";
			
		}
		else
		{
			$cp_previous = $cp-10;
			echo "<td class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='<<' onclick=\"pageTransferAllrr($cp_previous,'$FormName')\"></td>";
			
		}

		if ($cp<=1)
			echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='<' disabled></td>";
		else
		{
			$cp--;
			echo "<td class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='<' onclick=\"pageTransferAllrr($cp,'$FormName')\"></td>";
		}
		for($i=1;$i<=10;$i++)
		{
			$disp_i=$i+(10*floor(($CurrentPage)/10));
			if($CurrentPage>=10)
			{
				$disp_i=$disp_i-5;
				if($CurrentPage>=15 && $CurrentPage%10>=5)
				{
					$disp_i=$disp_i+5;
				}
			}

			if($disp_i<=$TotalPages)
			{
				if($disp_i==$CurrentPage)
					echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-inverse' value='".$disp_i."' disabled></span></td>";
				else 
				{
					echo "<td align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-inverse' value='".$disp_i."' onclick=\"pageTransferAllrr($disp_i,'$FormName')\"></td>";
				}
			}
		}

		$forward=$CurrentPage;
		if ($forward>=$TotalPages)
			echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>' disabled></td>";
		else
		{
			$forward_page=$CurrentPage+1;
			echo "<td class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>' onclick=\"pageTransferAllrr($forward_page,'$FormName')\"></td>";
		}

		if ($forward>=$TotalPages-9)
		{
			echo "<td  align='center' height='16px' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>>' disabled></td> ";
		}
		else
		{
			$forward_next=$forward+10;
			echo "<td class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>>' onclick=\"pageTransferAllrr($forward_next,'$FormName')\"></td>";
		}
		
		if($CurrentPage==$TotalPages)
		{
			echo "<td class='PageLink' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>|' disabled></td>";
		}
		else
		{
			echo "<td  class='Pagetext' align='center' style='padding:0px 2px 0px 2px;'><input type='button' class='btn btn-small btn-secondary' value='>|' onclick=\"pageTransferAllrr($TotalPages,'$FormName')\"></td> ";
		}
	}
	?>
	<!-- paging code ends here --> 
	</tr>
	<tr><td height='5px'></td></tr>
</table>

<script>
function pageTransferAllrr(pagenumber,formname)
{
	with(document.forms[formname])
	{		
		
		HdnPage.value=pagenumber;
		HdnMode.value="paging";
		action.value = "";
		submit();
	}
}
</script>
<?php 
include_once('session_check.php'); 
include_once("connect.php");


include_once('header.php');?>
<link href="assets/custom/css/divisionlist.css" rel="stylesheet" type="text/css" />
<style>
.modal-body{
	overflow:auto;
}
#division_name-error,#rulelist1-error,#rulelist-error,#division_name1-error,.error{
	color:red;
}
</style>
<style>
div#spinner
{
    display: none;
    width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
    overflow: auto;
}    
</style>
<?php

// $custqry = $conn->prepare("select * from customer_info where user_type=:user_type");        
$customer_id="";
if($_SESSION['loginid']!='')  {
  if($_SESSION['usertype']=='user') {
   $customer_id=$_SESSION['loginid'];
  }
 
}
if($_SESSION['childrens']){
	$childrens_id=$_SESSION['childrens'];
	$children = array($_SESSION['childrens']);
	array_push($children,$customer_id);
	$ids = join(',',$children); 

}else{
	$ids=$customer_id;
}

    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$urlArr= explode('=',$actual_link);	

if(isset($_POST["hnd_team_id"])){
	$hdn_team_id = $_POST["hnd_team_id"];
}
	
	
//Get customer_subscribed_sports data Start here

$sports[]=array();
$sportslists = "select * from customer_subscribed_sports where customer_id=:cid";
$sportslistsqry = $conn->prepare($sportslists);
$sportslistsqry->execute(array(":cid"=>$customer_id));
$soprts_Count = $sportslistsqry->rowCount();
if($soprts_Count>0){
    $getResSports     =   $sportslistsqry->fetch();
    foreach($getResSports as $sportlist)
    {
        $sports[]= $getResSports['sport_id']; 
    }
}

//Get customer_subscribed_sports data End here
//$sportname='basketball';
//Get Sport details start here
if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
      $getSportsRow=$get_sport_qry->fetch();
      $sportid= $getSportsRow['sportcode'];
    }
}
else{
	
    $sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
}
// Get sports End Here
	
$condition = "";
$limit="";
$srchArr   = array();
if(isset($_POST['searchbtnpost'])){
   $divisionname= $_POST['divisionname'];   
   $condition  .=" and name like '%$divisionname%' ";
   $srchArr[":srch_name"]    = "%".$divisionname."%";
}
if(isset($_POST['hnd_team_id'])){
   $divisionname= $_POST['hnd_team_id'];   
   $condition  .=" and name like '%$divisionname%' ";
   $srchArr[":srch_name"]    = "%".$divisionname."%";
}


$alert_message = '';
$alert_class = '';

if (isset($_GET['msg'])) { 
    if ($_GET['msg'] == 1) {
        $alert_message = "Added new division successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 2) {
        $alert_message = "Updated new  division successfully";
        $alert_class = "alert-success";
    } else if($_GET['msg'] == 3) {
        $alert_message = "Division deleted successfully";
        $alert_class = "alert-success";
    }
}

/****Paging ***/
$Page = 1;$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
/*End of paging*/
?>

    <div class="page-content-wrapper">
        <div class="page-content">

            <div class="row">
                <div class="col-md-12">
                    <form action="" method="post" id="searchplayedform">
                        <input type="hidden" name="customerid" id="customerid" value="<?php echo $cid ?>">
                        <input type="hidden" name="sportid" id="sportid" value="<?php echo $sportid ?>">

                        <div class="portlet light col-md-5" style="padding: 15px 15px 0px;background: #FFF;border: 1px solid #CCC;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px !Important;    float: right;margin-bottom:10px;">
                            <div class="portlet-title " style="border-bottom: 0px solid #eee;">                            
                                <div class="col-md-12" style="padding:0px">
                                    <div class="col-md-10 col-sm-10 col-xs-12 searchboxstyle" style="padding-right:0px">
                                        <div class="form-group">
                                            <input class="form-control border-radius" type="text" name="divisionname"  placeholder="Division Name" id="manage_division_search" value="<?php echo $divisionname; ?>"> 
                                        </div>
                                    </div>
									<div class="col-md-2 col-sm-2 col-xs-12 resetbtnstyle" style="text-align: right;padding: 0px ;">
                                        <div class="form-group">
                                            <a class="btn btn-danger resetbtn">Reset</a> 
                                        </div>
										 <?php if($hdn_team_id!=""){
											$divisionids=$hdn_team_id;}
										 ?>
                                        
										</div>

                                    <!--  <div class="col-md-4 col-sm-4 col-xs-12 " style="text-align: right;padding: 0px ;">
                                        <div class="form-group">
                                            <input type="submit" id="searchbtn" name="searchbtnpost" class="btn btn-secondary " value="Search" name="search">
                                            <a href="manage_division.php" class="btn btn-danger resetbtn">Reset</a> 
                                        </div>
                                                                         </div> -->
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                 </div><!--Col-md-12 -->
            </div> <!--row -->

            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>            
          
            <div class="row">
                <div class="col-md-12">                    
                    <!-- <div class="portlet light col-md-5 col-sm-5 col-xs-12 portletbg" >
                        <div class="portlet-title ">
                            <div class="col-md-12 removerightpadding">
                                <div class="caption font-red-sunglo col-md-3 col-sm-3 col-xs-3 searchtext">
                                    <i class="icon-search font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase">Search</span>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-8 removerightpadding">                                       
                                    <div class="col-md-12 col-sm-12 col-xs-12 removerightpadding">
                                        <div class="form-group ">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-radius" id="manage_division_search" placeholder="Search">
                                            </div>
                                        </div> 
                                    </div> 
                               </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">

                        <div class="widget-header"> 
                            <h3>
                            <i class="icon-settings font-red-sunglo"></i>
                             LIST OF DIVISIONS                     
                            </h3>
                            <div class="pull-right">   
								<?php if(isset($_GET['sport'])){ ?>
                                <!-- <input type="button" class="btn btn-small addcustomerbtn" onclick="document.location='edit_division.php?sport=<?php echo $sportname; ?>'" value="Add New" style="margin-right:10px;border-radius: 4px !important;"> -->
								<button type="button" class="btn btn-small addcustomerbtn" data-toggle="modal" data-target="#DivisionModal"  style='margin-right:10px;border-radius: 4px !important;'>Add New</button>
								<input type="hidden" name="sports_name" id="sports_name" value="<?php echo $sportname?>" >
								<?php }else{
									if($soprts_Count>0){
									
										if($sports[1]=='4444') { $ls='basketball'; } 
										if($sports[1]=='4443') { $ls='football'; } 

										if($sports[1]=='4441') { $ls='baseball'; } 
										if($sports[1]=='4442') { $ls='softball'; } 
								?>
								<!--  <input type="button" class="btn btn-small addcustomerbtn" onclick="document.location='edit_division.php?sport=<?php echo $ls; ?>'" value="Add New" style="margin-right:10px;border-radius: 4px !important;"> -->
									<button type="button" class="btn btn-small addcustomerbtn" data-toggle="modal" data-target="#DivisionModal"  style='margin-right:10px;border-radius: 4px !important;'>Add Division</button>
									<input type="hidden" name="sports_name" id="sports_name" value="<?php echo $ls?>" >
								<?php }}?>
                            </div>
                        </div>
						<div class="loadingsection">
							 <img src="images/loading-publish.gif" alt="loadingimage" id="loadingimage">
					</div>
                    <div class="customerlistparent">
                        <form id="customer_list" name="customer_list" method="post" action="">
                        <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                        <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                        <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
						 <input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $hdn_team_id ?>">
                        <!-- <div class="customerlistparent"> -->
                            <?php 
                            // $dbQry = "select * from customer_info where user_type=:user_type";
                            // $getResQry      =   $conn->prepare($dbQry);
                            // $QryArr = array(":user_type"=>"user");
                            // $getResQry->execute($QryArr);
                            // $getResCnt      =   $getResQry->rowCount();
                            // $getResQry->closeCursor();
                            ?>
                            <table class="table table-striped  table-hover dataTable no-footer dataTable" id="divisionlistingtable" >
                                <thead>
                                    <tr>
                                        <th> Division Id </th>
                                        <th> Division Name </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
								$i=1;
                                $dbQry = "select * from customer_division where custid in ($ids)".$condition;
								//echo  $dbQry;
								$getResQry      =   $conn->prepare($dbQry);
                                $QryArr = array(":ids"=>$ids);
                                $getResQry->execute($QryArr);
                                $getResCnt      =   $getResQry->rowCount();
                                $getResQry->closeCursor();
                                if ($getResCnt > 0) {
                                    $TotalPages=ceil($getResCnt/$RecordsPerPage);
                                    $Start=($Page-1)*$RecordsPerPage;
                                    $sno=$Start+1;                                        
                                    $dbQry.=" limit $Start,$RecordsPerPage";
									$getResQry      =   $conn->prepare($dbQry);
                                    $getResQry->execute($QryArr);
                                    $getResCnt      =   $getResQry->rowCount();

                                    if($getResCnt>0){
                                        $getResRows     =   $getResQry->fetchAll();
                                        $getResQry->closeCursor();
                                        $s=1;
										$customer_division_name='';
										$db_division_name =  '';
										$db_division_rule =  '';
                                        foreach ($getResRows as $division) {
											$sportname=$sportname?$sportname:$ls;
											?> 
                                            <tr>
                                                <td><?php echo $division['id']; ?></td>
                                                <td nowrap><?php echo $division['name'] ?></td>
                                                <td>
                                                    <!-- <table>
                                                        <tr>
                                                            <td> -->
																<?php 
															$division_id=$division['id'];
															$division_cust_id=$division['custid'];
															$DivisionQry = $conn->prepare("select * from customer_division where id='$division_id' and custid='$division_cust_id'");
															$DivisionQry->execute(); 
															$CntDivision = $DivisionQry->rowCount();
														   $DivisionResRow = $DivisionQry->fetchAll(PDO::FETCH_ASSOC);
																
															foreach($DivisionResRow as $DivisionRes) {
																
																$customer_division_name =  explode(" - ",$DivisionRes['name']);	$db_division_name =  $customer_division_name[0];
																$db_division_rule =  $customer_division_name[1];
															}
														   
														   ?>
														   <a href="#" id="edit_division" data-id="<?php echo  $division['id'];?>" data-name="<?php echo $db_division_name ?>"
														   data-role="<?php echo $db_division_rule;?>"  data-sports="<?php echo $sportname;?>" data-toggle="modal" class="btn btn-xs btn-success edit_popup"  customerid="<?php echo $division['id']; ?>"><i class="fa fa-pencil"></i> Edit
															</a>
									                          <!--  </td>
									                       </tr>
                                                    </table> -->
                                                </td>
                                            </tr>
                                        <?php $i++; 
											$s++;}
                                        } else {
                                           echo "<tr><td colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
                                        }
                                } else {
                                    echo "<tr><td colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
                                }?>
                                </tbody>
                            </table>
                            
                                <?php
                                if($TotalPages > 1){

                                echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
                                $FormName = "customer_list";
                                require_once ("paging.php");
                                echo "</td></tr>";

                                }
                               ?>
                        </div>
                        </form>
                    </div><!--- portlet-body customerlist-tbl-pr clearfix !-->
                </div><!--- col-md-12 !-->
            </div><!--- row !-->
        </div>
    </div>
</div>
<!-- Division Edit model popup-->
				<div id="EditDivisionModal" class="modal fade" role="dialog">
				
				  <div class="modal-dialog">							
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Division Information<span style="margin-right: 10%;" class="mode-color" id="mode">Edit</span></h4>
					  </div>
					  <div class="modal-body">
					  <form name="editdivision" id="editdivisionfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="edit_division.php">
						<input type="hidden" name="editdivisionnew" id="editdivisionnew" value="Edit">
						<input type="hidden" name="cust_divsion_id" id="cust_divsion_id" value="">
						<input type="hidden" name="division_customer_id" id="division_customer_id" value="<?php echo $customer_id;?>">
						<input type="hidden" name="role_name" id="role_name" value="">
						<input type="hidden" name="role_sports" id="role_sports" value="">
						<div class="col-md-10" style='margin:auto;float:none;'>
							<div class="form-group col-md-12 ">
								<label>Division Name<span class="error">*</span></label>
								<input class="form-control requiredcs border-radius" type="text" name="division_name1" id="division_name1" placeholder="Division Name" value="<?php echo $db_division_name;?>"/> 				
							</div>	
							<div class="form-group col-md-12 ">
								<label>Division Rule<span class="error">*</span></label>
								<select name="division_rulelist1" class="form-control seldivinrule requiredunique border-radius" id="rulelist">
										<option value="">---Select---</option>
										<?php if($sportname=='basketball')
										{ ?>
										<option value="FIBA" <?php echo $db_division_rule == "FIBA" ? "selected":""?>>FIBA</option>
										<option value="HS" <?php echo $db_division_rule == "HS" ? "selected":""?>>HS</option>
										<option value="NBA" <?php echo $db_division_rule == "NBA" ? "selected":""?>>NBA</option>
										<option value="NCAA" <?php echo $db_division_rule == "NCAA" ? "selected":""?>>NCAA</option>
										<option value="NCAAW" <?php echo $db_division_rule == "NCAAW" ? "selected":""?>>NCAAW</option>
										<?php } ?>		
										<?php if($_REQUEST['sport']=='baseball' || $_REQUEST['sport']=='softball')
										{?>
											<option value="HS_BA" <?php echo $db_division_rule == "HS_BA" ? "selected":""?>>HS_BA</option>
											<option value="HS_SB" <?php echo $db_division_rule == "HS_SB" ? "selected":""?>>HS_SB</option>
											<option value="MLB" <?php echo $db_division_rule == "MLB" ? "selected":""?>>MLB</option>
											<option value="NCAA_BA" <?php echo $db_division_rule == "NCAA_BA" ? "selected":""?>>NCAA_BA</option>
											<option value="NCAA_SB" <?php echo $db_division_rule == "NCAA_SB" ? "selected":""?>>NCAA_SB</option>
										<?php } ?>

										<?php if($_REQUEST['sport']=='football')
										{ ?>
											<option value="Arena" <?php echo $db_division_rule == "Arena" ? "selected":""?>>Arena</option>
											<option value="HS" <?php echo $db_division_rule == "HS" ? "selected":""?>>HS</option>
											<option value="Indoor" <?php echo $db_division_rule == "Indoor" ? "selected":""?>>Indoor</option>
											<option value="NCAA" <?php echo $db_division_rule == "NCAA" ? "selected":""?>>NCAA</option>
											<option value="NFL" <?php echo $db_division_rule == "NFL" ? "selected":""?>>NFL</option>
										<?php } ?>
								</select>
							</div>	
							<div class="form-group col-md-12 popupbtn">										
								<input class="btn editnewdivibtn btn-success" type="button" value="Update">
								<button class="btn cancelbtn btn-danger" style="margin-left:15px;" type="button" data-dismiss="modal">Cancel</button>
							</div>	
						</div>
						</form>
					  </div>

					 <!-- <div class="modal-footer">
					 						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					 </div> -->
					</div>
				  </div>
				</div>
<!-- Division Add model popup -->
<div id="DivisionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Division Information <span style="margin-right: 10%;" class="mode-color" id="mode">Add</span></h4>
	  </div>
	  <div class="modal-body">
	  <form name="adddivision" id="divisionfrm" method="POST" class="form-horizontal" novalidate="novalidate" action="add_division.php">
		<input type="hidden" name="adddivisionnew" id="adddivisionnew" value="Add">
		<input type="hidden" name="division_customer_id" id="division_customer_id" value="<?php echo $customer_id;?>">

		<?php $sportname=$sportname?$sportname:$ls; ?>
		<input type="hidden" name="hidden_sportsname" id="hidden_sportsname" value="<?php echo $sportname;?>">
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Division Name<span class="error">*</span></label>
				<input class="form-control requiredcs border-radius" type="text" name="division_name" id="division_name" placeholder="Division Name" /> 				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Division Rule<span class="error">*</span></label>
				<select name="division_rulelist" class="form-control seldivinrule border-radius" id="rulelist1">
					<option value=''>---Select---</option>
					
					<?php if($sportname=='basketball')
						{ ?>
						<option value="FIBA">FIBA</option>
						<option value="HS">HS</option>
						<option value="NBA">NBA</option>
						<option value="NCAA">NCAA</option>
						<option value="NCAAW">NCAAW</option>
						<?php } ?>		
						<?php if($sportname=='baseball' || $sportname=='softball')
						{?>
						<option value="HS_BA">HS_BA</option>
						<option value="HS_SB">HS_SB</option>
						<option value="MLB">MLB</option>
						<option value="NCAA_BA">NCAA_BA</option>
						<option value="NCAA_SB">NCAA_SB</option>
						<?php } ?>

						<?php if($sportname=='football')
						{ ?>
						<option value="Arena">Arena</option>
						<option value="HS">HS</option>
						<option value="Indoor">Indoor</option>
						<option value="NCAA">NCAA</option>
						<option value="NFL">NFL</option>
					<?php } ?>
				</select>
			</div>	
				
			<div class="form-group col-md-12 popupbtn">										
				<input class="btn addnewdivibtn btn-success" type="button" value="Submit" >
				<button class="btn cancelbtn btn-danger" style="margin-left:15px;" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
	  </div>

	 <!-- <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	 </div> -->
	</div>
  </div>
</div>

<?php include_once('footer.php'); ?>


<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL SCRIPTS -->
<!-- <script src="assets/custom/js/addcustomer.js" ></script> -->
<script src="assets/custom/js/managedivision.js" ></script>




   
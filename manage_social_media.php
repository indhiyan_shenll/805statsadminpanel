<?php 
include_once('session_check.php'); 
include_once('connect.php');
include_once('common_functions.php'); 
include_once('usertype_check.php');

$cid = $Cid;
if (isset($_POST['update'])) {
    $Fb = $_POST['fb'];
    $Tw = $_POST['tw'];
    $Ins = $_POST['ins'];
    $Yt = $_POST['yt'];
    $UpdateQry = $conn->prepare("update customer_info set fb=:fb, tw=:tw, ins=:ins, yt=:yt where id=:cid");
    $UpdateQryArr = array(":fb"=>$Fb, ":tw"=>$Tw, ":ins"=>$Ins, ":yt"=>$Yt, ":cid"=>$cid);
    $UpdateQry->execute($UpdateQryArr);
    header('Location:manage_social_media.php?msg=1');
    exit;
}
$SelectQry = $conn->prepare("select fb,tw,ins,yt from customer_info where id=:cid");
$SelectQryArr = array(':cid'=>$cid);
$SelectQry->execute($SelectQryArr);
$CntRcds = $SelectQry ->rowCount();
$Fb = '';
$Tw = '';
$Ins = '';
$Yt = '';
if ($CntRcds > 0) {
    $FertchRcds = $SelectQry->fetch(PDO::FETCH_ASSOC);
    $Fb = $FertchRcds['fb'];
    $Tw = $FertchRcds['tw'];
    $Ins = $FertchRcds['ins'];
    $Yt = $FertchRcds['yt'];
}

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 1) {
        $alert_message = "Updated successfully";
        $alert_class = "alert-success";
    } 
}
include_once('header.php'); ?>
<link href="assets/custom/css/managesocialmedia.css" rel="stylesheet" type="text/css" />
    <div class="page-content-wrapper">
        <div class="page-content">            
            <div class="row">
                <form id="socialmediaform" method="POST">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="col-md-12 left-right-padding">
                            <?php if (isset($_GET['msg'])) { ?>
                            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <p> <?php echo $alert_message; ?> </p>
                            </div>
                            <?php } ?>
                            <div class="portlet light info-caption">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"> Manage Social Media</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 left-padding">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <div class="col-md-8 col-sm-12 col-xs-12 media-auto">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group social-label">
                                                <label>Facebook</label>
                                            </div>                                               
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-facebook-official font-blue"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="fb" value="<?php echo $Fb; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 media-auto">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group social-label">
                                                <label>Twitter</label>
                                            </div>                                               
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-twitter-square font-blue"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="tw" value="<?php echo $Tw; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 media-auto">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group social-label">
                                                <label>Instagram</label>
                                            </div>                                               
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-instagram font-blue"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="ins" value="<?php echo $Ins; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 media-auto">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group social-label">
                                                <label>Youtube</label>
                                            </div>                                               
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-youtube-play font-blue"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="yt" value="<?php echo $Yt; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body bottom-padding mangeteam-form-bottom">
                                        <div class="form-actions form-actions-btn custom-form-action">
                                            <button type="submit" class="btn customgreenbtn" name="update">Update</button>
                                        </div>                                                                                
                                    </div>
                                </div>
                            </div>                    
                        </div>                                               
                    </div>                    
                </form>
            </div>            
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>
<?php 
include_once('session_check.php'); 
include_once('connect.php');
include_once('common_functions.php'); 
include_once('usertype_check.php');

$alert_message = '';
$alert_class = '';
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 2) {
        $alert_message = "Duplicate game!!";
        $alert_class = "alert-danger";
    }
}

$Gid = '';
if (isset($_GET['gid'])) {
    $Gid = base64_decode($_GET['gid']);
}

// print_r($_SESSION);exit;
if (isset($_GET['sport'])) {
// if (isset($_SESSION['sportname'])) {

    $SportName = $_REQUEST['sport'];
    // $SportName = $_SESSION['sportname'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
            $SportId = $QrySportVal['sportcode']; 
        }
    }    
} else {

    $SportQryArr = array();
    $SportQuery = $conn->prepare("select * from games_info where id=:gid");
    $SportQryArr = array(":gid"=>$Gid);
    $SportQuery->execute($SportQryArr);
    $CntSport = $SportQuery->rowCount();
    if ($CntSport > 0) {
        $SportRes = $SportQuery->fetch(PDO::FETCH_ASSOC);
        $SportId = $SportRes['sport_id'];
    }

    $SportQry = $conn->prepare("select * from sports where sportcode='$SportId'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
            $SportName = $QrySportVal['sport_name']; 
        }
    }

}


$GameQryArr = array();
$GameQry = $conn->prepare("select * from games_info where id=:gid");
$GameQryArr = array(":gid"=>$Gid);
$GameQry->execute($GameQryArr);
$CntGame = $GameQry->rowCount();

if ($CntGame > 0) {

    $GameRes = $GameQry->fetch(PDO::FETCH_ASSOC);
    $GameNameDB = $GameRes['game_name'];
    $GameDateDB = $GameRes['date'];
    $GameTimeDB = $GameRes['time'];
    $ZoneDB = $GameRes['zone'];
    $VisitorTeamIdDB = $GameRes['visitor_team_id'];    
    $HomeTeamIdDB = $GameRes['home_team_id'];
    $VisitorTeamInfo = json_decode(getTeamInfo($VisitorTeamIdDB, $Cid), true);
    $VisitorTeamnameDB = $VisitorTeamInfo['team_name'];
    $HomeTeamInfo = json_decode(getTeamInfo($HomeTeamIdDB, $Cid), true);
    $HomeTeamnameDB = $HomeTeamInfo['team_name'];
    $GenderDB = $GameRes['team_type'];
    $LocationDB = $GameRes['location'];
    $DivisionDB = $GameRes['division'];
    $DivisionRuleDB = $GameRes['rule'];
    $GameTypeDB = $GameRes['isLeagueGame'];
    $TournamentDB = $GameRes['tournament'];
    $SeasonDB = $GameRes['season'];
    $EntryModeDB = $GameRes['entrymode'];
    $GameNotesDB = $GameRes['game_notes'];
    $GameLocationDB = $GameRes['game_location'];
    $mode="Edit";

} else {

    $GameNameDB = "";
    $GameDateDB = "";
    $GameTimeDB = "";
    $ZoneDB = "";
    $VisitorTeamnameDB = "";
    $HomeTeamnameDB = "";
    $GenderDB = "";
    $LocationDB = "";
    $DivisionDB = "";
    $DivisionRuleDB = "";
    $GameTypeDB = "";
    $TournamentDB = "";
    $SeasonDB = "";
    $EntryModeDB = "";
    $GameNotesDB = "";
    $GameLocationDB = "";
    $mode="Add";
}


$RuleList = '';
$GresArr = '';
$insetArr = '';

if (isset($_POST['addsubmit'])) {
    
    $GameName = strip_tags($_POST['gamename']);
    $GameDate = strip_tags($_POST['gamedate']);
    $GameTime = strip_tags($_POST['gametime']);
    $Zone = $_POST['zone'];
    $Visitor = strip_tags($_POST['visitor']);
    $Home = strip_tags($_POST['home']);
    $Gender = $_POST['gender'];
    $Location = strip_tags($_POST['location']);
    $DivisionList = $_POST['divisionlist'];
    $GameType = $_POST['gametype'];
    $EntryMode = $_POST['entrymode'];
    $SeasonList = $_POST['seasonlist'];
    $Tournament= strip_tags($_POST['tournament']);
    $GameInfo = strip_tags($_POST['gameinfo']);
    $GameLoc = $_POST['gameloc'];
    if (isset($_POST['rulelist']))
        $RuleList = $_POST['rulelist'];   
    $Year = date("Y");
    if ($RuleList != "") {
        $DivisionList = $DivisionList ." - ".$RuleList;
    } else {
        $DivisionList = $DivisionList;
    }
    

    $TeamvQuery = $conn->prepare("select * from teams_info where team_name =:visitor");
    $TeamvQuery->execute(array(":visitor"=>$Visitor));
    $CntTeamv = $TeamvQuery->rowCount();

    $TeamVid = '';
    if ($CntTeamv > 0) {
        $TeamvRows = $TeamvQuery->fetchAll(PDO::FETCH_ASSOC);
        foreach ($TeamvRows as $Vrow) {
            $Teamvid = $Vrow['id'];
            $Vcustid = $Vrow['customer_id'];
        }    
    } else {
        $Teamvid = "";
    }

    $TeamhQuery = $conn->prepare("select * from teams_info where team_name =:home");
    $TeamhQuery->execute(array(":home"=>$Home));
    $CntTeamh = $TeamhQuery->rowCount();

    $Teamhid = '';
    if ($CntTeamh > 0) {
        $TeamhRows = $TeamhQuery->fetchAll(PDO::FETCH_ASSOC);
        foreach ($TeamhRows as $Hrow) {
            $Teamhid = $Hrow['id'];
            $Hcustid = $Hrow['customer_id'];
        }    
    } else {
        $Teamhid = "";
    }
    if ($mode == "Add") {

        $Gress = $conn->prepare("select * from games_info where (visitor_team_id=:teamvid or home_team_id=:teamhid  or  visitor_team_id=:teamhid or home_team_id=:teamvid) and date=:gamedate and time=:gametime and year=:year");
        $GresArr = array(":teamvid"=>$Teamvid, ":teamhid"=>$Teamhid, ":gamedate"=>$GameDate, ":gametime"=>$GameTime, ":year"=>$Year);
        $Gress->execute($GresArr);    
        $CntGress = $Gress->rowCount();
        if ($CntGress == 0) {
            if ($DivisionList !="" ) {

                // $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
                // $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                // $CntDivisionQuery = $DivisionQuery->rowCount();
                if ($_SESSION['master'] != 1 ) {
                    $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
                    $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                    $CntDivisionQuery = $DivisionQuery->rowCount();
                } else {
                    $children = array($_SESSION['childrens']);
                    $ids = $_SESSION['loginid'].",".join(',',$children);
                    $DivisionQuery = $conn->prepare("select * from customer_division where id ='$DivisionList' OR (name='$DivisionList' and custid  in ($ids))");
                    $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                    $CntDivisionQuery = $DivisionQuery->rowCount();
                }

                if ($CntDivisionQuery > 0) {
                    $DivRows = $DivisionQuery->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($DivRows as $Drow) {
                        $Divid = $Drow['id'];
                    }            
                } else {
                    $InsertDivQuery = $conn->prepare("INSERT INTO customer_division(name,custid) VALUES (:divisionlist,:cid)");
                    $InsertDivQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                    $Divid = $conn->lastInsertId();
                }
            } else {
                $Divid = "";
            }

            if ($SeasonList !="" ) {
                $SeasonQuery = $conn->prepare("select * from customer_season where id =:seasonlist OR (name=:seasonlist and custid=:cid)");
                $SeasonQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
                $CntSeasonQuery = $SeasonQuery->rowCount();
                if ($CntSeasonQuery > 0) {
                    $SeasonRows = $SeasonQuery->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($SeasonRows as $Snrow) {
                        $Snid = $Snrow['id'];
                    }            
                } else {
                    $InsertDivQuery = $conn->prepare("INSERT INTO customer_season(name,custid) VALUES (:seasonlist,:cid)");
                    $InsertDivQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
                    $Snid = $conn->lastInsertId();
                }
            } else {
                $Snid = "";
            }  
            if ($Gender == "1") {
                $Gen = "M";
            } else {
                $Gen = "F";
            } 

            $Ggid = $conn->prepare("select id from games_info order by id desc limit 0,1");
            $Ggid->execute();
            $Ggrow = $Ggid->fetch();    
            $Gg = $Ggrow['id'];$Gg++; 

            $Date = str_replace('/', '', $GameDate);
            $Xmlmatch = "BB_".$Teamvid."_".$Teamhid."_".$Date."_".$Gen; 
            $Xmlquery = $conn->prepare("select * from games_info where new_game_xml like '{$Xmlmatch}%'");
            $Countxml = $Xmlquery->rowCount();
            $NewCountxml= $Countxml +1;
            $Gs = '';
            if ($SportName == 'basketball') {
                $Gs = 'BB';
            }
            if ($SportName == 'football') {
                $Gs = 'FB';
            }
            if ($SportName == 'baseball' || $SportName == 'softball') {
                $Gs = 'BA';
            }
            $Xmlname = $Gs."_".$Teamvid."_".$Teamhid."_".$Date.$Gg.".xml";

            $InsertGame = $conn->prepare("INSERT INTO games_info(sport_id, game_name, date, time, year, zone, visitor_team_id, home_team_id, team_type, game_location, isLeagueGame, entrymode, tournament, game_notes, location, division, season, new_game_xml, home_customer_id, visitor_customer_id) VALUES (:sportid, :gamename, :gamedate, :gametime, :year, :zone, :teamvid, :teamhid, :gender, :gameloc, :gametype, :entrymode, :tournament, :gameinfo, :location, :divid, :snid, :xmlname, :hcustid, :vcustid)");
            $insetArr =  array(":sportid"=>$SportId, ":gamename"=>$GameName, ":gamedate"=>$GameDate, ":gametime"=>$GameTime, ":year"=>$Year, ":zone"=>$Zone, ":teamvid"=>$Teamvid, ":teamhid"=>$Teamhid, ":gender"=>$Gender, ":location"=>$Location, ":gametype"=>$GameType, ":entrymode"=>$EntryMode, ":tournament"=>$Tournament, ":gameinfo"=>$GameInfo, ":gameloc"=>$GameLoc, ":divid"=>$Divid, ":snid"=>$Snid, ":xmlname"=>$Xmlname, ":hcustid"=>$Hcustid, ":vcustid"=>$Vcustid);
            $InsertGame->execute($insetArr);        

            // $_SESSION['gamename'] = $_SESSION['gamedate'] = $_SESSION['location'] = $_SESSION['tournament'] = $_SESSION['gameinfo'] = $_SESSION['gameloc'] = "";
            if($InsertGame){
                header('Location:game_list.php?msg=1');
                exit;
            }

        } else {        
            header('Location:manage_game.php?msg=2');
            exit;        
        }

    } else {
        
        if ($DivisionList !="" ) {

            // $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
            // $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
            // $CntDivisionQuery = $DivisionQuery->rowCount();

            if ($_SESSION['master'] != 1) {
                $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
                $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $CntDivisionQuery = $DivisionQuery->rowCount();
            } else {
                $children = array($_SESSION['childrens']);
                $ids = $_SESSION['loginid'].",".join(',',$children);
                $DivisionQuery = $conn->prepare("select * from customer_division where id ='$DivisionList' OR (name='$DivisionList' and custid  in ($ids))");
                $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $CntDivisionQuery = $DivisionQuery->rowCount();
            }

            if ($CntDivisionQuery > 0) {
                $DivRows = $DivisionQuery->fetchAll(PDO::FETCH_ASSOC);
                foreach ($DivRows as $Drow) {
                    $Divid = $Drow['id'];
                }            
            } else {
                $InsertDivQuery = $conn->prepare("INSERT INTO customer_division(name,custid) VALUES (:divisionlist,:cid)");
                $InsertDivQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $Divid = $conn->lastInsertId();
            }
        } else {
            $Divid = "";
        }

        if ($SeasonList !="" ) {
            $SeasonQuery = $conn->prepare("select * from customer_season where id =:seasonlist OR (name=:seasonlist and custid=:cid)");
            $SeasonQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
            $CntSeasonQuery = $SeasonQuery->rowCount();
            if ($CntSeasonQuery > 0) {
                $SeasonRows = $SeasonQuery->fetchAll(PDO::FETCH_ASSOC);
                foreach ($SeasonRows as $Snrow) {
                    $Snid = $Snrow['id'];
                }            
            } else {
                $InsertDivQuery = $conn->prepare("INSERT INTO customer_season(name,custid) VALUES (:seasonlist,:cid)");
                $InsertDivQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
                $Snid = $conn->lastInsertId();
            }
        } else {
            $Snid = "";
        }  
        if ($Gender == "1") {
            $Gen = "M";
        } else {
            $Gen = "F";
        } 

        $Ggid = $conn->prepare("select id from games_info order by id desc limit 0,1");
        $Ggid->execute();
        $Ggrow = $Ggid->fetch();    
        $Gg = $Ggrow['id'];$Gg++; 

        $Date = str_replace('/', '', $GameDate);
        $Xmlmatch = "BB_".$Teamvid."_".$Teamhid."_".$Date."_".$Gen; 
        $Xmlquery = $conn->prepare("select * from games_info where new_game_xml like '{$Xmlmatch}%'");
        $Countxml = $Xmlquery->rowCount();
        $NewCountxml= $Countxml +1;
        $Gs = '';
        if ($SportName == 'basketball') {
            $Gs = 'BB';
        }
        if ($SportName == 'football') {
            $Gs = 'FB';
        }
        if ($SportName == 'baseball' || $SportName == 'softball') {
            $Gs = 'BA';
        }
        $Xmlname = $Gs."_".$Teamvid."_".$Teamhid."_".$Date.$Gg.".xml";
        
        $UpdateGame = $conn->prepare("update games_info set visitor_customer_id=:cid, year=:year, home_customer_id=:cid, game_name=:gamename, date=:gamedate, time=:gametime, zone=:zone, visitor_team_id=:teamvid, home_team_id=:teamhid, team_type=:gender, game_location=:gameloc, isLeagueGame=:gametype, entrymode=:entrymode, tournament=:tournament, game_notes=:gameinfo, location=:location, division=:divid, season=:snid, new_game_xml=:xmlname where id=:gid");
        $UpdateArr =  array(":cid"=>$Cid, ":year"=>$Year, ":gamename"=>$GameName, ":gamedate"=>$GameDate, ":gametime"=>$GameTime, ":zone"=>$Zone, ":teamvid"=>$Teamvid, ":teamhid"=>$Teamhid, ":gender"=>$Gender, ":location"=>$Location, ":gametype"=>$GameType, ":entrymode"=>$EntryMode, ":tournament"=>$Tournament, ":gameinfo"=>$GameInfo, ":gameloc"=>$GameLoc, ":divid"=>$Divid, ":snid"=>$Snid, ":xmlname"=>$Xmlname, ":gid"=>$Gid);       
        $UpdateGame->execute($UpdateArr);        

        // $_SESSION['gamename'] = $_SESSION['gamedate'] = $_SESSION['location'] = $_SESSION['tournament'] = $_SESSION['gameinfo'] = $_SESSION['gameloc'] = "";

        // if($UpdateGame){
            header('Location:game_list.php?msg=3');
            exit;
        // } 
    }
}
include_once('header.php'); ?>

<link href="assets/custom/css/addgame.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<style>
.game-info-caption{
  padding: 3px 20px 3px !important;
}
</style>
<input type="hidden" id="sportid" value="<?php echo $SportId; ?>" name="sportid">
<input type="hidden" id="sportn" value="<?php echo $SportName; ?>" name="sportn">
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if (isset($_GET['msg'])) { ?>
            <div class="alert alert-block fade in <?php echo $alert_class; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $alert_message; ?> </p>
            </div>
            <?php } ?>            
            <div class="row">
                <form id="gameform" method="POST">
                    <div class="col-md-12">
                        <div class="col-md-12 left-right-padding">
                            <div class="portlet light game-info-caption">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo caption-width-team">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"> Game Information</span>
                                        <p class="mode-color"><?php echo $mode;?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 left-padding">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light ">
                                <!-- <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"> Game Information</span>
                                    </div>
                                </div> -->
                                <div class="portlet-body form">
                                    <div class="form-body top-padding">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="gamename">Game Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="gamename" name="gamename" placeholder="" value="<?php echo $GameNameDB; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="gamedate">Game Date
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="input-group input-sm date date-picker date-pick-left minicalender" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">
                                                        <input type="text" class="form-control input-sm" id="gamedate" name="gamedate" placeholder="" value="<?php echo $GameDateDB; ?>" >
                                                        <span class="input-group-btn ">
                                                            <button class="btn default input-sm" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>

                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="gametime">Game Time
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="input-group input-sm date-pick-left minitime">
                                                        <input type="text" class="form-control input-sm timepicker timepicker-no-seconds" id="gametime" name="gametime" placeholder="" value="<?php if(!empty($GameTimeDB)){echo $GameTimeDB;}else{echo date("g:i A.", time());} ?>">
                                                        <span class="input-group-btn">
                                                            <button class="btn default input-sm" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="zone">Time Zone</label>
                                                    <select class="form-control input-sm" name="zone" id="zone">
                                                        <option value="Pacific" <?php echo $ZoneDB == "Pacific" ? "selected" :"" ?> >PST</option>
                                                        <option value="Mountain" <?php echo $ZoneDB == "Mountain" ? "selected" :"" ?>>MST</option>
                                                        <option value="Central" <?php echo $ZoneDB == "Central" ? "selected" :"" ?>>CST</option>
                                                        <option value="Eastern" <?php echo $ZoneDB == "Eastern" ? "selected" :"" ?>>EST</option>               
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="visitor">Visitor
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="visitor" name="visitor" placeholder="" autocomplete="off" value="<?php echo $VisitorTeamnameDB; ?>"> 
                                                    <div class="display_resulterrormsg" id="display_visitor_result"></div>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="home">Home
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <input type="text" class="form-control input-sm" id="home" name="home" placeholder="" autocomplete="off" value="<?php echo $HomeTeamnameDB; ?>"> 
                                                    <div class="display_resulterrormsg" id="display_home_result"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                    <select class="form-control input-sm" name="gender" id="gender">
                                                        <option value="1" <?php echo $GenderDB == "1" ? "selected" :"" ?>>Male</option>
                                                        <option value="2" <?php if($SportName =='softball' || $SportName =='baseball') { echo "selected"; } ?> <?php echo $GenderDB == "2" ? "selected" :"" ?> >Female</option>               
                                                    </select>
                                                </div>                                                    
                                            </div>                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="location">Location</label>
                                                    <input type="text" class="form-control input-sm" id="location" name="location" placeholder="" value="<?php echo $LocationDB; ?>"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="divisionlist">Division
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <!-- <select class="form-control input-sm" name="divisionlist" id="divisionlist">
                                                        <option value="">---Select---</option>
                                                        <option value="addnew">Add New</option>
                                                        <?php 
                                                        $FetchDiv = json_decode(getCustomerDivisions($Cid),true);
                                                        foreach ($FetchDiv as $FetchRows) { ?>
                                                            <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $DivisionDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php } ?>
                                                    </select> --> 
                                                    <?php 
                                                    if ($_SESSION['master'] != 1) { ?>
                                                    <select class="form-control input-sm" name="divisionlist" id="divisionlist">
                                                        <option value="">---Select---</option>
                                                        <option value="addnew">Add New</option>
                                                        <?php 

                                                        $FetchConf = json_decode(getCustomerDivisions($Cid),true);
                                                        foreach ($FetchConf as $FetchRows) { ?>
                                                            <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $DivisionDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php } ?>             
                                                    </select>
                                                    <?php } else {?> 
                                                    <select class="form-control input-sm" name="divisionlist" id="divisionlist">
                                                        <option value="">---Select---</option>
                                                        <?php 
                                                        $children = array($_SESSION['childrens']);
                                                        $ids = $_SESSION['loginid'].",".join(',',$children); 
                                                        // $divlist = $conn->prepare("select * from customer_division where custid in ($ids) or custid='$Cid'");
                                                        $divlist = $conn->prepare("select * from customer_division where custid in ($ids)");
                                                        $divlist->execute();
                                                        $Cntdivlist = $divlist->rowCount();
                                                        if ($Cntdivlist > 0) {
                                                            $FetchDiv = $divlist->fetchAll(PDO::FETCH_ASSOC);
                                                            foreach ($FetchDiv as $FetchRows) { ?>
                                                                <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $DivisionDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php }} ?>             
                                                    </select>
                                                    <?php } ?>                                                    
                                                </div>                                                
                                            </div>

                                            <!-- Division Modal -->
                                            <div class="modal fade " id="DivisionModal" role="dialog">
                                                <div class="modal-dialog">                                                    
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Add new division</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">    
                                                                <label for="adddivtext">Enter New Division</label>
                                                                <input type="text" id="adddivtext" class="form-control" name="adddivtext" />
                                                            </div>
                                                            <input type="button" name="adddivbtn" class="btn btn-success" value="Submit" id="adddivbtn">
                                                            <button class="btn btn-danger customredbtn cancelbtn" type="button" data-dismiss="modal">Cancel</button>                                    
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                   
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rulelist">Division Rule
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <select class="form-control input-sm" name="rulelist" id="rulelist">
                                                        <option value="">---Select---</option>
                                                       <?php if($SportName == 'basketball') { ?>
                                                            <option value="FIBA">FIBA</option>
                                                            <option value="HS">HS</option>
                                                            <option value="NBA">NBA</option>
                                                            <option value="NCAA">NCAA</option>
                                                            <option value="NCAAW">NCAAW</option>
                                                        <?php } 
                                                        if ($SportName == 'baseball' || $SportName == 'softball') { 
                                                        ?>
                                                            <option value="HS_BA">HS_BA</option>
                                                            <option value="HS_SB">HS_SB</option>
                                                            <option value="MLB">MLB</option>
                                                            <option value="NCAA_BA">NCAA_BA</option>
                                                            <option value="NCAA_SB">NCAA_SB</option>
                                                        <?php } if ($SportName == 'football') { ?>
                                                            <option value="Arena">Arena</option>
                                                            <option value="HS">HS</option>
                                                            <option value="Indoor">Indoor</option>
                                                            <option value="NCAA">NCAA</option>
                                                            <option value="NFL">NFL</option>
                                                        <?php } ?>            
                                                    </select>                                                      
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gametype">Game Type
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control input-sm" name="gametype" id="gametype">
                                                <option value="2" <?php echo $GameTypeDB == "2" ? "selected" :"" ?> >Non –Conference</option>
                                                <option value="1" <?php echo $GameTypeDB == "1" ? "selected" :"" ?>>Preseason</option>
                                                <option value="3" <?php echo $GameTypeDB == "3" ? "selected" :"" ?>>Conference</option>
                                                <option value="4" <?php echo $GameTypeDB == "4" ? "selected" :"" ?>>Post Season</option>            
                                            </select>  
                                        </div>                                        
                                    </div>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-md-6 right-padding">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <div class="form-body top-padding">
                                        <div class="form-group">
                                            <label for="tournament">Tournament Name(If applicable)</label>
                                            <input type="text" class="form-control input-sm" id="tournament" name="tournament" placeholder="" value="<?php echo $TournamentDB; ?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="seasonlist">Season</label> 
                                                    <span class="required"> * </span>         
                                                    <select class="form-control input-sm" name="seasonlist" id="seasonlist">
                                                        <option value="">---Select---</option>
                                                        <option value="addnew">Add New</option>
                                                        <?php 
                                                        $FetchDiv = json_decode(getCustomerSeasons($Cid),true);
                                                        foreach ($FetchDiv as $FetchRows) { ?>
                                                            <option value="<?php echo $FetchRows['id']; ?>" <?php echo $FetchRows['id'] == $SeasonDB ? "selected" :"" ?> ><?php echo $FetchRows['name']; ?></option> 
                                                        <?php } ?>  
                                                    </select>
                                                </div>
                                            </div> 

                                            <!-- Season Modal -->
                                            <div class="modal fade " id="SeasonModal" role="dialog">
                                                <div class="modal-dialog">                                                    
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h2 class="modal-title" >Add new season</h2>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">    
                                                                <label for="addssntext">Enter New Season</label>
                                                                <input type="text" id="addssntext" class="form-control" name="addssntext" />
                                                            </div>
                                                            <input type="button" name="addssnbtn" class="btn btn-success" value="Submit" id="addssnbtn">
                                                            <button class="btn btn-danger customredbtn cancelbtn" type="button" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                    
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="entrymode">Entry Mode
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <select class="form-control input-sm" name="entrymode" id="entrymode">
                                                    <?php
                                                    if ($SportName != 'football') { ?>
                                                        <option value="10" <?php echo $EntryModeDB == "10" ? "selected" :"" ?>>2 Click Entry</option> 
                                                    <?php } ?>
                                                        <option value="0" <?php echo $EntryModeDB == "0" ? "selected" :"" ?>>Box Score Entry</option>
                                                        <option value="2" <?php if($SportName == 'softball' || $SportName == 'baseball') { echo "selected"; } ?> <?php echo $EntryModeDB == "2" ? "selected" :"" ?> >Play by Play</option>           
                                                    </select> 
                                                </div>                                                   
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gameinfo">Game Notes</label>
                                            <textarea class="form-control input-sm" rows="3" name="gameinfo" id="gameinfo"><?php echo $GameNotesDB; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="gameloc">Game Location</label>
                                            <textarea class="form-control input-sm" rows="3" name="gameloc" id="gameloc" ><?php echo $GameLocationDB; ?></textarea>
                                        </div>
                                        <div class="form-actions form-actions-btn">
                                            <button type="submit" class="btn customgreenbtn" name="addsubmit"><?php echo $mode == "Add" ? "Save" : "Update" ?></button>
                                            <button type="button" class="btn customredbtn" id="cancelbtn">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>                        
                    </div>                    
                </form>
            </div>            
        </div>
    </div>
</div>

<?php include_once('footer.php'); ?>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="assets/custom/js/addgame.js" type="text/javascript" ></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>


   
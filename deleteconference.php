<?php 
include_once('connect.php');

if ((isset($_POST['conferenceid'])) && (!empty($_POST['conferenceid']))) {
    $conferenceid	= $_POST['conferenceid'];
	$seasonid		= $_POST['seasonid'];
	
    $delseasonqry = $conn->prepare("delete from customer_season_conference where season_id=:seasonid and conference_id=:conference_id");
	$QryArr			= array(":seasonid"=>$seasonid,":conference_id"=>$conferenceid);

    $delseasonqry->execute($QryArr);

	$delconfqry = $conn->prepare("delete from customer_conference_division where season_id=:seasonid and  conference_id=:conference_id");
	$QryArr			= array(":seasonid"=>$seasonid,":conference_id"=>$conferenceid);

    $delconfqry->execute($QryArr);
	
	$delteamqry = $conn->prepare("delete from customer_division_team where conference_id=:conference_id and season_id=:season_id");
	$QryArr			= array(":conference_id"=>$conferenceid,":season_id"=>$seasonid);

    $delteamqry->execute($QryArr);


	$delplayerqry = $conn->prepare("delete from customer_team_player where season_id=:season_id");
	$QryArr			= array(":season_id"=>$seasonid);

    $delplayerqry->execute($QryArr);

	echo "success";
	exit;
}

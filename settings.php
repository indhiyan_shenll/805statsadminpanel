<?php 
include_once('session_check.php');
include_once('usertype_check.php'); 
include_once('header.php');

?>
<link href="assets/custom/css/settings.css" rel="stylesheet" type="text/css" />
<?php
$AlertMessage = '';
$AlertClass = '';
$AlertFlag = false;
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 1) {
        $AlertMessage = "Successfully saved your settings";
        $AlertClass = "alert-success";
        $AlertFlag = true;
    }
}
$cid = $Cid;
$customer_id = $_SESSION['loginid'];
$qry = $conn->prepare("SELECT sport_id from games_info where (home_customer_id = $customer_id OR visitor_customer_id = $customer_id) limit 0,1");
$qry->execute();
$CntQry = $qry->rowCount();
if ($CntQry) {
    $FetchRow = $qry->fetchAll(PDO::FETCH_ASSOC);
    foreach ($FetchRow as $trow ) {
        $sport_id = $trow['sport_id'];
    }
} else {
    $sport_id = $_SESSION['sportid'];
}

error_reporting(E_ALL);
$backgroundradio = '';
if (isset($_POST['gen_slider_page'])) {
    $gen_slider_page = $_POST['gen_slider_page'];
    $gen_standing_page = $_POST['gen_standing_page'];
    // $gen_schedule_page = $_POST['gen_schedule_page'];
    $gen_content_bgcolor = !empty($_POST['gen_content_area_bg_color'])? "#".$_POST['gen_content_area_bg_color'] : "" ;
    $gen_content_fontcolor = !empty($_POST['gen_content_area_font_color'])? "#".$_POST['gen_content_area_font_color'] : "" ;
    $menu_bgcolor = !empty($_POST['menu_bgcolor'])? "#".$_POST['menu_bgcolor'] : "" ;
    $menu_hovercolor = !empty($_POST['menu_hovercolor'])? "#".$_POST['menu_hovercolor'] : "" ;
    $menu_fontcolor = !empty($_POST['menu_fontcolor'])? "#".$_POST['menu_fontcolor'] : "" ;
    $slider_heading_bgcolor = !empty($_POST['slider_heading_bgcolor'])? "#".$_POST['slider_heading_bgcolor'] : "" ;
    $slider_content_bgcolor = !empty($_POST['slider_content_bgcolor'])? "#".$_POST['slider_content_bgcolor'] : "" ;
    $slider_fontcolor = !empty($_POST['slider_fontcolor'])? "#".$_POST['slider_fontcolor'] : "" ;
    $slider_bordercolor = !empty($_POST['slider_bordercolor'])? "#".$_POST['slider_bordercolor'] : "" ;
    $TeamSubmenubgcolor     = !empty($_POST['team_submenu_bgcolor'])? "#".$_POST['team_submenu_bgcolor'] : "" ;
    $SubmenubgColor         = !empty($_POST['head_submenu_bgcolor'])? "#".$_POST['head_submenu_bgcolor'] : "" ;
    $sliderteamnamecolor    = !empty($_POST['slider_teamname_color'])? "#".$_POST['slider_teamname_color'] : "" ;
    $Teamplayerbgcolor      = !empty($_POST['team_player_bgcolor'])? "#".$_POST['team_player_bgcolor'] : "" ;
    $TeamPlayerFontColor    = !empty($_POST['team_player_fontcolor'])? "#".$_POST['team_player_fontcolor'] : "" ;
    $TeamSubmenufontcolor   = !empty($_POST['team_submenu_fontcolor'])? "#".$_POST['team_submenu_fontcolor'] : "" ;
    $Alltbl_heading_bgcolor = !empty($_POST['all_table_heading_bg_color'])? "#".$_POST['all_table_heading_bg_color'] : "" ;
    $Alltbl_heading_fontcolor   = !empty($_POST['all_table_heading_font_color'])? "#".$_POST['all_table_heading_font_color'] : "" ;
    $Tbl_alt_fontcolor1 = !empty($_POST['table_alternate_font_color_1'])? "#".$_POST['table_alternate_font_color_1'] : "" ;
    $Tbl_alt_fontcolor2 = !empty($_POST['table_alternate_font_color_2'])? "#".$_POST['table_alternate_font_color_2'] : "" ; 
    $Tbl_alt_bgcolor_1  = !empty($_POST['table_alternate_bgcolor_1'])? "#".$_POST['table_alternate_bgcolor_1'] : "" ;
    $Tbl_alt_bgcolor_2  = !empty($_POST['table_alternate_bgcolor_2'])? "#".$_POST['table_alternate_bgcolor_2'] : "" ;
    $Team_leaderboard_option = $_POST['team_leaderboard_option'];

    $backgroundradio = $_POST['backgroundradio'];
    $hiddenbgimg = $_POST['hiddenbgimg'];
    if ($backgroundradio == "bgimg") {
        $backgroundimage = $_FILES['backgroundimage'];
        $day = date("ymdihs");
        if ($backgroundimage['name'] != '') {
            $file_name = $backgroundimage['name'];
            $file_size = $backgroundimage['size'];
            $file_tmp = $backgroundimage['tmp_name'];
            $file_type = $backgroundimage['type'];
            $hi= $day.$file_name;
            move_uploaded_file($file_tmp,"uploads/bg/".$day.$file_name);
        } else {
            $hi = $hiddenbgimg;
        }
        // echo $Team_leaderboard_option;exit;
        $Qry = $conn->prepare("update customer_info set bgimg='$hi', bgcolor='', gen_slider_page='$gen_slider_page', gen_standing_page='$gen_standing_page',  gen_content_bgcolor='$gen_content_bgcolor', gen_content_fontcolor='$gen_content_fontcolor', menu_bgcolor='$menu_bgcolor', menu_hovercolor='$menu_hovercolor', menu_fontcolor='$menu_fontcolor', slider_heading_bgcolor='$slider_heading_bgcolor', slider_content_bgcolor='$slider_content_bgcolor', slider_fontcolor='$slider_fontcolor', slider_bordercolor='$slider_bordercolor', team_submenu_bgcolor='$TeamSubmenubgcolor', head_submenu_bgcolor='$SubmenubgColor', slider_teamname_color='$sliderteamnamecolor', team_player_bgcolor='$Teamplayerbgcolor',team_player_fontcolor='$TeamPlayerFontColor', team_submenu_fontcolor='$TeamSubmenufontcolor',gen_table_heading_bgcolor='$Alltbl_heading_bgcolor', gen_table_heading_fontcolor='$Alltbl_heading_fontcolor', gen_table_alt_bg_color1='$Tbl_alt_bgcolor_1', gen_table_alt_bg_color2='$Tbl_alt_bgcolor_2',gen_table_alt_fontcolor1='$Tbl_alt_fontcolor1', gen_table_alt_fontcolor2='$Tbl_alt_fontcolor2', team_leaderboard_option='$Team_leaderboard_option' where id='$cid'");
        $Qry->execute();
        // mysql_query($Qry) or die(mysql_error());
        echo "<script>window.location.href='settings.php?msg=1'</script>";
        exit;            
    } else {
        $backgroundcolor = $_POST['backgroundcolor'];
        $Qry = $conn->prepare("update customer_info set bgimg='', bgcolor='$backgroundcolor', gen_slider_page='$gen_slider_page', gen_standing_page='$gen_standing_page', gen_content_bgcolor='$gen_content_bgcolor', gen_content_fontcolor='$gen_content_fontcolor', menu_bgcolor='$menu_bgcolor', menu_hovercolor='$menu_hovercolor', menu_fontcolor='$menu_fontcolor', slider_heading_bgcolor='$slider_heading_bgcolor', slider_content_bgcolor='$slider_content_bgcolor', slider_fontcolor='$slider_fontcolor', slider_bordercolor='$slider_bordercolor', team_submenu_bgcolor='$TeamSubmenubgcolor', head_submenu_bgcolor='$SubmenubgColor', slider_teamname_color='$sliderteamnamecolor', team_player_bgcolor='$Teamplayerbgcolor',team_player_fontcolor='$TeamPlayerFontColor', team_submenu_fontcolor='$TeamSubmenufontcolor',gen_table_heading_bgcolor='$Alltbl_heading_bgcolor', gen_table_heading_fontcolor='$Alltbl_heading_fontcolor', gen_table_alt_bg_color1='$Tbl_alt_bgcolor_1', gen_table_alt_bg_color2='$Tbl_alt_bgcolor_2',gen_table_alt_fontcolor1='$Tbl_alt_fontcolor1', gen_table_alt_fontcolor2='$Tbl_alt_fontcolor2', team_leaderboard_option='$Team_leaderboard_option' where id='$cid'");
        $Qry->execute();
        // mysql_query($Qry) or die(mysql_error());
        echo "<script>window.location.href='settings.php?msg=1'</script>";
        exit;
    }  
}

$result = $conn->prepare("select * from customer_info where id=$cid");
$result->execute();
$resultArr = $result->fetch(PDO::FETCH_ASSOC); 
if ($sport_id == 4443) {
    $defaultbgimage = 'defaultbgimg_fb.jpg';
} elseif ($sport_id == 4441 || $sport_id == 4442) {
    $defaultbgimage = 'defaultbgimg_ba.jpg';
} else {
    $defaultbgimage = 'defaultbgimg_bb.jpg';
}

$ArrDefaultSettings = array("bgcolor"=>"", "bgimg"=>$defaultbgimage, "gen_slider_page"=>"default", "gen_standing_page"=>"default", "gen_content_bgcolor"=>"#1A1A1A", "gen_content_fontcolor"=>"#FFFFFF", "menu_bgcolor"=>"#A00312", "menu_hovercolor"=>"#080808", "menu_fontcolor"=>"#FFFFFF", "slider_heading_bgcolor"=>"#1A1A1A", "slider_content_bgcolor"=>"#999999", "slider_fontcolor"=>"#EBDF40", "slider_bordercolor"=>"#870205","slider_teamname_color"=>"#000000","team_submenu_bgcolor"=>"#000000","head_submenu_bgcolor"=>"#000000","team_player_bgcolor"=>"#A00312","team_player_fontcolor"=>"#FFFFFF","team_submenu_fontcolor"=>"#FFFFFF","gen_table_heading_bgcolor"=>"#A00312","gen_table_heading_fontcolor"=>"#FFFFFF","gen_table_alt_bg_color1"=>"#F7F7F7","gen_table_alt_bg_color2"=>"#E1E1E1","gen_table_alt_fontcolor1"=>"#000000","gen_table_alt_fontcolor2"=>"#000000","team_leaderboard_option"=>"show");

// Update customer template settings
if (!empty($resultArr['bgcolor']))
    $ArrDefaultSettings['bgcolor'] = $resultArr['bgcolor'];
if (!empty($resultArr['bgimg']))
    $ArrDefaultSettings['bgimg'] = $resultArr['bgimg'];
if (!empty($resultArr['gen_slider_page']))
    $ArrDefaultSettings['gen_slider_page'] = $resultArr['gen_slider_page'];
if (!empty($resultArr['gen_standing_page']))
    $ArrDefaultSettings['gen_standing_page'] = $resultArr['gen_standing_page'];

if (!empty($resultArr['gen_content_bgcolor']))
    $ArrDefaultSettings['gen_content_bgcolor'] = $resultArr['gen_content_bgcolor'];
if (!empty($resultArr['gen_content_fontcolor']))
    $ArrDefaultSettings['gen_content_fontcolor'] = $resultArr['gen_content_fontcolor'];
if (!empty($resultArr['menu_bgcolor']))
    $ArrDefaultSettings['menu_bgcolor'] = $resultArr['menu_bgcolor'];
if (!empty($resultArr['menu_hovercolor']))
    $ArrDefaultSettings['menu_hovercolor'] = $resultArr['menu_hovercolor'];
if (!empty($resultArr['menu_fontcolor']))
    $ArrDefaultSettings['menu_fontcolor'] = $resultArr['menu_fontcolor'];
if (!empty($resultArr['slider_heading_bgcolor']))
    $ArrDefaultSettings['slider_heading_bgcolor'] = $resultArr['slider_heading_bgcolor'];
if (!empty($resultArr['slider_content_bgcolor']))
    $ArrDefaultSettings['slider_content_bgcolor'] = $resultArr['slider_content_bgcolor'];
if (!empty($resultArr['slider_fontcolor']))
    $ArrDefaultSettings['slider_fontcolor'] = $resultArr['slider_fontcolor'];
if (!empty($resultArr['slider_bordercolor']))
    $ArrDefaultSettings['slider_bordercolor'] = $resultArr['slider_bordercolor'];
if (!empty($resultArr['slider_teamname_color']))
    $ArrDefaultSettings['slider_teamname_color']    = $resultArr['slider_teamname_color'];
if (!empty($resultArr['head_submenu_bgcolor']))
    $ArrDefaultSettings['head_submenu_bgcolor']     = $resultArr['head_submenu_bgcolor'];
if (!empty($resultArr['team_player_bgcolor']))
    $ArrDefaultSettings['team_player_bgcolor']      = $resultArr['team_player_bgcolor'];
if (!empty($resultArr['team_player_fontcolor']))
    $ArrDefaultSettings['team_player_fontcolor']    = $resultArr['team_player_fontcolor']; 
if (!empty($resultArr['team_submenu_fontcolor']))
    $ArrDefaultSettings['team_submenu_fontcolor']   = $resultArr['team_submenu_fontcolor'];
if (!empty($resultArr['team_submenu_bgcolor']))
    $ArrDefaultSettings['team_submenu_bgcolor']     = $resultArr['team_submenu_bgcolor'];
if (!empty($resultArr['gen_table_heading_bgcolor']))
    $ArrDefaultSettings['gen_table_heading_bgcolor']     = $resultArr['gen_table_heading_bgcolor'];
if (!empty($resultArr['gen_table_heading_fontcolor']))
    $ArrDefaultSettings['gen_table_heading_fontcolor']     = $resultArr['gen_table_heading_fontcolor'];
if (!empty($resultArr['gen_table_alt_bg_color1']))
    $ArrDefaultSettings['gen_table_alt_bg_color1']     = $resultArr['gen_table_alt_bg_color1'];
if (!empty($resultArr['gen_table_alt_bg_color2']))
    $ArrDefaultSettings['gen_table_alt_bg_color2']     = $resultArr['gen_table_alt_bg_color2'];
if (!empty($resultArr['gen_table_alt_fontcolor1']))
    $ArrDefaultSettings['gen_table_alt_fontcolor1']     = $resultArr['gen_table_alt_fontcolor1'];
if (!empty($resultArr['gen_table_alt_fontcolor2']))
    $ArrDefaultSettings['gen_table_alt_fontcolor2']     = $resultArr['gen_table_alt_fontcolor2'];
if (!empty($resultArr['team_leaderboard_option']))
    $ArrDefaultSettings['team_leaderboard_option']     = $resultArr['team_leaderboard_option'];

if (!empty($resultArr['site_template_settings']))
    $ArrDefaultSettings['site_template_settings']     = $resultArr['site_template_settings'];

?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <?php if ($AlertFlag == true) { ?>
            <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $AlertMessage; ?> </p>
            </div>
            <?php } ?>
            
            <!-- END PAGE HEADER-->

            <div class="row">
                <form id="settingsform" method="POST"  enctype="multipart/form-data">
                    <div class="col-md-12 settings-div">                        
                        <div class="col-md-6">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                <fieldset>
                                    <legend>General settings</legend>
                                    <div class="form-body setting-type">
                                        <!-- <div class="m-heading-1 border-red m-bordered settings-sub-title">
                                            <h3>General Settings</h3>                                            
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group parentradio">
                                                    <div class="mt-radio-list settings-radio"> 
                                                    <?php $selected = (!empty($ArrDefaultSettings['bgimg']))?'checked':''; ?>
                                                        <label class="mt-radio mt-radio-outline"> Background Image
                                                            <input type="radio" value="bgimg" id="radio1_1" name="backgroundradio" <?php echo $selected; ?>>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="file" id="backgroundimage" name="backgroundimage">
                                                    <span><?php echo $ArrDefaultSettings['bgimg']; ?></span>
                                                </div> 

                                            </div>
                                            <div class="col-md-6">                                                
                                                <div class="form-group">
                                                    <?php $previewImage = "uploads/bg/".$ArrDefaultSettings['bgimg']; ?>
                                                    <input type="hidden" name="hiddenbgimg" id="hiddenbgimg" value="<?php echo $ArrDefaultSettings['bgimg']; ?>">
                                                    <img id="preview" src="<?php echo $previewImage; ?>" alt="your image" /> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group parentradio">
                                                    <div class="mt-radio-list settings-radio"> 
                                                    <?php $selected = (!empty($ArrDefaultSettings['bgcolor']))?'checked':''; ?>                           
                                                        <label class="mt-radio mt-radio-outline" > Background Color
                                                            <input type="radio" value="bgcolor" id="radio1_2" name="backgroundradio" <?php echo $selected; ?>>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['bgcolor']; ?>" name="backgroundcolor"> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Standing Page</label>
                                                    <select class="form-control input-sm" id="standings" name="gen_standing_page">
                                                        <option value="default" <?php echo ($ArrDefaultSettings['gen_standing_page']=='default')?'selected':'' ?> >default</option>
                                                        <option value="advanced" <?php echo ($ArrDefaultSettings['gen_standing_page']=='advanced')?'selected':'' ?> >advanced</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table alternate color 1</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="table_alternate_bgcolor_1" value="<?php echo $ArrDefaultSettings['gen_table_alt_bg_color1']; ?>"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table alternate color 2</label>
                                                    <input type="text"  name="table_alternate_bgcolor_2" class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_table_alt_bg_color2']; ?>"> 
                                                </div>
                                            </div>
                                        </div>                                         
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Content background color</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_content_bgcolor']; ?>" name="gen_content_area_bg_color"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Content font color</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_content_fontcolor']; ?>" name="gen_content_area_font_color"> 
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table heading background color</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_table_heading_bgcolor']; ?>" name="all_table_heading_bg_color"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table heading font color</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_table_heading_fontcolor']; ?>" name="all_table_heading_font_color"> 
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table alternate font color 1</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_table_alt_fontcolor1']; ?>" name="table_alternate_font_color_1"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Table alternate font color 2</label>
                                                    <input type="text"  class="form-control input-sm demo" data-position="bottom right" value="<?php echo $ArrDefaultSettings['gen_table_alt_fontcolor2']; ?>" name="table_alternate_font_color_2"> 
                                                </div>
                                            </div>
                                        </div>                                       
                                        <div class="form-actions custom-form-action">
                                            <button type="submit" class="btn customgreenbtn" style="color:white;" name="addsubmit">Update</button>
                                        </div>                                                                                
                                    </div>
                                </fieldset>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <fieldset>
                                        <legend>Slider Settings</legend>
                                        <div class="form-body setting-type">
                                            <!-- <div class="m-heading-1 border-red m-bordered settings-sub-title">
                                                <h3>Slider Settings</h3>                                            
                                            </div>  -->                                   
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Slider Type</label>
                                                        <?php 
                                                        if ($sport_id != 4443) { ?>
                                                        <select class="form-control input-sm" id="slider" name="gen_slider_page">
                                                            <option value="advanced_new" <?php echo ($ArrDefaultSettings['gen_slider_page']=='advanced_new')?'selected':'' ?> >Team image with short Name</option>
                                                            <option value="default" <?php echo ($ArrDefaultSettings['gen_slider_page']=='default')?'selected':'' ?> >Team name only</option>
                                                        </select>
                                                        <?php } else { ?>
                                                        <select id="slider" name="gen_slider_page" class="form-control input-sm" >
                                                        <option value="advanced_new" <?php echo ($ArrDefaultSettings['gen_slider_page']=='advanced_new')?'selected':'' ?> >Team name only</option>
                                                        <option value="default" <?php echo ($ArrDefaultSettings['gen_slider_page']=='default')?'selected':'' ?> >Team image with short Name</option>
                                                    </select>
                                                    <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Team name color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="slider_teamname_color" value="<?php echo $ArrDefaultSettings['slider_teamname_color']; ?>"> 
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Timeline background color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="slider_heading_bgcolor" value="<?php echo $ArrDefaultSettings['slider_heading_bgcolor']; ?>"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Timeline font color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="slider_fontcolor" value="<?php echo $ArrDefaultSettings['slider_fontcolor']; ?>"> 
                                                    </div>
                                                </div>
                                            </div>                                
                                        </div>
                                    </fieldset>                                    
                                </div>
                            </div>
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <fieldset>
                                        <legend>Menu Settings</legend>
                                        <div class="form-body setting-type">
                                            <!-- <div class="m-heading-1 border-red m-bordered settings-sub-title">
                                                <h3>Menu Settings</h3>                                            
                                            </div> -->

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Background color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="menu_bgcolor" value="<?php echo $ArrDefaultSettings['menu_bgcolor']; ?>"> 
                                                    </div>
                                                </div>                                           
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Hover color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="menu_hovercolor" value="<?php echo $ArrDefaultSettings['menu_hovercolor']; ?>"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Font color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="menu_fontcolor" value="<?php echo $ArrDefaultSettings['menu_fontcolor']; ?>"> 
                                                    </div>
                                                </div>
                                            </div> 
                                                                           
                                        </div>
                                    </fieldset>                                    
                                </div>
                            </div>
                            <div class="portlet light ">
                                <div class="portlet-body form">
                                    <fieldset>
                                        <legend>Team Page Settings</legend>
                                        <div class="form-body setting-type">
                                            <!-- <div class="m-heading-1 border-red m-bordered settings-sub-title">
                                                <h3>Team Page Settings</h3>                                            
                                            </div>  -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Submenu background color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="team_submenu_bgcolor" value="<?php echo $ArrDefaultSettings['team_submenu_bgcolor']; ?>"> 
                                                    </div>
                                                </div>                                           
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Submenu font color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="team_submenu_fontcolor" value="<?php echo $ArrDefaultSettings['team_submenu_fontcolor']; ?>"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Player position background color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="team_player_bgcolor" value="<?php echo $ArrDefaultSettings['team_player_bgcolor']; ?>"> 
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Player position font color</label>
                                                        <input type="text"  class="form-control input-sm demo" data-position="bottom right" name="team_player_fontcolor" value="<?php echo $ArrDefaultSettings['team_player_fontcolor']; ?>"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">                                                
                                                    <div class="form-group parentradio">
                                                        <label>Team leaderboard</label>
                                                        <div class="mt-radio-inline settings-radio">
                                                            <?php
                                                            $selected = ($ArrDefaultSettings['team_leaderboard_option']=='show')?'checked':''; ?>
                                                            <label class="mt-radio mt-radio-outline">
                                                                <input type="radio" name="team_leaderboard_option" id="optionsRadios4" value="show"  <?php echo $selected; ?>> Show
                                                                <span></span>
                                                            </label>
                                                            <?php 
                                                            $selected = ($ArrDefaultSettings['team_leaderboard_option'] == 'hide')?'checked':''; ?>
                                                            <label class="mt-radio mt-radio-outline">
                                                                <input type="radio" name="team_leaderboard_option" id="optionsRadios5" value="hide" <?php echo $selected; ?>> Hide
                                                                <span></span>
                                                            </label>                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                        
                                        </div>
                                    </fieldset>                                    
                                </div>
                            </div>                    
                        </div>
                                              
                    </div>                    
                </form>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/components-color-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script src="assets/custom/js/settings.js" type="text/javascript"></script>
<!-- <script src="assets/pages/scripts/ui-general.min.js" type="text/javascript"></script> -->
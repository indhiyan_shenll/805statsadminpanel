<?php
include_once('session_check.php');
include_once('connect.php'); 
unset($_SESSION['divisionid']);
unset($_SESSION['seasonid']);
unset($_SESSION['conferenceid']);

if(isset($_POST['seasonid'])){
	$SeasonId      = $_POST['seasonid'];
	$PostType      = $_POST['post_type'];
	$_SESSION['seasonid']  = $SeasonId;
	

	$SeasonOptions = '';
	if($PostType == "seasonlist"){
		$_SESSION['conferenceid']	= '';		
		$_SESSION['divisionid']		= '';

		$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
		$Qryarr = array(":season_id"=>$SeasonId);
		$QryExe->execute($Qryarr);
		$QryCntSeasonconf	= $QryExe->rowCount();											
		
		if ($QryCntSeasonconf > 0) {
			$SeasonOptions     .= "<option value=''>Select conference</option>";
			while ($rowSeason   = $QryExe->fetch(PDO::FETCH_ASSOC)){				
				$SeasonOptions .= "<option value='".$rowSeason['conference_id']."'>".$rowSeason['conference_name']."</option>";
			}
		}else{
			$SeasonOptions   ="<option value=''>No conference found</option>";
		}
	}else if($PostType == "conferencelist"){
		$SeasonId					= $_POST['seasonid'];
		$conferenceid				= $_POST['conferenceid'];
		$_SESSION['conferenceid']	= $conferenceid;		
		$_SESSION['seasonid']		= $SeasonId;

		$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
		$QryarrCon = array(":conference_id"=>$conferenceid,":season_id"=>$SeasonId);

		$QryExeDiv->execute($QryarrCon);
		$QryCntSeason = $QryExeDiv->rowCount();
		$DivisionWrapHtml= $AddNewSeasonTree='';
		$Inc =0;
		if ($QryCntSeason > 0) {
			$SeasonOptions     .= "<option value=''>Select division</option>";
			while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
				$SeasonOptions     .= "<option value='".$row['id']."'>".$row['name']."</option>";
			}
		}else{
			$SeasonOptions     = "<option value=''>No season found</option>";
		}
	
	}else if($PostType == "divisionlist"){
		$conferenceid				= $_POST['conferenceid'];		
		$divisionid					= $_POST['divisionid'];
		$_SESSION['divisionid']		= $divisionid;	
		$_SESSION['conferenceid']	= $conferenceid;
		

		$QryExe1		= $conn->prepare("select * from customer_division_team where customer_id=:customer_id and conference_id=:conference_id and division_id!=:divisionid and season_id=:season_id");

		$Qryarr = array(":customer_id"=>$customerid,":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":divisionid"=>$_SESSION['divisionid']);

		$QryExe1->execute($Qryarr);
		$QryCntTeam = $QryExe1->rowCount();
		$TeamIdArr  = array();

		if ($QryCntTeam > 0) {
			while ($rowTeam = $QryExe1->fetch(PDO::FETCH_ASSOC)){	
				$TeamIdArr[] = $rowTeam['team_id'];
			}
		}
		
		?>
		                             
			<div class="form-body top-padding" style="padding: 0px 15px 0px 15px"> 
									<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
									<div class="row">

										<div class="col-xs-6 col-md-6" style="padding-left: 0px;">
											<div class="portlet light">
											<select name="from[]" id="undo_redo" class="form-control border-radius " size="13">
											<?php	
												$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();									
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
														echo "<option value='".$rowTeam['team_id']."' >".$rowTeam['team_name']."</option>";
													}
												}
											?>
											</select>
											
											</div>

										</div>
										
										
										
										<div class="col-xs-6 col-md-6 rightsidewrap"  style="padding-right: 0px;">
											<div class="portlet light">
											<div id="undo_redo_to" class="form-control border-radius requiredcs" size="13" multiple="multiple">
											
											<?php											

												$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();										
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){				
														//echo "<option value='".$rowTeam['id']."'>".$rowTeam['team_name']."</option>";
													}
												}
											?>

											</div>
											
											</div>
										</div>
									</div>
								</div> 
			
			<script>
			$(document).ready(function() {
				$('#undo_redo').multiselect({
				sort:false,
				search: {
					left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
					right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
				},
				moveToRight:function($left, $right, $options) { skipStack:false;},
				moveToLeft: function($left, $right, $options) {skipStack:false; }
				});
			});

			
			</script>   

		<?php
	}
	echo $SeasonOptions;
}
?>
<?php include("connect.php");
include_once('session_check.php');
if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	$HdnMode=$_REQUEST["HdnMode"];
	$HdnPage=$_REQUEST["HdnPage"];
    $searchname =  $_REQUEST['searchbyname'];
    $searchstatus =  $_REQUEST['Searchbystatus'];
    
	//$Page=$HdnMode;
    //$Page=$HdnPage;
	$Page=1;
}
?>
<form id="frm_player_list" name="frm_player_list" method="post" action="player_list.php" enctype="multipart/form-data">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $searchname;?>">
<input type="hidden" name="hnd_status" id="hnd_status" value="<?php echo $searchstatus;?>">
<table class="table table-striped table-bordered  dataTable" id="sample_1">
<thead>
	<tr>
		<th nowrap class="tbl_center" > Player&nbsp;Id </th>
        <th nowrap class="tbl_center" > First&nbsp;Name </th>
        <th nowrap class="tbl_center" > Last&nbsp;Name </th>
        <th nowrap class="tbl_center" > Uniform&nbsp;No </th>
        <th nowrap class="tbl_center" > Position </th>
        <th nowrap class="tbl_center" > Team&nbsp;Name </th>
        <th nowrap class="tbl_center" > Player&nbsp;Image </th>
        <th nowrap class="tbl_center" > Player&nbsp;Image&nbsp;by&nbsp;Season</th>
        <th nowrap class="tbl_center" > isActive </th>
        <th nowrap class="tbl_center" > Action</th>
	</tr>
</thead>
<tbody>
<?php

if(isset($_REQUEST['searchbyname']))
{
    $team_manager_id = stripslashes($_REQUEST['team_manager_id']);
	$cid        =  $_REQUEST['cid'];
	$searchname =  $_REQUEST['searchbyname'];
	$sportid    =  stripslashes($_REQUEST['sportid']);
	$sportname  =  $_REQUEST['sportname'];
    $searchstatus =  $_REQUEST['Searchbystatus'];
	if($searchstatus=="active"){
       $status="and isActive='1'";
       $status1="and player_info.isActive='1'";
    }
    if($searchstatus=="Inactive"){
        $status="and isActive='0'";
        $status1="and player_info.isActive='0'";
    }
    // if($searchstatus=="all"){
    //     $status="and isActive!=''";
    //     $status1="and player_info.isActive!=''";

    // }
	if ($searchname != "") {
        if ($searchname == "all")
             $res = "select * from player_info where customer_id in ($cid) $team_manager_id $team_manager_id $status and lastname!='TEAM' and (sport_id='$sportid')";
            else
            $res="select * from player_info where team_id='$searchname' and lastname!='TEAM' $status order by lastname";
        } else {
        $res="select * from player_info where customer_id='$cid' and lastname!='TEAM' $status order by lastname";
	}
    //echo $res;
    $getResQry      =   $conn->prepare($res);
    $getResQry->execute();
    $getResCnt      =   $getResQry->rowCount();
    $getResQry->closeCursor();
	    if($getResCnt>0){
	        $TotalPages=ceil($getResCnt/$RecordsPerPage);
	        $Start=($Page-1)*$RecordsPerPage;
	        $sno=$Start+1;
	            
	        $res.=" limit $Start,$RecordsPerPage";
	                
	        $getResQry      =   $conn->prepare($res);
	        $getResQry->execute();
	        $getResCnt      =   $getResQry->rowCount();
	    if($getResCnt>0){
	        $getResRows     =   $getResQry->fetchAll();
	        $getResQry->closeCursor();
	        $s=1;
        foreach($getResRows as $row){
		?>
			<tr>
				<td nowrap class="tbl_center_td"><?php echo $row['id']; ?></td>
				<td nowrap class="tbl_center_td"><a href="manage_player.php?p_id=<?php echo base64_encode($row['id']); ?> "><?php echo $row['firstname']; ?></a></td>
				<td nowrap class="tbl_center_td"><?php echo $row['lastname']; ?></td>
				<td nowrap class="tbl_center_td"><?php echo $row['uniform_no']; ?></td>
				<td nowrap class="tbl_center_td"><?php echo $row['position']; ?></td>
				<td nowrap class="tbl_center_td">
				<?php 
				$teamid=$row['team_id']; 
			    $res2="select * from teams_info where id='$teamid' and customer_id ='".$row['customer_id']."' and (sport_id='$sportid')";
                $getResQry2      =   $conn->prepare($res2);
			    $getResQry2->execute();
			    $getResCnt2      =   $getResQry2->rowCount();
			    $getResRow     =   $getResQry2->fetchAll();
				foreach ($getResRow as $row2 ) {
				     echo $row2 ['team_name'];
				}
				?>
				</td>
				<td nowrap class="tbl_center_td">
				<?php if($row['image']!=""){ ?>
				<a class="btn_round_green  btn-circle btn-icon-only tooltips btn_round_green" style="line-height: 1.00;"  data-toggle="modal" data-id="<?php echo $row['id']; ?>" href="#small<?php echo $row['id']; ?>"><i class="fa fa-photo"></i></a>
                <div class="modal fade bs-modal-sm" id="small<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Player Image</h4>
                            </div>
                            <div class="modal-body">
                             <?php if($row['image']!=""){ ?>
							<img src="uploads/players/<?php echo $row['image']; ?>" alt="" width="150" height="150" />
							<?php }?>
                            </div>
                            <div class="modal-footer" style="margin-top:10px;">
                                <button type="button" class="btn red btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <?php } ?>
				</td>
				<td class="tbl_center_td"><button type="button" class="btn btn_purple purple btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row['id']; ?>">Season by Image</button>
                <div class="modal fade draggable-modal" id="myModal<?php echo $row['id']; ?>" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Season Profile Images</h4>
                            </div>
                            <div class="modal-body"> 
                            <form class="profileimgform " method="POST" enctype="multipart/form-data">  
                            <input name="playerid" type="hidden" value="<?php echo $row['id']; ?>" class="hidplayerid"/>
                                <?php
                                    $PlayerImgIds = "select * from player_images where player_id=:PlayerId";
                                    $getPlayerQry    =   $conn->prepare($PlayerImgIds);
                                    $getPlayerQry->execute(array(":PlayerId"=>$row['id']));
                                    $ProfileCnt      =   $getPlayerQry->rowCount();
                        
                                ?>
                                <div class="col-md-12 playerimgcont">
                                    <div class="alert" id="deletestatus"></div>
                                    <table class="table table-bordered" style="border: 1px solid #ccc;">
                                    <tr><th class="imghead">Action</th><th class="imghead">Season</th><th class="imghead">Profile Image</th><?php if ($ProfileCnt){ echo '<th class="imghead">Remove</th>';}?></tr>
                                    <?php                                               
                                    $SeasonIdsArr = array();
                                     if ($ProfileCnt){
                                        $getPlayersRows     =   $getPlayerQry->fetchAll();
                                            foreach($getPlayersRows as $ImgList){
                                                $ProfileImgId= $ImgList['player_img_id'];
                                                $SeasonIdArr  = array_filter(explode("##",$ImgList['season_id']));
                                                $SeasonIdsArr  = array_merge($SeasonIdsArr,$SeasonIdArr);

                                                $SeasonId    = implode(",",$SeasonIdArr);
                                                
                                                $SelectedSeason = '<select class="profileimgsedit" multiple="multiple">';                                   
                                                 $sealist="select * from customer_season where custid=:cid";
                                                 $getseaQry      =   $conn->prepare($sealist);
                                                 $getseaQry->execute(array(":cid"=>$cid));
                                                 $getseaCount      =   $getseaQry->rowCount();
                                                 $getseaRows     =   $getseaQry->fetchAll();
                                                foreach($getseaRows as $sowlist){
                                                    $Selected  = (in_array($sowlist["id"],$SeasonIdArr))?'selected':'';
                                                    
                                                    $SelectedSeason .='<option value="'.$sowlist["id"].'" '.$Selected.'>'.$sowlist["name"].'</option>';
                                                 } 
                                                $SelectedSeason .='</select>';  

                                                $ProfileImg  = $ImgList['profile_imgs'];
                                                $ProfImgHtml = '';

                                                if($ProfileImg!="" && is_file("uploads/players/".$ProfileImg)){ 
                                                    $ProfImgHtml ="<img src='uploads/players/$ProfileImg' alt='' width='40' height='40'/>";
                                                } 
                                                $Seasons     = "select name from customer_season where id in($SeasonId)";
                                                 $getSeasonsQry      =   $conn->prepare($Seasons);
                                                 $getSeasonsQry->execute();
                                                 $getSeasonsCount      =   $getSeasonsQry->rowCount();
                                                echo '<tr><td style="vertical-align:middle;"><span date-imgid='.$ProfileImgId.' class="editplayimg">Edit</span><p class="loading_img"><img src="images/loading-publish.gif"></p></td><td class="text-center seasonsnames profileimgswrap">';
                                                 $getSeasonsRows     =   $getSeasonsQry->fetchAll();
                                                foreach( $getSeasonsRows as $SeasonsList ){                      
                                                    echo '<p style="margin-top:0px">'.$SeasonsList['name'].'</p>';                             
                                                }
                                                echo $SelectedSeason;
                                                echo '</td>';
                                                echo "<td class='text-center profileimgdips'>$ProfImgHtml</td>";
                                                echo "<td class='text-center deleteimgicons'><img src='images/delete.png' class='imguploadicons deleteprofileimg' date-imgid='$ProfileImgId'></td></tr>";
                                         }  
                                     }else{
                                        echo '<tr><td colspan="3" class="imgslistbody">Season image not uploaded</td></tr>';
                                     }?>        
                                     </table>
                                </div>
                                <div class="col-md-12 playerimguploadwrap imguploadcont"> 
                                    <h4 class='addnewheading text-left' style="color: #000;">Add image to season</h4>
                                    <div class="col-md-5 col-sm-12">                        
                                        <div class="col-md-12 profileimgswrap bottomadddmore" >    
                                                          
                                            <select name="seasons[0][]" class="profileimgs" multiple="multiple">
                                                <?php
                                                $sealistQry="select * from customer_season where custid=:cid";
                                                $getsealistResQry      =   $conn->prepare($sealistQry);
                                                $getsealistResQry->execute(array(":cid"=>$cid));
                                                $getRowCount      =   $getsealistResQry->rowCount();
                                                $getseasonListRows     =   $getsealistResQry->fetchAll();
                                                foreach ($getseasonListRows as $sowlist) {
                                                $SelDsbld   = (in_array($sowlist["id"],$SeasonIdsArr))?" disabled":"";
                                                    echo '<option value="'.$sowlist["id"].'" '.$SelDsbld.'>'.$sowlist["name"].'</option>';

                                                } ?>
                                            </select>
                                    
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-12 imgsuploaderfile">                       
                                        <div class="form-group col-md-12">
                                            <input type="file" name="upload[]"  multiple="multiple">
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12 imgsuploader">                       
                                        <div class="imgiconcont">
                                            <img src="images/addmoreimg.png" class='imguploadicons addplusicon'>
                                        </div>
                                    </div>                                          
                                </div>
                                <div id="moreuploadwrap">
                                </div>
                                <div class="col-md-12 col-sm-12" style='margin-top:10px;padding-bottom: 25px;'>
                                    <input class="btn btn-success uploadimgbtn" type="button" name="addsubmit" value="Update/Save" >&nbsp;&nbsp;<input class="btn btn-danger" type="button" value="Cancel" id="cancelbtn" data-dismiss="modal">
                                </div>
                                
                              </form>
						</div>
                        <div class="modal-footer" style="border-top: 0px solid #e5e5e5; ">
                           <!--  <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                            <button type="button" class="btn green">Save changes</button> -->
                        </div> 
                    <!-- /.modal-content -->
                    </div>
                <!-- /.modal-dialog -->
                   </div>
				</td>
                <td nowrap class="tbl_center_td">
                    <?php 
                    $isactive=$row['isActive'];
                    if($isactive=="1")
                    {
                    $status_val='<img src="./images/active_player.png"  class="tooltips" data-original-title="Click here to Deactive" ></img>';
                    $change_status='Deactive';
                    }else
                    {
                    $status_val='<img src="./images/deactive_player.png"  class="tooltips" data-original-title="Click here to Active"></img>';
                    $change_status='Active';
                    }
                    ?>
                    <a href="javascript:void(0);"  id="<?php echo $row['id']?>" rel="<?php echo $change_status;?>"   class="status_change tooltips"  ><?php echo $status_val;?></a>
                </td>
				<td nowrap class="tbl_center_td"> <a class="btn_round btn-circle btn-icon-only btn-default red tooltips btn-danger" style="line-height: 1.00;" type="button " onclick="return confirmation('<?php echo base64_encode($row['id']); ?>','<?php echo $sportname; ?>');" style="cursor:pointer;" data-original-title="Delete Player" /><i class="icon-trash"></i></a></td>
			</tr>

		<?php
		$s++;
		}
	} else{
            echo "<td colspan='9' style='text-align:center;'>No Player(s) found.</td>";
        }
    }
    else{
        echo "<tr><td colspan='9' style='text-align:center;'>No Player(s) found.</td></tr>";
    }
	
}?>
 </tbody>
 </table>
<?php
	if($TotalPages > 1){

	echo "<tr><td style='text-align:center;' colspan='12' valign='middle' class='pagination'>";
	$FormName = "frm_player_list";
	require_once ("paging.php");
	echo "</td></tr>";

	}
?>

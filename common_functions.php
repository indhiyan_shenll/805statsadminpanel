<?php
include_once("connect.php");
function cropImage($data = array())
{  
	require_once('lib/imageresize.php');
	$height = (int)$data['height'];
	$width = (int)$data['width'];
	$image_source = $data['image_source'];
	$image_desti_dir = $data['destination_folder'];
	$image_filename= $data['image_name'];
	$destination_resize_img = $image_desti_dir . $image_filename; 
	$resize = new ResizeImage($image_source);
	$resize->resizeTo($width, $height, 'exact');
	$resize->saveImage($destination_resize_img); 
	return true;
}
function getTeamInfo($Teamid, $Customerid = '') 
{
	global $conn;
	$QryCondn = ($Customerid) ? "and customer_id=:customer_id" : "" ;

	$TeamQry = $conn->prepare("select * from teams_info where id=:teamid $QryCondn");
	$TeamQryArr = array(":teamid"=>$Teamid, ":customer_id"=>$Customerid);
	$TeamQry->execute($TeamQryArr);
	$CntTeam = $TeamQry->rowCount();
	if ($CntTeam > 0) {
		$TeamRows = $TeamQry->fetch(PDO::FETCH_ASSOC);		
	}
	return json_encode($TeamRows);
}
function getTeamName($Teamid) 
{
	global $conn;
	if (!empty($Teamid)) {

		$TeamQry = $conn->prepare("select team_name from teams_info where id=:teamid");
		$TeamQryArr = array(":teamid"=>$Teamid);
		$TeamQry->execute($TeamQryArr);
		$CntTeam = $TeamQry->rowCount();
		if ($CntTeam > 0) {
			$FetchTeamRows = $TeamQry->fetch(PDO::FETCH_ASSOC);	
			$TeamRows = $FetchTeamRows['team_name'];	
		}
		return json_encode($TeamRows);
	}
}
function getCustomerDivisions($Customerid) 
{
	global $conn;
	$CustQryCondn = ($Customerid) ? " custid=:customer_id" : "" ;	

	$Qry = $conn->prepare("select * from customer_division where $CustQryCondn");
	$QryArr = array(":customer_id"=>$Customerid);
	$Qry->execute($QryArr);
	$CntRcds = $Qry->rowCount();
	if ($CntRcds > 0) {
		$Rows = $Qry->fetchAll(PDO::FETCH_ASSOC);		
	}
	return json_encode($Rows);
}
function getCustomerConference($Customerid) 
{
	global $conn;
	$CustQryCondn = ($Customerid) ? " customer_id=:customer_id" : "" ;	

	$Qry = $conn->prepare("select * from customer_conference where $CustQryCondn");
	$QryArr = array(":customer_id"=>$Customerid);
	$Qry->execute($QryArr);
	$CntRcds = $Qry->rowCount();
	if ($CntRcds > 0) {
		$Rows = $Qry->fetchAll(PDO::FETCH_ASSOC);		
	}
	return json_encode($Rows);
}
function getCustomerSeasons($Customerid) 
{
	global $conn;
	$CustQryCondn = ($Customerid) ? " custid=:customer_id" : "" ;	

	$Qry = $conn->prepare("select * from customer_season where $CustQryCondn");
	$QryArr = array(":customer_id"=>$Customerid);
	$Qry->execute($QryArr);
	$CntRcds = $Qry->rowCount();
	if ($CntRcds > 0) {
		$Rows = $Qry->fetchAll(PDO::FETCH_ASSOC);		
	}
	return json_encode($Rows);
}
function getDivisionName($Divisionid) 
{
	global $conn;
	// $CustQryCondn = ($Customerid) ? " custid=:customer_id" : "" ;	
	if (!empty($Divisionid)) {
		$Qry = $conn->prepare("select * from customer_division where id=:divisionid");
		$QryArr = array(":divisionid"=>$Divisionid);
		$Qry->execute($QryArr);
		$CntRcds = $Qry->rowCount();
		if ($CntRcds > 0) {
			$FetchTeamRows = $Qry->fetch(PDO::FETCH_ASSOC);	
			$DBRows = $FetchTeamRows['name'];		
		}
		return json_encode($DBRows);
	}
}

function getGameType($gametype) 
{
	global $conn;
	if (!empty($gametype)) {

		if ($gametype == '1') {
			$Rows = "Preseason";
		} elseif ($gametype == '2') {
			$Rows = "Non –Conference";
		} elseif ($gametype == '3') {
			$Rows = "Conference";
		} elseif ($gametype == '4') {
			$Rows = "Post Season";
		} else {
			$Rows = "Non –Conference";
		}
		return json_encode($Rows);
	}
}
function getSeasonName($Seasonname) 
{
	global $conn;

	if (!empty($Seasonname)) {
		$Qry = $conn->prepare("select name from customer_season where id=:seasonname");
		$QryArr = array(":seasonname"=>$Seasonname);
		$Qry->execute($QryArr);
		$CntRcds = $Qry->rowCount();
		if ($CntRcds > 0) {
			$FetchTeamRows = $Qry->fetch(PDO::FETCH_ASSOC);	
			// $DBRows = $FetchTeamRows['name'];		
		}
		return json_encode($FetchTeamRows);
	}
}
function getTeamsByCustomerid($customer_id, $teamid = '', $sportid) 
{
	global $conn;
	$TeamQryCondn = ($teamid) ? "and id='$customer_id'" : "" ;
	$SportQryCondn = ($sportid) ? "and sport_id='$sportid'" : "" ;

	$TeamQry = $conn->prepare("select * from teams_info where customer_id IN ($customer_id) $SportQryCondn $TeamQryCondn ");
	$TeamQryArr = array(":teamid"=>$teamid, ":customer_id"=>$Customerid, ":sportid"=>$sportid);
	$TeamQry->execute($TeamQryArr);
	$CntTeam = $TeamQry->rowCount();
	if ($CntTeam > 0) {
		$TeamRows = $TeamQry->fetchAll(PDO::FETCH_ASSOC);		
	}
	return json_encode($TeamRows);
}
?>
<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_REQUEST['searchbydiv'])) {
	$season       = $_REQUEST['searchbyseason'];
	$Searchbydiv  = $_REQUEST['searchbydiv'];
	$sportid      = $_REQUEST['sportid'];
	$cid          = $_REQUEST['cid'];
    
    $gameDate = ($_REQUEST['gamedate'])?$_REQUEST['gamedate']:"";
    $gamesplit = explode("/", $gameDate);
    $gameMonth = date("F", mktime(0, 0, 0, $gamesplit[0], 10));
    $DateCondn = "";
    if(!empty($gameDate))        
        $DateCondn = " and DATE_FORMAT(STR_TO_DATE(date ,'%m/%d/%Y'),'%m/%Y')='$gameDate'";
	$seasonCondn = !empty($season) ? "and season='$season'" : "";
    if ($_SESSION['master']==1) {
        $children = $_SESSION['loginid'].",".$_SESSION['childrens'];
        $cid = $children;
    }

	$divCondn = '';
	$divid='';
    
	if (!empty($Searchbydiv)) {
		$resdiv = $conn->prepare("select * from customer_division where name like '{$Searchbydiv}%' and custid in ($cid)");
    } else {
        $resdiv = $conn->prepare("select * from customer_division where name like '{$Searchbydiv}%' and custid='$cid'");  
    }
		$resdiv->execute();
		$Cntresdiv = $resdiv->rowCount();
		if ($Cntresdiv > 0) {
			$FetchRows = $resdiv->fetchAll(PDO::FETCH_ASSOC);
			foreach ($FetchRows as $rowdiv) {
				$divid =  $rowdiv['id'];
			}
		}

        if ($_SESSION['team_manager_id']) {
            $teamlogin_id = $_SESSION['team_manager_id'];
            
        } else {
            $teamlogin_id = '';
        }
        if ($_SESSION['signin'] == 'team_manager') {
            $team_id = " AND (visitor_team_id=$teamlogin_id OR home_team_id=$teamlogin_id)";
        } else {
            $team_id = '';
        }

        if ($_SESSION['master'] == 1) {
            $divCondn = " and division='$divid'";
            $sportIdcondn = $sportid;
        } else {
            if (!empty($divid)) {
                $divCondn = " and division='$divid'";
            } else { 
                $divCondn = " and division=''";
            }

            $sportIdcondn = " and (sport_id='$sportid')";
        }
		// $res = $conn->prepare("select * from games_info where (home_customer_id='$cid' or visitor_customer_id='$cid' )  $sportIdcondn $divCondn $seasonCondn $team_id order by date");
        if (!empty($Searchbydiv)) {
            if ($_SESSION['master']==1) {
                $children=$_SESSION['loginid'].",".$_SESSION['childrens'];
                 $res = $conn->prepare("select * from games_info where (home_customer_id in ($children) or visitor_customer_id in ($children))  $sportIdcondn $divCondn $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");                
            } else {
                $res = $conn->prepare("select * from games_info where (home_customer_id in ($cid) or visitor_customer_id in ($cid))  $sportIdcondn $divCondn $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");
            }
        } else {
            // $res = $conn->prepare("select * from games_info where (home_customer_id in ($cid) or visitor_customer_id in ($cid))  $sportIdcondn $seasonCondn $team_id order by date");
            if ($_SESSION['master']==1) {

                $children=$_SESSION['loginid'].",".$_SESSION['childrens'];
                $res = $conn->prepare("select * from games_info where (home_customer_id IN ($children) or visitor_customer_id in ($children)) $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");
            } else {
                $res = $conn->prepare("select * from games_info where (home_customer_id in ($cid) or visitor_customer_id in ($cid))  $sportIdcondn $seasonCondn $team_id $DateCondn ORDER BY STR_TO_DATE(date, '%m/%d/%Y'),time");
            }
        }
		$resArr = array(":cid"=>$cid, ":sportid"=>$sportid);
   		$res->execute($resArr);

	$resCnt = $res->rowCount();
	if ($resCnt > 0) {
	    $GameRes = $res->fetchAll(PDO::FETCH_ASSOC);
	    $GameDetail = array();
	    $j = 0;
	    foreach ($GameRes as $GameRow) {
	        $GameYear = date("Y", strtotime($GameRow['date']));
	        $GameMonth = date("m", strtotime($GameRow['date']));
	        $MatchDetail = $GameRow['visitor_team_id']. " vs ".$GameRow['home_team_id'];
	        $GameDetail[$GameYear][$GameMonth][$j] = $GameRow;
	        $j++;
	    }   
	
		?>
		<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="col-md-12 mycarousalnew">
                <div class="col-md-4 col-sm-4 col-xs-4 logames ">
                    <i class="fa fa-list font-red-sunglo"></i><span class="caption-subject font-red-mint bold uppercase "> List of games</span>                        
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 monthrcds">                    
                    <div class="gamemonth"><strong class=""></strong>
                    <!-- <input type="hidden" id="datepicker" buttonImage value=""> -->
                    <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 logamesbtn">
                    <a href="manage_game.php" class="btn btn-small addcustomerbtn" style="float:right;margin-top:-1px;">
                 Add Game </a>
                </div>
            </div>           
            <div class="carousel-inner" role="listbox">
            
            <?php 
            foreach ($GameDetail as $gameDetailYear => $GameDetailYearVal) {                        
                foreach ($GameDetailYearVal as $GameDetailMonth => $GameDetailMonthInfo) {
                $first_key = key($GameDetailMonthInfo); 
                $month = date("F", mktime(0,0,0,$GameDetailMonth+1,0,0));
                ?>
                <div class="item <?php if ($first_key == 0) {echo "active"; }?>" month-name="<?php echo $month.", ".$gameDetailYear; ?>" >
                <input type="hidden" id="hiddenmonth" value="<?php echo date('F', strtotime($GameDetailMonth)); ?>">
                <?php 
                    foreach ($GameDetailMonthInfo as $Keys => $Values) { 
                        $VisitName = json_decode(getTeamName($Values['visitor_team_id']), true);
                        $HomeName = json_decode(getTeamName($Values['home_team_id']), true);
                        $DivisionName = json_decode(getDivisionName($Values['division']), true);
                        $GameType = json_decode(getGameType($Values['isLeagueGame']), true);
                        $TournamentName = ($Values['isLeagueGame']) ? $Values['isLeagueGame'] : " " ;

                        $enableModify = "display:none;";
                        if ($_SESSION["usertype"] == "team_manager") {
                            $start_date = new DateTime(date("Y-m-d H:i:s", strtotime($Values["date"]." ".$Values["time"])));
                            $end_date = new DateTime(date("Y-m-d H:i:s"));
                            $interval = $start_date->diff($end_date);
                            if ($interval->y == 0 && $interval->m == 0 && $interval->d <= 3) {
                                $diffDay = $interval->d;
                                $enableModify = "";
                            }
                        }

                        $SportQry = $conn->prepare("SELECT subsports.sport_id,sports.sport_name FROM customer_subscribed_sports as subsports left join sports on subsports.sport_id=sports.sportcode where customer_id=:customer_id");
                        $SportQryArr = array(":customer_id"=>$cid);
                        $SportQry->execute($SportQryArr);
                        $FetchSportName = $SportQry->fetch(PDO::FETCH_ASSOC);
                        $sportname = $FetchSportName["sport_name"];
                        ?>

                            <div class="panel-body rc-padding <?php echo $Values['date'];?>">
                                <div class="row rm-margin opencloseportletcaption">                                
                                    <div class="col-md-12 rm-padding">
                                        <div class="portlet box grey portletdown">
                                            <div class="portlet-title">
                                                <div class="caption captionstyle tools">
                                                    <a href="javascript:;" class="expand" id="shortgamedetail">
                                                        <?php echo $VisitName." vs ".$HomeName." "; ?><small><?php echo "&nbsp;&nbsp;&nbsp;".date("F d, Y", strtotime($Values['date'])); echo " ".$Values['time']; ?></small>
                                                    </a>
                                                </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                                </div>
                                           </div>
                                            <div class="portlet-body portlet-custom expand opencloseportlet">
                                                <div class="slimScrollDiv" >
                                                    <div class="scroller removerightpadding"  data-always-visible="1" data-rail-visible="1" data-rail-color="blue" data-handle-color="red" data-initialized="1">
                                                        <div class="col-md-12 rm-padding table-responsive">
                                                            <table class="table table-hover table-bordered gameinfotable">
                                                                <tr>
                                                                    <th nowrap ><strong>Game Id</strong></th>
                                                                    <th nowrap><strong>Game Name</strong></th>
                                                                    <th nowrap><strong>Visitor Team</strong></th>
                                                                    <th nowrap><strong>Home Team</strong></th>
                                                                    <th nowrap><strong>Division</strong></th>
                                                                    <th nowrap><strong>Game Type</strong></th>
                                                                    <th nowrap><strong>Location</strong></th>
                                                                    <th nowrap><strong>Tournament</strong></th>
                                                                    <th nowrap><strong>Action</strong></th>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap><?php echo $Values['id']; ?></td>
                                                                    <td nowrap><?php echo $Values['game_name']; ?></td>
                                                                    <td nowrap><?php echo $VisitName; ?></td>
                                                                    <td nowrap><?php echo $HomeName; ?></td>
                                                                    <td nowrap><?php echo $DivisionName; ?></td>
                                                                    <td nowrap><?php echo $GameType; ?></td>
                                                                    <td nowrap><?php echo $Values['game_location']; ?></td>
                                                                    <td nowrap><?php echo $Values['tournament']; ?></td>
                                                                    <?php 
                                                                        $GameQryArr = array();
                                                                        $gameid = $Values['id'];
                                                                        $GameQry = $conn->prepare("select * from games_info where id=:gameid");
                                                                        $GameQryArr = array(":gameid"=>$gameid);
                                                                        $GameQry->execute($GameQryArr);
                                                                        $CntGame = $GameQry->rowCount();
                                                                        if ($CntGame > 0) {

                                                                            $GameRows = $GameQry->fetch(PDO::FETCH_ASSOC);
                                                                            $sportid = $GameRows['sport_id'];
                                                                        }

                                                                        $sportnquery = $conn->prepare("select * from sports where sportcode='$sportid'");
                                                                        $sportnquery->execute();
                                                                        $Cntsportnquery = $sportnquery->rowCount();
                                                                        if ($Cntsportnquery > 0) {
                                                                            $sportrows = $sportnquery->fetchAll(PDO::FETCH_ASSOC);
                                                                            foreach ($sportrows as $sportnames) {
                                                                                $teamsportname = $sportnames['sport_name']; 
                                                                                $teamsportname = strtolower($teamsportname);
                                                                            }
                                                                        } else {
                                                                            $teamsportname = "";
                                                                        }
                                                                        ?>
                                                                    <td nowrap>
                                                                        <div class="actions">
                                                                            <a title="Edit game" class="btn btn-success" href="manage_game.php?gid=<?php echo base64_encode($Values['id']); ?>" class="btn btn-default btn-sm">
                                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                            <a title="Delete game" class="btn btn-danger" onclick="return deleteGame('<?php echo $Values['id']; ?>','<?php echo $teamsportname; ?>');">
                                                                            <i class="fa fa-trash" ></i> Delete </a>

                                                                            <!-- <a href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="btn btn-warning modifystatsbtn">
                                                                                    <i class="fa fa-pencil" ></i> Modify 
                                                                            </a> -->
                                                                            <?php if ($sportname == 'basketball') {
                                                                                if ($_SESSION["usertype"] != "team_manager") {
                                                                            ?>
                                                                            <a href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="btn btn-warning modifystatsbtn">
                                                                                <i class="fa fa-pencil" ></i> Modify 
                                                                            </a>
                                                                            <?php } else { ?>
                                                                                 <a style="<?php echo $enableModify; ?>" href="game_stats.php?gid=<?php echo base64_encode($Values['id']); ?>" title="Modify game" class="btn btn-warning modifystatsbtn">
                                                                                <i class="fa fa-pencil" ></i> Modify 
                                                                                </a>

                                                                            <?php } } ?>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </div>                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                    <?php } ?>
                </div><?php
                
                }
            }
            ?>
            </div>
            <a class="left carousel-control carouselstle" href="#myCarousel" role="button" data-slide="prev">
                <i class='fa fa-chevron-left'></i>
                <span class="sr-only">Previous</span>

            </a>
            <a class="right carousel-control carouselstle" href="#myCarousel" role="button" data-slide="next">
                <i class='fa fa-chevron-right'></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
<?php } else {   ?>

<div class="col-md-12 mycarousalnew">
    <div class="col-md-4 col-sm-4 col-xs-4 logames ">
        <i class="fa fa-list font-red-sunglo"></i><span class="caption-subject font-red-mint bold uppercase "> List of games</span>                        
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4 monthrcds">        
        <div class="gamemonth"><strong ><?php echo $gameMonth.", ".$gamesplit[1];?></strong>
        <input type="hidden" id="datepicker" value=""><i class="fa fa-calendar" id="datepicker-button"></i>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4 logames">
        <a href="manage_game.php" class="btn btn-small addcustomerbtn" style="float:right;margin-top:-8px;">
     Add Game </a>
    </div>
</div>
<div class="carousel-inner" role="listbox">
    <div class="item active">
        <div class="panel-body rc-padding carousel-inner ajaxcarousel-inner">
            <div class="row">                                
                <div class="col-md-12">
                    <div class="portlet box grey portletdown">
                        <div class="portlet-title">
                            <div class="caption captionstyle nogamescaption" style="color:#000;background-image:none;display: block !important;width: 100%">
                                No games Found</div>
                                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }  }?>
<script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="assets/custom/js/jquery.mtz.monthpicker.js"></script>
<script src="assets/custom/js/gamelist.js" type="text/javascript"></script>



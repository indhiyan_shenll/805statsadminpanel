<?php 
include_once('session_check.php');
include_once('connect.php'); 

if(isset($_GET['divisionid'])){
	$DivisionId    = $_GET['divisionid'];
	$SeasonId      = $_GET['seasonid'];
	$ConferenceId  = $_GET['conferenceid'];
	
	$_SESSION['divisionid']   = $DivisionId;
	$_SESSION['seasonid']     = $SeasonId;
	$_SESSION['conferenceid'] = $ConferenceId;


	$QryExe = $conn->prepare("select *,seasontbl.name as seasonname,divtble.name as divisionname from customer_conference_division as seasonconfdiv LEFT JOIN customer_conference as custconf ON  seasonconfdiv.conference_id=custconf.id LEFT JOIN customer_division as divtble ON seasonconfdiv.division_id=divtble.id LEFT JOIN customer_season as seasontbl ON seasonconfdiv.season_id=seasontbl.id where division_id=:division_id");
	$Qryarr = array(":division_id"=>$DivisionId);
	$QryExe->execute($Qryarr);
	$QryCntSeasonconf	= $QryExe->rowCount();
	$SelectedSeason='';
	if ($QryCntSeasonconf > 0) {
		$rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC);
		$SelectedSeason = $rowSeason['seasonname']." / ".$rowSeason['conference_name']." / ".$rowSeason['divisionname'];
	}

	header("Location:add_divisionteam.php");
	exit;

}

include_once('header.php'); ?>
<link href="assets/custom/css/addteamtoseason.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
	<form id="addteamform" method="POST">
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">                
                    <div class="col-md-12">
                        <div class=" left-right-padding">
                            <div class="row searchheder">                
                                <div class="col-md-12 searchbarstyle">
									<div class="col-md-2 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group caption font-red-sunglo selecttext">
											<span class="caption-subject bold uppercase">Select season</span>
										</div>									
									</div>

									<div class="col-md-4 col-sm-3 col-xs-12">
										<div class="form-group ">											
											<select class="form-control  border-radius" name="seasonlist" id="seasonlist">
											<option value=''>Select season</option>
											<?php
											$Qry		= $conn->prepare("select * from customer_season where custid=:custid order by season_order desc");
											$Qryarr		= array(":custid"=>$customerid);
											$Qry->execute($Qryarr);
											$QryCntSeason = $Qry->rowCount();
											$DivisionWrapHtml= $AddNewSeasonTree='';
											$Inc =0;
											if ($QryCntSeason > 0) {
												while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){									
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}else{
												echo "<option value=''>No season found</option>";
											}
											?>											
											</select>
											
											<script>$("#seasonlist").val("<?php echo $_SESSION['seasonid'];?>");</script>
										</div>
									</div>

									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="conferencelist" id="conferencelist">
												<option value=''>Select conference</option>	
												<?php
													$Qry		= $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
													$Qryarr		= array(":season_id"=>$_SESSION['seasonid']);
													$Qry->execute($Qryarr);
													$QryCntSeason = $Qry->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['conference_name']."</option>";
														}
													}else{
														echo "<option value=''>No conference found</option>";
													}
												?>
											</select>
											
											<script>$("#conferencelist").val("<?php echo $_SESSION['conferenceid'];?>");</script>
										</div>									
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="divisionlist" id="divisionlist">
												<option value=''>Select division</option>	
												<?php
													$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
													$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid']);

													$QryExeDiv->execute($QryarrCon);
													$QryCntSeason = $QryExeDiv->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['name']."</option>";
														}
													}else{
														echo "<option value=''>No season found</option>";
													}
												?>
											</select>
											<script>$("#divisionlist").val("<?php echo $_SESSION['divisionid'];?>");</script>
										</div>									
									</div>
								</div>
							</div>
                        </div>
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
						<div class="portlet light addteammainwrap">                               
							<div class="portlet-body form">
								<div class="form-body top-padding" style="padding-top:5px;"> 
									<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
									<div class="row">
										<div class="col-xs-5 col-md-5">
											
											<select name="from[]" id="undo_redo" class="form-control border-radius " size="13" multiple="multiple">
											<?php
											$QryExe1		= $conn->prepare("select * from customer_division_team where customer_id=:customer_id and season_id=:season_id");

											$Qryarr = array(":customer_id"=>$customerid,":season_id"=>$_SESSION['seasonid']);

											$QryExe1->execute($Qryarr);
											$QryCntTeam = $QryExe1->rowCount();
											$TeamIdArr  = array();

											if ($QryCntTeam > 0) {
												while ($rowTeam = $QryExe1->fetch(PDO::FETCH_ASSOC)){	
													$TeamIdArr[] = $rowTeam['team_id'];
												}
											}
											$QryExe1		= $conn->prepare("select * from teams_info where customer_id=:custid and team_name!=''");
											$Qryarr		= array(":custid"=>$customerid);
											$QryExe1->execute($Qryarr);
											$QryCntSeason = $QryExe1->rowCount();
											$DivisionWrapHtml= $AddNewSeasonTree='';
											$Inc =0;
											$AssignedTeams ='';
											if ($QryCntSeason > 0) {
												while ($row = $QryExe1->fetch(PDO::FETCH_ASSOC)){													
													if(in_array($row['id'],$TeamIdArr)){ 									
														$AssignedTeams .= "<option value='".$row['id']."' disabled>".$row['team_name']."</option>";
													 
													}else{
														echo "<option value='".$row['id']."'>".$row['team_name']."</option>";
													}
													
												}
												echo $AssignedTeams;
											}else{
												echo "<option value=''>No team found</option>";
											}
											?>	
											</select>
										</div>
										
										<div class="col-xs-2 col-md-2 centeredbtnswrap">
											<!-- <button type="button" id="undo_redo_undo" class="btn btn-primary btn-block">undo</button> -->
											<button type="button" id="undo_redo_rightAll" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-forward"></i></button>
											<button type="button" id="undo_redo_rightSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
											<button type="button" id="undo_redo_leftSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
											<button type="button" id="undo_redo_leftAll" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>
											<!-- <button type="button" id="undo_redo_redo" class="btn btn-warning btn-block">redo</button> -->
										</div>
										
										<div class="col-xs-5 col-md-5 rightsidewrap" >
											
											<select name="selectedteam[]" id="undo_redo_to" class="form-control border-radius requiredcs" size="13" multiple="multiple">
											<option value="" class="emptyselected"></option>
											<?php											

												$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();										
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){	
														if($rowTeam['team_name']!=''){
														echo "<option value='".$rowTeam['id']."'>".$rowTeam['team_name']."</option>";
														}
													}
												}
											?>

											</select>
											<div class="row">
												<div class="col-sm-6">
													<button type="button" id="undo_redo_move_up" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
												</div>
												<div class="col-sm-6">
													<button type="button" id="undo_redo_move_down" class="btn btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
												</div>
											</div>

										</div>
										<div class="col-xs-5 col-md-5 loadingwrap" >
											<img src="images/loading-publish.gif">
										</div>
									</div>
								</div> 
								
								 <div class="">
										<button type="button" class="btn green-meadowsave" name="addsubmit" id="addteambtnid">Save</button>
										<a href="manage_season.php"><button type="button" class="btn red" id="cancelbtn">Cancel</button></a>
								 </div>   
							</div>
						</div>           
                    </div>                    
                
            </div>            
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
</div>

</form>
<!-- END CONTAINER -->

<?php include_once('footer.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="assets/global/plugins/multiselect.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

 <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
$(document).ready(function() {
	$('#undo_redo').multiselect({
		sort:false,
        search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="    margin-bottom: 3px;"><a href="add_divisionplayer.php"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Assign Players</button></a></p><p class="clearfix" style="    margin-bottom: 0px;"><label>Selected Team</label></p>',
        },
		rightSelected:true,
		afterMoveToRight: function($left, $right, $options) { }
    });


$(document).on('click','#addteambtnid', function(evt) {
	if (!$("#addteamform").validate()) { 
		return false;
	} 	
	$("#addteamform").submit();		
});


$.validator.addMethod('requiredcs',function(value){
	if(value==''){
		return false;
	}else{
		return true;
	}
},""
);


$("#addteamform").validate({
	 rules: {
		 conferencelist:{requiredcs :true},
		 divisionlist:{requiredcs:true},
		 "selectedteam[]":{requiredcs:true},
		 },
		messages: {
		 conferencelist:{requiredcs:"Please select conference"},
		 divisionlist:{requiredcs:"Please select division"},
		 "selectedteam[]":{requiredcs:"Please add team"},
	   },
		submitHandler: function (form) {			
			var $form = $(form);
			var FormArr  = $form.serialize();	
			$("#undo_redo_to option").each(function()
			{
				$(this).prop('selected', true);
			});

			var seasonlist       =  $('#seasonlist').val();	
			var conferencelist   =  $('#conferencelist').val();
			var divisionlist     =  $('#divisionlist').val();
			var selectedteam     =  $('#undo_redo_to').val();

			$(".rightsidewrap").hide();
			$(".loadingwrap").show();

			var result=$.ajax({
			 type:"POST",			
			 url:"assignteamtoseason.php",
			 data:{"seasonlist":seasonlist,"conferencelist":conferencelist,"divisionlist":divisionlist,"selectedteam":selectedteam},
			 success: function(data) {
			    $(".rightsidewrap").show();
				$(".loadingwrap").hide();
				$( ".page-content").empty().append(result.responseText);
				return false;
			 },
			 error: function(data) {
				alert("something wrong");
				
			 },
			}); 		 }
	});

});

$(document).on('change','#seasonlist,#conferencelist,#divisionlist', function(evt) {
		var $this          =  $(this);	
		var SelectAttrid   =  $this.attr("id");
		var post_type      =  '';
		var SeasonId       =  '';	
		var ConferenceId   =  '';
		var DivisionId     =  '';

		if(SelectAttrid=="seasonlist"){
			if($(this).val()!=''){
				post_type      =  "seasonlist";
				SeasonId       =  $('#seasonlist').val();		
			}
		}else if(SelectAttrid=="conferencelist"){
			post_type      =  "conferencelist";
			SeasonId       =  $('#seasonlist').val();
			ConferenceId   =  $('#conferencelist').val();
			
		}else if(SelectAttrid=="divisionlist"){
			post_type      =  "divisionlist";
			SeasonId       =  $('#seasonlist').val();	
			ConferenceId   =  $('#conferencelist').val();
			DivisionId     =  $('#divisionlist').val();
			
		}
		
		var result=$.ajax({
			 type:"POST",
			 async:false,
			 url:"selectseasonlist.php",
			 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},
			 dataType:"text",
		 }); 	
		console.log(result);
		if(result.statusText=="OK"){
			if(post_type=="seasonlist"){					
				$('#conferencelist').empty().append(result.responseText);
				$('#divisionlist').empty().append("<option value=''>Select</option>");
			}else if(post_type=="conferencelist"){					
				$('#divisionlist').empty().append(result.responseText);
				
			}else{
				$('.addteammainwrap').empty().append(result.responseText);
			}
			return false;
			
		}else{
			return true;
		}		
		
	
});
</script>
   
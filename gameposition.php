<?php
include_once('session_check.php'); 
include_once('connect.php');
include_once('common_functions.php'); 
include_once('usertype_check.php');
//print_r($_SESSION); exit;
// Get Sports name Start Here
if(isset($_GET['sport'])){
	$sportname= $_GET['sport'];
}


// Get Sports name End Here
// Get Select Division start Here
if ($_SESSION['master'] != 1) { 
    $Selectdivision='<select class="form-control divisionlist ampl_width100" name="divisionlist" id="divisionlist">';
    $Selectdivision.='<option value="">---Select---</option>';
    $Selectdivision.='<option value="addnew">Add New</option>';
    $FetchConf = json_decode(getCustomerDivisions($Cid),true);
    foreach ($FetchConf as $FetchRows) { 
    	$devid=$FetchRows["id"];
    	$name=$FetchRows["name"];
        $Selectdivision.="<option value=".$devid.">".$name."</option>";
    }              
    $Selectdivision.='</select>';
} else {
    $Selectdivision='<select class="form-control divisionlist ampl_width100" name="divisionlist" id="divisionlist">';
    $Selectdivision.='<option value="">---Select---</option>';
    $children = array($_SESSION['childrens']);
    $ids = $_SESSION['loginid'].",".join(',',$children); 
    $divlist = $conn->prepare("select * from customer_division where custid in ($ids)");
    $divlist->execute();
    $Cntdivlist = $divlist->rowCount();
    if ($Cntdivlist > 0) {
        $FetchDiv = $divlist->fetchAll(PDO::FETCH_ASSOC);
        foreach ($FetchDiv as $FetchRows) {
        	$devsionid=$FetchRows["id"];
        	$devsionname=$FetchRows["name"];
          $Selectdivision.="<option value=".$devsionid.">".$devsionname."</option>"; 
        }
    }            
    $Selectdivision='</select>';
} 
// Get Select Division End Here

// division Rlues Start here
 if($SportName == 'basketball') { 
    $rulelist='<option value="FIBA">FIBA</option>
    <option value="HS">HS</option>
    <option value="NBA">NBA</option>
    <option value="NCAA">NCAA</option>
    <option value="NCAAW">NCAAW</option>';
} 
if ($SportName == 'baseball' || $SportName == 'softball') { 
	$rulelist='<option value="HS_BA">HS_BA</option>
    <option value="HS_SB">HS_SB</option>
    <option value="MLB">MLB</option>
    <option value="NCAA_BA">NCAA_BA</option>
    <option value="NCAA_SB">NCAA_SB</option>';
} 
if ($SportName == 'football') { 
     $rulelist='<option value="Arena">Arena</option>
    <option value="HS">HS</option>
    <option value="Indoor">Indoor</option>
    <option value="NCAA">NCAA</option>
    <option value="NFL">NFL</option>';
} 

if ($SportName != 'football') { 
    $click_entry="selected";
    $play_entry="";
    $score_entry="";
} 
else if($SportName == 'softball' || $SportName == 'baseball'){
    $play_entry="selected";
    $score_entry="";
    $click_entry="";
 } else {
    $score_entry="selected";
    $click_entry="";
    $play_entry="";
 }
$entry_mode="<option value='10' ".$click_entry.">2 Click Entry</option>"; 
$entry_mode.="<option value='0' ".$score_entry.">Box Score Entry</option>";
$entry_mode.="<option value='2' ".$play_entry.">Play by Play</option>";           
// devision Rules end here
// Get select season data
$FetchDiv = json_decode(getCustomerSeasons($Cid),true);
foreach ($FetchDiv as $FetchRows) { 
	$seaid=$FetchRows["id"];
	$seaname=$FetchRows["name"];
    $season.="<option value=".$seaid."> ".$seaname."</option>";
}
// Gender selected

if($SportName =='softball' || $SportName =='baseball'){
	 $selected_female="selected";
} else {
	$selected_male="selected";
}
$selecgender="<option value='1' ".$selected_male.">Male</option>";
$selecgender.="<option value='2' ".$selected_female.">Female</option>";
 $signlegameformentry = '				
			<form action="" class="form-inline multipleplayerformgrp" method="POST" enctype="multipart/form-data">
				<div class="portlet light ">
					<div class="portlet-body form">
						<div class="form-body bulkplayerformbody">
							<div class="">
								<div class="uploadstatus" style="display:inline-block;width:16px;height:16px;">
									<img src="images/loading-publish.gif" style="width:16px;height:16px;" class="uploadstatusimg">
								</div>
								
								<div class="form-group">
									<label for="gamename">Game Name<span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control ampl_width100 plnew_firstname" maxlength="25" id="gamename" name="gamename" value="" placeholder="Gamename *">
								</div>
								<div class="form-group">
									<label for="gamedate">Game Date<span class="required"> * </span></label>
                                    <input type="text" class="form-control  dategame date-picker ampl_width100" id="gamedate" name="gamedate" placeholder="Game Date" value="" >
							    </div>
								
								<div class="form-group">
									<label for="gametime">Game Time<span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control time-picker timepicker timepicker-no-seconds ampl_width100 plnew_lastname" maxlength="25" id="gametime" name="gametime" value="" placeholder="Gametime *">
								</div>
								<div class="form-group">
									<label for="timezone">Time Zone</label>
									 <select class="form-control" name="timezone" id="timezone" >
                                        <option value="Pacific">PST</option>
                                        <option value="Mountain">MST</option>
                                        <option value="Central">CST</option>
                                        <option value="Eastern">EST</option>               
                                      </select>
							    </div>
								<div class="form-group">
									<label for="visitor">Visitor <span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control visitorteam ampl_width100" maxlength="25" id="visitor" name="visitor"  placeholder="Visitor" value="s">
									<div class="display_resulterrormsg display_visitor_result" id=""></div>
								</div>
								<div class="form-group">
									<label for="home">Home <span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control ampl_width100 hometeam" maxlength="25" id="home" name="home"  placeholder="Home" value="s">
									<div class="display_resulterrormsg display_home_result" id=""></div>
								</div>
								<div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select class="form-control ampl_width90" name="gender" id="gender" >
                                    '.$selecgender.'
                                                  
                                    </select>
                                </div>
								
								<div class="form-group">
									<label for="location">Location</label>
									<input type="text" class="form-control ampl_width100 " id="location" name="location" value="" placeholder="Location">
								</div>
								<div class="form-group">
								    <label for="divisionlist">Division <span class="cs_mandatory">*</span></label>
									   '.$Selectdivision.'
								</div>
								<div class="form-group">
								    <label for="divisionlist">Division Rule <span class="cs_mandatory">*</span></label>
									<select class="form-control ampl_width100" name="rulelist" id="rulelist" disabled="">
                                        <option value="">---Select---</option>
                                        '.$rulelist.'
                                    </select>
								</div>						

								<div class="form-group">
									<label for="gametype">Game Type <span class="cs_mandatory">*</span></label>
									<select class="form-control ampl_width100" name="gametype" id="gametype">
                                        <option value="2">Non –Conference</option>
                                        <option value="1">Preseason</option>
                                        <option value="3">Conference</option>
                                        <option value="4">Post Season</option>            
                                    </select>
								</div>
								<div class="form-group">
									<label for="tournament">Tournament Name(If applicable)</label>
									<input type="text" class="form-control ampl_width100 uniformint"  maxlength="2" id="uniform" name="tournament" value="" placeholder="Tournament Name">
								</div>
								<div class="form-group">
									<label for="seasonlist">Season</label>          
									<select class="form-control seasonlist ampl_width100" name="seasonlist" id="seasonlist">
									  <option value="">---Select---</option>
									  <option value="addnew">Add New</option>
										'.$season.'
									</select>
								</div>
								<div class="form-group">
									<label for="entrylist">Entry Mode <span class="cs_mandatory">*</span></label>  
									<select class="form-control ampl_width100" name="entrylist" id="entrylist">
										   '.$entry_mode.'     
									</select>
								</div>
								<div class="form-group">
									<label for="gamenotes">Game Notes</label>
									<textarea class="form-control" id="gamenotes" rows="10" maxlength="500" name="gamenotes" placeholder="Game Notes"></textarea>
								</div>
								<div class="form-group">
									<label for="gamelocation">Game Location</label>
									<textarea class="form-control" id="gamelocation" rows="10" maxlength="500" name="gamelocation" placeholder="Game Location"></textarea>
								</div>		
							</div>
						</div>
					</div>
				</div>
			</form>';
?>







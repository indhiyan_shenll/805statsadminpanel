<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner"> 2016 &copy; Powered By
				<a target="_blank" href="http://www.805stats.com">805stats.com</a>. All Rights Reserved. 			   
			</div>
			<!-- END FOOTER -->
		</div>
		
		<!--[if lt IE 9]>
		<script src="../assets/global/plugins/respond.min.js"></script>
		<script src="../assets/global/plugins/excanvas.min.js"></script> 
		<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
		<![endif]-->
		<!-- BEGIN CORE PLUGINS -->
	   
		<script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

		<script src="assets/global/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
		<script src="assets/global/scripts/jquery.validate.js" type="text/javascript"></script>
		<script src="assets/global/scripts/custom.js" type="text/javascript"></script>
		<script src="assets/custom/js/bootstrap-multiselect.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<!-- <script src="assets/pages/form-validation.js" type="text/javascript"></script> -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
		<script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
		<script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
	  
		<!-- END THEME LAYOUT SCRIPTS -->
		<!-- TABLE SCRIPT START HERE-->
		<script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>

		<style>
		.modal-content {
			border-radius: 10px !important;
		}
		.caption-width{
			width:100%;
		}
		.caption-width-team{
			width:100%;
		}
		.mode-color{
			background-image: -webkit-linear-gradient(top, #3C4C5B, #263849);
		    border-color: rgba(0, 0, 0, 0.15);
		    color: #FFF;
		    padding: 5px 14px;
		    font-size: 13px;
		    float: right;
		    margin: 0px;
		    background-image: -webkit-linear-gradient(top, #70A62F, #70A62F);
		    border-color: rgba(0, 0, 0, 0.15);
		    border-radius: 4px !important;
		}
		.modal-footer .btn.btn-default{
			background-image: -webkit-linear-gradient(top, #E95D5D, #E40304);
			border-color: rgba(0, 0, 0, 0.15);
			border-radius: 4px !important;
			color: #fff;
			line-height: 1.44;
			padding: 5px 20px;
			width: 100px;
		}
		</style>
	   <!-- TABLE SCRIPT END HERE-->
    </body>
</html>
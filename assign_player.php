<?php 
include_once('session_check.php'); 
include_once('connect.php');

if (isset($_REQUEST["assignplayer"])) {

    $teamid = $_REQUEST["teamid"];
    $gameid = $_REQUEST["gameid"];
    $teamtype = $_REQUEST["assignteamtype"];

    $GameQry = $conn->prepare("select * from game_details where id=:id");
    $GameQryArr = array(":id"=>$gameid);
    $GameQry->execute($GameQryArr);
    $fetchRow = $GameQry->fetch(PDO::FETCH_ASSOC);
    $gameid = $fetchRow["id"];
    $season = $fetchRow["season"];
    $date = explode("-",$fetchRow["date"]);
    $year = $date[0];
    $assign_player_id = json_decode(stripslashes($_REQUEST["assignplayer"]), true);

    // $playerStatus = "";
    for ($i=0;$i<count($assign_player_id);$i++) {
        $SelectQry = $conn->prepare("SELECT * FROM player_info where id = ".$assign_player_id[$i]);
        $SelectQry->execute();
        $Cntplayer = $SelectQry->rowCount();

        if ($Cntplayer > 0) {
            $fetchPlayerData = $SelectQry->fetch(PDO::FETCH_ASSOC);
            $playercode = $fetchPlayerData["id"];
            $checkname = strtoupper($fetchPlayerData["lastname"]).", ".strtoupper($fetchPlayerData["firstname"]);
            // $gp = $gs = '0';
            // $fgm = $fga = $fgm3 = $fga3 = $ftm = $fta = $tp = $oreb = $dreb = $treb = $pf = $tf = $ast = $to1 = $blk = $stl = $min = 0; 
            // $insertPlayer = $conn->prepare("INSERT INTO individual_player_stats(playercode, year, season, checkname, gamecode, teamcode, gp, gs, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min) VALUES (:playercode, :year, :season, :checkname, :gamecode, :teamcode, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min)");
            // $insertPlayerArr = array(":playercode"=>$playercode, ":year"=>$year, ":season"=>$season, ":checkname"=>$checkname, ":gamecode"=>$gameid, ":teamcode"=>$teamid, ":gp"=>$gp, ":gs"=>$gs, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":ast"=>$ast, ":to1"=>$to1, ":blk"=>$blk, ":stl"=>$stl, ":min"=>$min);
            // $insertres = $insertPlayer->execute($insertPlayerArr);
            // $last_insert_playerid = $conn->lastInsertId();
            $playerStatus[$i]["playername"] = $checkname;
            $playerStatus[$i]["playercode"] = $playercode;
            $playerStatus[$i]["teamtype"] = $teamtype;
            $playerStatus[$i]["status"] = "success";
        }
             
    }
    echo json_encode($playerStatus);
}
exit;
<?php 
include_once('connect.php');
include_once('session_check.php');
include_once('usertype_check.php');
include_once('common_functions.php');

if($_SESSION['loginid']=='')  {
    header("Location:login.php");
    exit;
}
$cid = $Cid;

if ($_SESSION['team_manager_id']) {
    $teamlogin_id = $_SESSION['team_manager_id'];
    $chk_team_id= " AND id=".$teamlogin_id;
} else {
	$teamlogin_id = '';
}

if(isset($_GET["p_id"])){
   $playerid=base64_decode($_GET["p_id"]);
} else {
   $playerid="";
}



if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
       $getSportsRow=$get_sport_qry->fetch();
       $sportid= $getSportsRow['sportcode'];
    }
}

    $player_qry_str = "SELECT * FROM player_info where id=:player_id";
    $get_player_qry = $conn->prepare($player_qry_str);
    $get_player_qry->execute(array(":player_id"=>$playerid));
    $get_player_rowCount = $get_player_qry->rowCount();
    if($get_player_rowCount>0){
        $getResRow=$get_player_qry->fetch();
        $first_name_db    =   $getResRow["firstname"];
        $last_name_db     =   $getResRow["lastname"];
        $gender_db        =   $getResRow["sex"];
        $grade_db         =   $getResRow["grade"];
        $age_db           =   $getResRow["age"];
        $school_db        =   $getResRow["school"];
        $pre_school_db    =   $getResRow["prev_school"];
        $player_note_db   =   $getResRow["player_note"];
        $height_db        =   $getResRow["height"];
		$weight_db        =   $getResRow["weight"];
		$dob_db           =   $getResRow["date_of_birth"];
		$uniform_no_db    =   $getResRow["uniform_no"];
		$position          =   $getResRow["position"];
		$team_id_db       =   $getResRow["team_id"];
		$home_town_db     =   $getResRow["hometown"];
		$image_db         =   $getResRow["image"];
	    $sportid          =   $getResRow["sport_id"];
		$get_player_id    =   $getResRow["id"];
		$mode="Edit";
	} else {
        $first_name_db    =   "";
        $last_name_db     =   "";
        $gender_db        =   "";
        $grade_db         =   "";
        $age_db           =   "";
		$school_db        =   "";
        $pre_school_db    =   "";
        $player_note_db   =   "";
        $height_db        =   "";
        $weight_db        =   "";
        $dob_db           =   "";
        $uniform_no_db    =   "";
        $position         =   "";
        $team_id_db       =   "";
        $home_town_db     =   "";
        $home_town_db     =   "";
        $image_db         =   "";
        $sport_id        =   "";
		$get_player_id	= "";
        $mode="Add";
}  

// echo $mode;exit;

if($sportid!=""){
$sportnquery = "select * from sports where sportcode=:sportid";
$get_sportn_qry = $conn->prepare($sportnquery);
$get_sportn_qry->execute(array(":sportid"=>$sportid));
$get_sportnr_rowCount = $get_sportn_qry->rowCount();
	if($get_sportnr_rowCount>0){
		$getResSport=$get_sportn_qry->fetch();
		$sport_name    =   $getResSport["sport_name"];
		$sportname= strtolower($sport_name);
	} else{
	    $sportname="";
    }
} 



if(isset($_POST["first_name"])){
	//print_r($_POST);exit;
	$first_name 		=	$_POST['first_name']; 
    $last_name		    =	$_POST['last_name'];
    $gender		        =	$_POST['sel_gender'];
    $age		    	=	$_POST['age'];
    $school_name		=	$_POST['school_name'];
    $grade			    =	$_POST['grade'];
    $uniform_no			=	$_POST['uniform_no'];
    $sel_team	        =	$_POST['sel_team'];
    $sel_position	    =	$_POST['sel_position'];
    $home_town			=	$_POST['home_town'];
    $player_note	    =	$_POST['player_note'];
    $height				=	$_POST['player_height'];
    $weight		    	=	$_POST['player_weight'];
    $dob		        =	$_POST['dob'];
    $pre_school			=	$_POST['pre_school'];
    $isActive           =   "1";

	if(!empty($_FILES["player_image"]["name"])){

         if(is_file(SYSTEM_ROOT_PATH."/uploads/players/".$upfilename)){
            unlink(SYSTEM_ROOT_PATH."/uploads/players/".$upfilename);
            unlink(SYSTEM_ROOT_PATH."/uploads/players/thumb/".$upfilename);
		}

		    $filename=$_FILES["player_image"]["name"];
            $extension = pathinfo($filename,PATHINFO_EXTENSION);
            $upfilename=md5(time()).'.'.$extension; 
            move_uploaded_file($_FILES["player_image"]["tmp_name"],SYSTEM_ROOT_PATH."/uploads/players/".$upfilename);

	        $data = array(
             'width' => "200",
             'height' =>"200",
			 'image_source' =>SYSTEM_ROOT_PATH."/uploads/players/".$upfilename,
             'destination_folder' =>SYSTEM_ROOT_PATH."/uploads/players/thumb/",
             'image_name' =>$upfilename
            );     
           $cropsucessfully = cropImage($data);
    }

	$created_date		= date("Y-m-d H:i:s");

    

	if($mode=="Edit"){

		if($upfilename=="")

			$upfilename =	$_POST['hnd_img'];

		$update_results=array(":firstname"=>$first_name, ":lastname"=>$last_name, ":sex"=>$gender, ":age"=>$age, ":schoolname"=>$school_name, ":grade"=>$grade, ":uniform_no"=>$uniform_no,":teamid"=>$sel_team, ":position"=>$sel_position, ":hometown"=>$home_town,":playernote"=>$player_note,":height"=>$height,":weight"=>$weight, ":imageuploads"=>$upfilename, ":dob"=>$dob,":preschool"=>$pre_school, ":id"=>$playerid);

		$updateQry="update player_info set firstname=:firstname, lastname=:lastname, sex=:sex, age=:age, school=:schoolname, grade=:grade, uniform_no=:uniform_no, team_id=:teamid, position=:position, hometown=:hometown, player_note=:playernote, height=:height, weight=:weight, image=:imageuploads, date_of_birth=:dob, prev_school=:preschool where id=:id";//customer_id=:cid,
       $prepupdateQry=$conn->prepare($updateQry);
       $updateRes=$prepupdateQry->execute($update_results);
       if($updateRes){
        	header('Location:player_list.php?msg=2&sport='.$sportname);
        	exit;
       }
    }
    else{

		$insert_results=array(":cid"=>$cid, ":firstname"=>$first_name, ":lastname"=>$last_name, ":sex"=>$gender, ":age"=>$age, ":schoolname"=>$school_name, ":grade"=>$grade, ":uniform_no"=>$uniform_no,":teamid"=>$sel_team, ":position"=>$sel_position, ":hometown"=>$home_town,":playernote"=>$player_note,":height"=>$height,":weight"=>$weight, ":imageuploads"=>$upfilename,":dob"=>$dob,":preschool"=>$pre_school, ":isActive"=>$isActive,":sportsid"=>$sportid ,":date_added"=>$created_date);

        //print_r($insert_results); exit;
       // $insertqry="insert into player_info(customer_id, firstname, lastname, sex, age, school, grade, uniform_no, team_id, position, hometown, player_note, height, weight, image, date_of_birth, prev_school, isActive, sport_id, date_added)values(:cid, :firstname, :lastname, :sex, :age, :schoolname, :grade, :uniform_no, :teamid, :position, :hometown, :playernote, :height, :weight, :imageuploads, :dob, :preschool, :isActive, :sportsid, :date_added)";
       // $prepinsertqry=$conn->prepare($insertqry);
       // $insertRes=$prepinsertqry->execute($insert_results);

        $insertplayer = "INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, hometown, player_note, height, weight, image, isActive, date_of_birth, sport_id, date_added) VALUES ('$cid', '$first_name', '$last_name', '$gender', '$age', '$school_name', '$pre_school', '$grade', '$uniform_no', '$sel_team', '$sel_position', '$home_town', '$player_note', '$height', '$weight', '$upfilename', '$isActive', '$dob', '$sportid', '$created_date')";
		$prepinsertqry=$conn->prepare($insertplayer);
        $insertRes=$prepinsertqry->execute();
       if($insertRes){
			header('Location:player_list.php?msg=1&sport='.$sportname);
            exit;
        }
    }
}
include_once('header.php');

?>
<link href="assets/custom/css/manageplayer.css" rel="stylesheet" type="text/css" />
<script src="assets/custom/js/jquery.form.min.js"></script>
<!-- BEGIN CONTENT -->
    <style>
    .portlet.light>.portlet-title>.caption {
	    color: #666;
	    padding: 4px 0;
    }
    </style>
    <div class="page-content-wrapper">

        <!-- BEGIN CONTENT BODY -->

        <div class="page-content">

            <!-- BEGIN PAGE HEADER-->

            <div class="row">

            	<form role="form" name="frm_manage_player" id="frm_manage_player" method="Post" enctype="multipart/form-data">

            		<input type="hidden" name="hnd_img" id="hnd_img" value="<?php echo  $image_db ?>">

            		<input type="hidden" id="sportid" value="<?php echo $sportid; ?>" name="sportid">

                    <input type="hidden" id="sportn" value="<?php echo $sportname; ?>" name="sportn">

            		<input type="hidden" id="hnd_player_id" name="hnd_player_id" value=""> 
					<input type="hidden" id="hnd_addplayer" name="hnd_addplayer" value=""> 
					<input type="hidden" id="edit_player_id" value="<?php echo $get_player_id;?>" name="edit_palyer_id">

	                <div class="col-md-6 ">

	                    <!-- BEGIN SAMPLE FORM PORTLET-->

	                    <div class="portlet light bg_color_white_portlet bg_color_white_light" style="min-height:527px;">

	                        <div class="portlet-title">

	                            <div class="caption font-red-sunglo caption-width" >
                                  <i class="icon-settings font-red-sunglo" ></i>
                                   <span class="caption-subject bold uppercase">Player Information </span>
                                   <p class="mode-color"><?php echo $mode;?></p>
                                   
	                            </div>

	                        </div>

	                        <div class="portlet-body form">

	                            <div class="form-body">

	                            	<div class="col-md-12 col-sm-12 col-xs-12">

		                                <div class="form-group col-md-6 playerinfo_paddingleft">

		                                    <label>First Name <span class="error">*</span></label>

		                                    <input class="form-control input-sm" type="text" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $first_name_db;?>"/> 
											<label id="first_name-error" style="margin-bottom:0px;" class="error" for="first_name" >Please enter first name</label>
		                                </div>

		                                <div class="form-group col-md-6 playerinfo_paddingright">

		                                    <label>Last Name <span class="error">*</span></label>

		                                    <input class="form-control input-sm" type="text" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $last_name_db;?>"/> 
											<label id="last_name-error" class="error" style="margin-bottom:0px;" for="last_name">Please enter last name</label>
										<label id="last_name-error" class="error" for="last_name">Please enter last name</label>
										</div>

	                                </div>

	                                <div class="col-md-12 col-sm-12 col-xs-12">

		                                <div class="form-group col-md-6 playerinfo_paddingleft">

	                                     	<div class="form-group">

	                                            <label>Gender</label>

	                                            <select class="form-control input-sm" id="sel_gender" name="sel_gender">

	                                            	<option value="">Select</option>

	                                                <option value="male">Male</option>

													<option value="female">Female</option>

											    </select>

											    <script>$("#sel_gender").val("<?php echo $gender_db;?>")</script>

	                                        </div>

		                                </div>  

	                                    <div class="form-group col-md-3 playerinfo_paddingcenter">

		                                    <label>Height</label>

		                                    <input class="form-control input-sm" type="text" name="player_height" id="player_height" placeholder="Height" value="<?php echo $height_db;?>"/> 

		                                </div>

		                                 <div class="form-group col-md-3 playerinfo_paddingright">

		                                    <label>Weight</label>

		                                    <input class="form-control input-sm" type="text" name="player_weight" id="player_weight" placeholder="Weight" value="<?php echo $weight_db;?>"/> 

		                                </div>

	                                </div>

	                                <div class="col-md-12 col-sm-12 col-xs-12">

		                                 <div class="form-group col-md-3 playerinfo_paddingleft">

		                                    <label>Date of Birth</label>

		                                    <input class="form-control input-sm form-control-inline  date-picker" type="text" name="dob" id="dob" placeholder="Date of Birth" value="<?php echo $dob_db;?>"/> 

		                                </div>

	                                    <div class="form-group col-md-3 playerinfo_paddingcenter">

		                                    <label>Age</label>

		                                    <input class="form-control input-sm" type="text" name="age" id="age" placeholder="Age" value="<?php echo $age_db;?>"/> 

	                                    </div>

	                                     <div class="form-group col-md-6 playerinfo_paddingright">

		                                    <label>School</label>

		                                    <input class="form-control input-sm" type="text" name="school_name" id="school_name" placeholder="School" value="<?php echo $school_db;?>"/>

	                                	</div>

	                                </div>



	                                <div class="col-md-12 col-sm-12 col-xs-12">

		                               <div class="form-group col-md-6 playerinfo_paddingleft">

		                                    <label>Grade</label>

		                                    <input class="form-control input-sm" type="text" name="grade" id="grade" placeholder="Grade" / value="<?php echo $grade_db;?>"> 

	                                	</div>

		                                <div class="form-group col-md-6 playerinfo_paddingright">

		                                    <label>Previous School(if applicable)</label>

		                                    <input class="form-control input-sm" type="text" name="pre_school" id="pre_school" placeholder="Previous School" value="<?php echo $pre_school_db;?>" / > 

		                                </div>  

	                                </div> 

	                                <div class="col-md-12 col-sm-12 col-xs-12">

		                               <div class="form-group">

	                                    <label>Hometown</label>

	                                    <textarea class="form-control input-sm" name="home_town" id="home_town" rows="3"><?php echo $home_town_db;?></textarea>

	                                   </div>

	                                </div>

	                            </div> 

                                

	                        </div>

	                    </div>

	                </div>

	                <div class="col-md-6 ">

	                    <!-- BEGIN SAMPLE FORM PORTLET-->

	                    <div class="portlet light  team_bio_portlet" style="min-height:525px;">

	                        <div class="portlet-title">

	                            <div class="caption font-red-sunglo">

	                                <i class="icon-settings font-red-sunglo"></i>

	                                <span class="caption-subject bold uppercase">Team & bio Information</span>

	                            </div>

	                        </div>

	                        <div class="portlet-body form"> 

	                            <div class="form-body">

	                                <div class="col-md-12 col-sm-12 col-xs-12">

		                                <div class="form-group col-md-4 playerinfo_paddingleft">

		                                    <label>Uniform No <span class="error">*</span></label>

		                                    <input class="form-control input-sm" type="text" name="uniform_no" id="uniform_no" placeholder="Uniform No" value="<?php echo $uniform_no_db;?>" maxlength="3"/> 
											<label id="uniform_no-error" class="error" for="uniform_no">Please enter uniform no</label>
		                                </div>
										
		                                <div class="form-group col-md-4 playerinfo_paddingcenter">

	                                     	<div class="form-group">

	                                            <label>Position</label>

	                                            <select class="form-control input-sm" id="sel_position" name="sel_position">

	                                            	<option value="">select</option>

	                                            	<?php if($sportname=='basketball'){ ?>

													<option <?php if($position=='G') { echo "selected" ;} ?> value="G">G</option>

													<option <?php if($position=='C') { echo "selected" ;} ?> value="C">C</option>

													<option <?php if($position=='F') { echo "selected" ;} ?> value="F">F</option>

													<option <?php if($position=='SG') { echo "selected" ;} ?> value="SG">SG</option>

													<option <?php if($position=='GF') { echo "selected" ;} ?> value="GF">GF</option>

													<option <?php if($position=='PG') { echo "selected" ;} ?> value="PG">PG</option>

													<option <?php if($position=='PF') { echo "selected" ;} ?> value="PF">PF</option>

													<option <?php if($position=='SF') { echo "selected" ;} ?> value="SF">SF</option> <?php } ?>



													<?php if($sportname=='baseball' || $sportname=='softball'){ ?>

													<option value="Extra fldr">Extra fldr</option>

													<option value="Pitcher">Pitcher</option>

													<option value="Catcher">Catcher</option>

													<option value="First Base">First Base</option>

													<option value="Second Base">Second Base</option>

													<option value="Third Base">Third Base</option>

													<option value="Shortstop">Shortstop</option>

													<option value="Left Field">Left Field</option>

													<option value="Center Field">Center Field</option>

													<option value="Right Field">Right Field</option>

													<option value="Des Hitter">Des Hitter</option>

													<option value="Des Player">Des Player</option>

													<option value="Pinch Hit">Pinch Hit</option>

													<option value="Pinch Run">Pinch Run</option>

													<option value="Infielder">Infielder</option>

													<option value="Outfielder">Outfielder</option>

													<option value="Utility">Utility</option>

													 <?php } ?>







													<?php if($sportname=='football'){ ?>

													<option <?php if($position=='C') { echo "selected" ;} ?> value="C">C</option>

													<option <?php if($position=='C/G') { echo "selected" ;} ?> value="C/G">C/G</option>

													<option <?php if($position=='CB') { echo "selected" ;} ?> value="CB">CB</option>

													<option <?php if($position=='DB') { echo "selected" ;} ?> value="DB">DB</option>

													<option <?php if($position=='DE') { echo "selected" ;} ?> value="DE">DE</option>

													<option <?php if($position=='DL') { echo "selected" ;} ?> value="DL">DL</option>

													<option <?php if($position=='DT') { echo "selected" ;} ?> value="DT">DT</option>

													<option <?php if($position=='FB') { echo "selected" ;} ?> value="FB">FB</option>

													<option <?php if($position=='FS') { echo "selected" ;} ?> value="FS">FS</option>

													<option <?php if($position=='G') { echo "selected" ;} ?> value="G">G</option>

													<option <?php if($position=='G/T') { echo "selected" ;} ?> value="G/T">G/T</option>

													<option <?php if($position=='K') { echo "selected" ;} ?> value="K">K</option>

													<option <?php if($position=='KR') { echo "selected" ;} ?> value="KR">KR</option>

													<option <?php if($position=='LB') { echo "selected" ;} ?> value="LB">LB</option>

													<option <?php if($position=='NT') { echo "selected" ;} ?> value="NT">NT</option>

													<option <?php if($position=='OL') { echo "selected" ;} ?> value="OL">OL</option>

													<option <?php if($position=='L') { echo "selected" ;} ?> value="P">P</option>

													<option <?php if($position=='PR') { echo "selected" ;} ?> value="PR">PR</option>

													<option <?php if($position=='QB') { echo "selected" ;} ?> value="QB">QB</option>

													<option <?php if($position=='RB') { echo "selected" ;} ?> value="RB">RB</option>

													<option <?php if($position=='S') { echo "selected" ;} ?> value="S">S</option>

													<option <?php if($position=='SS') { echo "selected" ;} ?> value="SS">SS</option>

													<option <?php if($position=='T') { echo "selected" ;} ?> value="T">T</option>

													<option <?php if($position=='TE') { echo "selected" ;} ?> value="TE">TE</option>

													<option <?php if($position=='WR') { echo "selected" ;} ?> value="WR">WR</option>

													 <?php } ?>





													<?php if($sportname=='soccer'){ ?>

													<option <?php if($position=='D') { echo "selected" ;} ?> value="D">D</option>

													<option <?php if($position=='F') { echo "selected" ;} ?> value="F">F</option>

													<option <?php if($position=='G') { echo "selected" ;} ?> value="G">G</option>

													<option <?php if($position=='MF') { echo "selected" ;} ?> value="MF">MF</option>

													 <?php } ?>





													<?php if($sportname=='volleyball'){ ?>

													<option <?php if($position=='DS') { echo "selected" ;} ?> value="DS">DS</option>

													<option <?php if($position=='LIBERO') { echo "selected" ;} ?> value="LIBERO">LIBERO</option>

													<option <?php if($position=='LSH') { echo "selected" ;} ?> value="LSH">LSH</option>

													<option <?php if($position=='MB') { echo "selected" ;} ?> value="MB">MB</option>

													<option <?php if($position=='MH') { echo "selected" ;} ?> value="MH">MH</option>

													<option <?php if($position=='OH') { echo "selected" ;} ?> value="OH">OH</option>

													<option <?php if($position=='OPPOSITE') { echo "selected" ;} ?> value="OPPOSITE">OPPOSITE</option>

													<option <?php if($position=='PASSER') { echo "selected" ;} ?> value="PASSER">PASSER</option>

													<option <?php if($position=='RSH') { echo "selected" ;} ?> value="RSH">RSH</option>

													<option <?php if($position=='SETTER') { echo "selected" ;} ?> value="SETTER">SETTER</option>

													 <?php } ?>

	                                            </select>

	                                            <script>$("#sel_position").val("<?php echo $position; ?>");</script>

	                                        </div>

		                                </div>

		                                <div class="form-group col-md-4 playerinfo_paddingright">

                                     	    <div class="form-group" style="margin-bottom:0px;">

	                                         	<label>Team <span class="error">*</span></label>

	                                            <select class="form-control input-sm" id="sel_team" name="sel_team">

	                                            	<option value="">Select</option>

	                                                 <?php

	                                                if ($_SESSION['master']!=1 ) {

	                                                 	$tres = "select * from teams_info where customer_id='$cid' and sport_id='$sportid' $chk_team_id ";

	                                                } else {

														$children = array($_SESSION['childrens']);

														$ids = $_SESSION['loginid'].",".join(',',$children);

														$tres = "select * from teams_info where customer_id in ($ids) and sport_id='$sportid' $chk_team_id";

													}

													// echo $tres;

	                                                 //$selectTeam="select * from teams_info ";

	                                                 // $selectTeam="select * from teams_info where customer_id=:cid and sport_id=:sportid";

													$selectTeam = $tres;

	                                                 $select_teamqry=$conn->prepare($selectTeam);

	                                                 //print_r(array(":cid"=>$cid,":sportid"=>$sportid));exit;

	                                                 //$select_teamqry->execute();

	                                                 $select_teamqry->execute(array(":cid"=>$cid,":sportid"=>$sportid));



	                                                 $get_team_rowCount	= $select_teamqry->rowCount();

													 $getResRow=$select_teamqry->fetchAll(PDO::FETCH_ASSOC);

	                                                 foreach($getResRow as $getTeaminfo){

	                                                 	    $team_id=$getTeaminfo["id"];

	                                                 	    $team_name=$getTeaminfo["team_name"];

	                                                    echo "<option value='".$team_id."'>".$team_name."</option>";



	                                                 }



	                                                 ?>

	                                            </select>

	                                            <script>$("#sel_team").val("<?php echo $team_id_db; ?>");</script>

                                        	</div>
											<label id="sel_team-error" class="error" for="sel_team">Please select team</label>
                                        </div> 
									
		                            </div> 

		                            <div class="col-md-12 col-sm-12 col-xs-12">

			                            <div class="form-group">

		                                    <label>Player Notes or Bio</label>

		                                    <textarea class="form-control input-sm" name="player_note" id="player_note" rows="3"><?php echo $player_note_db;?></textarea> 

		                                </div> 

		                            </div>

		                            <?php if($mode=="Add"){?>

		                            <div class="col-md-12 col-sm-12 col-xs-12">

		                                <div class="form-group">

	                                        <label for="exampleInputFile1">Player Image</label>

	                                        <input type="file" name="player_image" id="player_image" value="" onchange="showimagepreview(this,this.id)">

	                                    </div>

	                                </div>

	                                <div class="col-md-12 col-sm-12 col-xs-12 playerimgcont">

		                                <div class="form-group">

		                                	

	                                        <img  class="imgpreview" alt="Preview Image" width="96" height="96" border="0"  src="images/defaultplayer.png" style="font-size: 12px;color: black;font-weight: bold;">

	                                    </div>

	                                </div>         

	                            

	                            <?php } if($mode=="Edit"){?>

                                    <div class="col-md-12 col-sm-12 col-xs-12">

		                                <div class="form-group">

	                                        <label for="exampleInputFile1">Player Image</label>

	                                        <input type="file" name="player_image" id="player_image" value="" onchange="showimagepreview1(this,this.id)">

	                                    </div>

	                                </div>

	                                <div class="col-md-12 col-sm-12 col-xs-12 playerimgcont2" style="display:none;">

	                                   <div class="form-group">

	                                	 <img  class="imgpreview" src="images/defaultplayer.png" width="96" height="96" border="0" style="font-size: 12px;color: black;font-weight: bold;">

	                                	</div>

	                                </div>

	                                 <div class="col-md-12 col-sm-12 col-xs-12 playerimgcont1">

		                                <div class="form-group">

		                                	<?php

		                                	if($image_db!=""){?>

                                             <img width="96" height="96" src="uploads/players/<?php echo $image_db;?>">

                                           <?php      

		                                	} else {

		                                	?>

	                                        <img width="96" height="96" class="imgpreview" style="font-size: 12px;color: black;font-weight: bold;" src="images/defaultplayer.png" alt="Preview Image" border="0">

	                                        <?php 

	                                        }

	                                        ?>

	                                    </div>

	                                </div>     



	                            <?php }?>

                                </div>

                                <div class="">
								<?php 
									 if($mode=="Edit"){
								
								?>

                                  <button type="submit" class="btn btn-success editsubmitform" style="margin-left:15px;margin-top:20px;">Submit</button>

                              	<?php }else{?>
								 <button type="button" class="btn btn-success addsubmitform" style="margin-left:15px;margin-top:20px;">Submit</button>
								<?php }?>
                                    <button type="button" class="btn red btn-danger" onclick="document.location='player_list.php?sport=<?php echo $sportname; ?>'" style="margin-top:20px;">Cancel</button>

                                </div>


	                           

	                        </div>

	                    </div>

	                </div>

               </form>

            </div>

        </div>

        <!-- END CONTENT BODY -->

    </div>

<div id="myModal" class="modal fade updateplayerform" role="dialog" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" id="btn_close">&times;</button>
						<h4 class="modal-title">Update Player Information</h4>
					  </div>
					  <div class="modal-body" style="text-align:center;">
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-top: 20px;" id="btn_close">Close</button>
					  </div>
					</div>

				  </div>
    <!-- END CONTENT -->                
    </div>
</div>
<!-- END CONTAINER -->
 <script src="assets/custom/js/addplayer.js" type="text/javascript"></script>
<?php include_once('footer.php'); ?>
<script>

  $( function() {

    $( "#dob" ).datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
    });

  } );

</script>

<script>

  function confirmation(id,sportname){

  	var check = confirm('Are you want to sure delete this player?');

    if (check == true) {
    	    window.location="player_list.php?pid="+id+"&sport="+sportname;
            return true;
        }
        else {
            return false;
       }
  }

  function showimagepreview(input,inputid) {

    if (input.files && input.files[0]) {

        var filerdr = new FileReader();

         filerdr.onload = function(e) {

			$('.imgpreview').attr('src', e.target.result);
			$('.imgpreview').css({"width": "96px", "height": "96px"});

		}



		filerdr.readAsDataURL(input.files[0]);

    }

}

function showimagepreview1(input,inputid) {

	$('.playerimgcont1').css("display","none");

	$('.playerimgcont2').css("display","block");

    if (input.files && input.files[0]) {

        var filerdr = new FileReader();

         filerdr.onload = function(e) {

			$('.imgpreview').attr('src', e.target.result);
			$('.imgpreview').css({"width": "96px", "height": "96px"});

		}



		filerdr.readAsDataURL(input.files[0]);

    }

}

</script>


<?php 
error_reporting(0);
include_once('session_check.php'); 
include_once("connect.php");

$TeamManId = '';
$CreatedBy = '';
$searchcondn = '';

if($_SESSION['loginid']!='')  { 
  if($_SESSION['usertype']=='user') {
        $cid		= $_SESSION['loginid'];
		$CreatedBy	= 'customer';
		$DispCondn  = " and created_by='$CreatedBy'" ;
		$VideoCond  = "?id=$cid";
  }
  elseif($_SESSION['signin'] == 'team_manager'){ 
	  $cid			= $_SESSION['loginid'];
	  $TeamManId	= $_SESSION['team_manager_id'];
	  $CreatedBy	= 'team';
	  $DispCondn	= " and (created_by='$CreatedBy' and team_id='$TeamManId') or assign_to like '%#$TeamManId#%'";
	  $VideoCond	= "?id=$cid&tid=$TeamManId";
		// echo "select * from customer_tv_station where team_id='$TeamManId' and customer_id='$cid'";exit;
	  $custtvstationqry = $conn->prepare("select * from customer_tv_station where team_id='$TeamManId' and customer_id='$cid'");	
	  $custtvstationqry->execute();
      $row = $custtvstationqry->fetch(PDO::FETCH_ASSOC);	
	 
      $manage_videos = $row['active'];
	  if($manage_videos == false){
		header("location:index.php");
		exit;
	 }
  }
}else{
	header("location:index.php");
	exit;
}

$InsertStsMsg ='';
$InsertStatus ='';

$alert_message = '';
$alert_class = '';

/*** search  post start here  ****/
$searchname   = $SrchDateFrmt = '';

if(isset($_POST['searchteam'])){
	//Search condition
	$searchname   = $_POST['searchteam'];
	$SrchDateFrmt = $_POST['searchbydate'];
	if(!empty($SrchDateFrmt)){
		$timestamp = strtotime(str_replace('/', '-', "01/".$SrchDateFrmt));
		$searchdate = date('Y-m', $timestamp);
	}
	
	//Get game_id from customer video info table
	$sqlgameqry = "select game_id,id from customer_video_info where customer_id='$cid' AND game_id>0 $DispCondn";
	$sqlgame=$conn->prepare($sqlgameqry);
	$sqlgame->execute();
	$rowcount=$sqlgame->rowCount();
	
	if($rowcount>0) {
		while($fetchgame = $sqlgame->fetch(PDO::FETCH_ASSOC)) {
			$gameid = $fetchgame["game_id"];
			$video_id = $fetchgame["id"];
			$Arrcustomergames[$gameid] = $video_id;
		}
	}

	if(count($Arrcustomergames)>0 && (!empty($searchname) || !empty($searchdate))) {
		//Get team_id from teams_info table for the searched name
		$Arrteamid=array();
		if(!empty($searchname)) {
			$sqlqry = "select id from teams_info where id<>'' AND team_name like '%".$searchname."%' ";
			$sql=$conn->prepare($sqlqry);
			$sql->execute();
			$rowcountsql=$sql->rowCount();
			if($rowcountsql>0) {
				while($fetch =$sql->fetch(PDO::FETCH_ASSOC)) {
					$teamid = $fetch["id"];
					$Arrteamid[] = $teamid;
				}
			}
		}
		if(count($Arrteamid)>0 || !empty($searchdate)) {
			$Arrvideoinfo = array();
			$teamnamecondn = count($Arrteamid)>0 ? " AND (home_team_id in ('".implode("','",$Arrteamid)."') OR visitor_team_id in ('".implode("','",$Arrteamid)."'))" : "";
			$datecondn = !empty($searchdate) ? " AND date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%Y-%m')='".$searchdate."'" : "";

			$sqlqry = "select id from games_info where id in ('".implode("','",array_keys($Arrcustomergames))."') $teamnamecondn $datecondn";
			$sql=$conn->prepare($sqlqry);
			$sql->execute();
			$rowcountsql=$sql->rowCount();
			
			if($rowcountsql>0) {
				while($fetch = $sql->fetch(PDO::FETCH_ASSOC)) {
					$gameid = $fetch["id"];
					$Arrvideoinfo[] = $Arrcustomergames[$gameid];
				}
			}
		}
	}

	//$searchnamecondn = count($Arrvideoinfo)>0 ? "id in ('".implode("','",$Arrvideoinfo)."')" : "";
	$searchbothcondn = '';
	if(count($Arrvideoinfo)>0)
		$Arrsearchcondn[] = "id in ('".implode("','",$Arrvideoinfo)."')";
	
		if(!empty($searchname))
			$Arrsearchcondn[] = "name like '%".$searchname."%'";
		if(!empty($searchdate))
			$Arrsearchcondn[] = "date_format(STR_TO_DATE(date, '%m/%d/%Y'),'%Y-%m')='".$searchdate."'";
			 $searchcondn = count($Arrsearchcondn)>0 ? "AND (".implode(" OR ",$Arrsearchcondn).")" : "";
 }

 /*** search  post end here  ****/
if((isset($_GET['msg'])) && !empty($_GET['msg'])){
	$InsertStatus = $_GET['msg'];
	
	if($InsertStatus==1){
		$InsertStsMsg = "Stream details inserted successfully";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==2){
		$InsertStsMsg = "Stream already exists";
		$AlertSts = 'alert-danger';
	}else if($InsertStatus==3){
		$InsertStsMsg = "Game stream details inserted successfully";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==4){
		$InsertStsMsg = "Game stream already exists";
		$AlertSts = 'alert-danger';
	}else if($InsertStatus==5){
		$InsertStsMsg = "Video status changed to display";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==6){
		$InsertStsMsg = "Video status changed to hide";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==7){
		$InsertStsMsg = "Video uploaded successfully";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==8){
		$InsertStsMsg = "Video stream  uploaded successfully";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==9){
		$InsertStsMsg = "Video deleted successfully";
		$AlertSts = 'alert-success';
	}else if($InsertStatus==10){
		$InsertStsMsg = "Finalize video option updated successfully";
		$AlertSts = 'alert-success';
	}
}

if(isset($_POST['category'])){

	$category=$_POST['category'];
	$date=$_POST['date'];
	$time=$_POST['time'];
	$descp=$_POST['descp'];
	$ename=$_POST['ename'];
	$catres=$conn->prepare("select max(id) as mid from customer_video_info");
	$catres->execute();
	$catrow=$catres->fetch();
	$catid=$catrow['mid'];
	$catid++;
	$url="http://54.201.230.81:8182/api/v1/customer/".$cid."/event/".$catid;	
	$json = file_get_contents($url);
	$data = json_decode($json,true);

	if($data['error'][0])
	{
		echo "Not Allowed";
	}
	else
	{
		$subscriber_url=$data['subscriber'][0]['url'];
		$publisher_url=$data['subscriber'][2]['url'];
	}
	$res=$conn->prepare("select * from customer_video_info where customer_id='$cid' and name='$ename' $DispCondn");
	$res->execute();
	$rescount=$res->rowCount();
	if($rescount>0)
	{
		header("location:manage_videos.php?msg=2");
		exit;
	} 
	else
	{ 
		$insert_results=array(":cid"=>$cid, ":category"=>$category, ":name"=>$ename, ":descp"=>$descp, ":time"=>$time, ":date"=>$date, ":hidden"=>'0', ":publisher_url"=>$publisher_url,":subscriber_url"=>$subscriber_url, ":created_by"=>$CreatedBy, ":team_id"=>$TeamManId);

		$insertqry="insert into customer_video_info(customer_id, category, name, descp, time, date, hidden, publisher_url, subscriber_url, created_by, team_id)values(:cid, :category, :name, :descp, :time, :date, :hidden, :publisher_url, :subscriber_url, :created_by, :team_id)";

		$prepinsertqry=$conn->prepare($insertqry);
		$insertRes=$prepinsertqry->execute($insert_results);
        if($insertRes){
		header("location:manage_videos.php?msg=1");
		exit;
		}

	}
}


if(isset($_POST['game'])){ 

	$gameid=$_POST['game'];
	$customer_id=$cid;
	$catres=$conn->prepare("select max(id) as mid from customer_video_info");
	$catres->execute();
	$catrow=$catres->fetch();
	$catid=$catrow['mid'];
	$catid++;
	$url="http://54.201.230.81:8182/api/v1/customer/".$customer_id."/game/".$catid;	
	$json = file_get_contents($url);

	$data = json_decode($json,true);

	if($data['error'][0])
	{
		echo "Not Allowed";
	}
	else
	{
		$subscriber_url=$data['subscriber'][0]['url'];
		$publisher_url=$data['subscriber'][2]['url'];
	}
	$res=$conn->prepare("select * from customer_video_info where customer_id='$cid' and game_id='$gameid'");
	$res->execute();
	$rescount=$res->rowCount();
	if($rescount>0)
	{
		header("location:manage_videos.php?msg=4");
		exit;
	}
	else
	{
		$insert_results=array(":cid"=>$cid, ":category"=>'game', ":game_id"=>$gameid, ":publisher_url"=>$publisher_url, ":subscriber_url"=>$subscriber_url,":created_by"=>$CreatedBy, ":team_id"=>$TeamManId);
		$insertqry="insert into customer_video_info(customer_id, category, game_id, publisher_url, subscriber_url, created_by, team_id)values(:cid, :category, :game_id, :publisher_url, :subscriber_url,:created_by, :team_id)";

		$prepinsertqry=$conn->prepare($insertqry);
		$insertRes=$prepinsertqry->execute($insert_results);
        if($insertRes){
		   header("location:manage_videos.php?msg=3");
		exit;
		}
	}
}
if(isset($_POST['upload_video_cat']))
{
	include('video_check.php');
	$msg='';

	$id=$_POST['upload_video_cat'];
	$name = $_FILES['file']['name'];
	$size = $_FILES['file']['size'];
	$tmp = $_FILES['file']['tmp_name'];
	$ext = getExtension($name);
	$kb=($size/1024)."kb";
	$kbs=round($kb, 0);
		
	include('s3_config.php');
	$s3filename=time().".".$ext;
	$actual_image_name =$cid."/".$id."/".$s3filename;
	
	//$actual_image_name;
	
	if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ) )
	{
		$s3file='http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
	}
	/*else 
	$msg = "S3 Upload Fail.";	*/	
	
	$customer_id=$cid;
	
	$update_results=array(":s3file"=>$s3file,":hidden"=>'0',":type"=>'manual',":id"=>$id);
	$updateQry="update customer_video_info set file=:s3file,hidden=:hidden, type=:type where id=:id";
	$prepupdateQry=$conn->prepare($updateQry);
	$updateRes=$prepupdateQry->execute($update_results);

	$urlstreaming="http://54.201.230.81:8182/api/v1/customer/".$customer_id."/game/".$id."/s3filekey/".$s3filename."/fsize/".$kbs."kb";
	function curl_s($path)
	{
		$url = $path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $result;
	}
	curl_s($urlstreaming);
	header("location:manage_videos.php?msg=7");
	exit;
} 

$url="http://54.201.230.81:8182/api/v1/customer/".$cid."/recordings";
$json = file_get_contents($url);
$VideoListData = json_decode($json,true);

$Inc=0;
if($VideoListData['list'][$Inc]=='')
{
//	echo "No video to download";
}else{	
	$VideoResArr = array();
	if (is_array($VideoListData['list'])) {
	foreach ($VideoListData['list'] as $data) {
	  $EventId = $data['event_id'];
	  if (isset($VideoResArr[$EventId])) {
		 $VideoResArr[$EventId][] = $data;
	  } else {
		 $VideoResArr[$EventId] = array($data);
	  }
	}
	}
}
include_once('header.php');
?>

<!-- MANAGE VIDEOS SCRIPTS-->
<script src="assets/custom/js/jquery.maskedinput.js" type="text/javascript"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="assets/custom/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="assets/custom/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="assets/custom/js/customscript.js"></script>


<link href="assets/custom/css/managevideos.css" rel="stylesheet" type="text/css" />
<script src="assets/custom/js/jscolor.js"></script>
 <script src="assets/custom/js/jquery.form-validator.min.js"></script>
<script src="assets/custom/js/jquery.form.min.js"></script>

<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
 <div class="page-content" style="<!-- background: #EFEFEF url(http://demo.shenll.net/massage/img/bg.png) repeat 0 0; -->">
	
	 <?php if(!empty($InsertStsMsg)){ ?>
            <div class="alert alert-block fade in <?php echo $AlertSts; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $InsertStsMsg; ?> </p>
            </div>
            <?php } ?> 
            <!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="row searchheder">
              <div class="col-md-7 searchbarstyle searchbarstyle1" >
				<div class="">
					<div class="col-md-2 col-sm-2 col-xs-12 ">
                        <div class="form-group">
                           <button type="button" class="btn addcustomerbtn  searchteambtn" >Search <!-- <i class="glyphicon glyphicon-search"></i> --></button>
                        </div>
                    </div>
					<div class="col-md-3 col-sm-2 col-xs-12 ">
                        <div class="form-group">
                           <button type="button" class="btn addcustomerbtn  createstreambtn" >Create Stream <!-- <i class="glyphicon glyphicon-search"></i> --></button>
                        </div>
                    </div>
					<div class="col-md-3 col-sm-2 col-xs-12 ">
                        <div class="form-group">
                           <button type="button" class="btn addcustomerbtn  uploadvideoicon" >Upload Videos <!-- <i class="glyphicon glyphicon-search"></i> --></button>
                        </div>
                    </div>
					<div class="col-md-2 col-sm-2 col-xs-12 ">
                        <div class="form-group">
							<?php
							$custqry = $conn->prepare("select customer_id from customer_tv_station where customer_id='$_SESSION[loginid]'");
								 
							$custqry->execute();
							$QryCntTeams = $custqry->rowCount(); 
							if($QryCntTeams>0){
							$get_customers = $custqry->fetch();
							$tv=1;
							echo '<button type="button" class="btn addcustomerbtn  tvstationcodeicon" >Get TV Station Code <!-- <i class="glyphicon glyphicon-blackboard"></i> --></button>';
							}
							?>
                        
                        </div>
                    </div>
					</div>
                </div>
            </div>

		<!-- Search toggle container start here -->
		<?php 
			if(($searchname!='') || ($SrchDateFrmt!=''))
			{ 
					$DisToggle = "display:block;";
			}else{
					$DisToggle = "display:none;";
			}
		?>
			<div class="searchinnercont togglecontainer searchtogglebar" style="<?php echo $DisToggle;?>">
			<!-- <div class="closetogglesrchbtn">
			</div> -->
			<div class="col-md-12 col-sm-12 col-xs-12 toggleinnrewrap" >			
						<div class="form-body top-padding">
								<div class="row">
									<div class="col-md-12 e_n_f_top_search_pad">
										<form method="post" class="searchform" action="">
											<div class="col-md-2 stream_create font-red-sunglo">SEARCH
											</div>
											<div class="col-md-4 e_n_f_search_btn" >
												<input type="text" class="form-control teamnamesearch border-radius" name="searchteam" placeholder="Enter team name or event" value="<?php echo $searchname;?>">
											</div>
											<div class="col-md-3 e_n_f_search_btn">
												<input type="text" class="form-control searchdatepicker border-radius" name="searchbydate" placeholder="MM/YYYY" id="searchdatepicker"  value="<?php echo $SrchDateFrmt;?>">
											</div>
											<div class="col-md-3 e_n_f_search">
												<button type="button" class="btn  searchtogglebtn_n_s" id="searchintble" style="padding: 6px 12px; margin-left: 12%;">
												  <span class="glyphicon glyphicon-search"></span> Search
												</button> &nbsp;<button type="button" class="btn  searchtogglebtn_red_n_c resetsearch"  style="padding: 6px 12px; float: right;width: 85px;padding-left: 5px;">
												  <span class="glyphicon glyphicon-close"></span> Reset
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				<!-- Search toggle container end here -->	


              	<!-- create stream toggle container start here -->
				<div class="streaminnercont togglecontainer createstreamtoggle" style="display:none;"> 
						<div class="col-md-12 col-sm-12 col-xs-12 toggleinnrewrap">
							<div class="form-body top-padding">
									<div class="row">
									<input type="hidden" name="category" value="event">
										<div class="col-md-12 col-md-8">
											<div class="col-md-2 pad_set">
												<p class="stream_create font-red-sunglo now_e_n_f_create">CREATE STREAM</p>
											</div>
											<div class="col-md-4 pad_none">
												<!-- <p> -->
												<select onchange="category(this.value);" class="form-control input-sm">
													<option value="">Choose Category</option>
													<option value="game">Game</option>
													<option value="event">Event</option>
													<option value="show">Show</option>
													<option value="ceremony">Ceremony</option>
													<option value="meeting">Meeting</option>
													<option value="other">Other</option>
												</select>
												<!-- </p> -->
											</div>
										<div id="category"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- create stream toggle container end here -->
                     <!-- Upload video toggle container start here -->
					<div class="uploadinnercont togglecontainer createstreamtoggle" style="display:none;">
						<div class="col-md-12 col-sm-12 col-xs-12 toggleinnrewrap">
								<div class="form-body top-padding">
									<div class="row">
										<div class="col-md-12 col-md-8">
											<div class="col-md-2 pad_set">
												<p class="stream_create font-red-sunglo now_e_n_f_create">UPLOAD VIDEO</p>
											</div>
											<div class="col-md-3 pad_none">
												<!-- <p> -->
												<select onchange="upload_video_category(this.value);" class="form-control input-sm">
													<option value="">Choose Category</option>
													<option value="game">Game</option>
													<option value="event">Event</option>
													<option value="show">Show</option>
													<option value="ceremony">Ceremony</option>
													<option value="meeting">Meeting</option>
													<option value="other">Other</option>
												</select>
												<!-- </p> -->
												<div style="display: none;" id="progress_div">
													<style type="text/css">.progressbar{background-color: #80B145;padding: 8px;border-radius:5px !important;}</style>
													<div class="progress">
														<div class="percent" id="progresspercent">0%</div >
														<div class="bar progressbar"></div >
													</div>
													<div id="status"></div>
												</div>
											</div>
											<div id="upload_video_cat"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<!-- Upload video toggle container end here -->
					<!-- Get TV station toggle container start here -->
					<?php
						if($tv==1){	?>
							<div class="tvstationinnercont togglecontainer createstreamtoggle" style="display:none;">
								<div class="col-md-12 col-sm-12 col-xs-12 toggleinnrewrap e_n_f_get_tv_station">
											<div class="form-body top-padding">
												<div class="row">
													<div class=" col-md-offset-1 col-md-10 text-center">
														<!-- <p class="stream_create1">GET TV STATION CODE</p> -->
													</div>
												</div>
												<div class="row">
														<div class=" col-md-12 embedded_wrap" style="display:block !important;clear: both;"><pre class="snippet"><button class="btncopy" data-clipboard-snippet="">Copy to clipboard</button><pre><code class="php">&lt;iframe src="http://805stats.com/myvideo/videoPlayer.php<?php echo $VideoCond; ?>"  style="width: 980px; height:  540px; max-width:100%;" allowfullscreen="true" allowScriptAccess="always"&gt;&lt;/iframe&gt;//Change height and width according to your site&gt;</code></pre></div>	
												</div>
										</div>
									</div>
								</div>
					<?php } ?>
					<!-- Get TV station toggle container end here --> 
					<!--Toogle-->
				<div class="portlet-body customerlist-tbl-pr clearfix" style="clear: both;">
					<div class="widget-header">	
						<h3>
							<i class="icon-settings font-red-sunglo"></i>
							Manage Videos						
						</h3>
				</div>
				<?php 
					if($TeamManId!=''){
						$tblHead = '<th style="border-width: 1px 0px 0px 0px;">&nbsp;&nbsp;</th>';		
					}else{
						$tblHead = '<th>ASSIGN </th>';
					}
					$res="select * from customer_video_info where customer_id='$cid' $DispCondn $searchcondn";
					$gres = $conn->prepare($res);
					$gres->execute();
					$RowCont=$gres->rowCount();
					
				?>	
		<table class="table table-striped table-hover customerlist-tbl" id="sample_1">
			<thead>
				<tr class="e_n_new_tr_class">
					<th style="text-align:left;padding-left:30px !important;"> EVENT </th>
					<th style="text-align:left;padding-left:30px !important;"> DATE & TIME</th>
					<?php echo $tblHead;?>
					<th > ACTION </th>
				</tr>
			</thead>
			 <tbody>
				<?php $i=0;
				  if($RowCont>0){
					while($row=$gres->fetch(PDO::FETCH_ASSOC)){
					
						$AssignedArr		= array();
						$AssignedArr		= array_filter(explode("#",$row['assign_to']));		
							
						$AssigneByCust  = (count($AssignedArr) >0 && in_array($TeamManId,$AssignedArr))?true:false;
						//echo $AssigneByCust ."test";
						//print_r($row);
						//print_r($VideoResArr); exit;
						//echo $row['id'].'<br>';
						$VideoCnt  = isset($VideoResArr[$row['id']])?count($VideoResArr[$row['id']]):0; 		
						$VideoId   = $row['id'];

						$FinalizeCss =$BorderCss = $BorderCss2 = '';
						if($row['final']!=1 && $VideoCnt>1){
							$FinalizeCss  = "padding-left:25px;";
							$BorderCss    = "border-right:none;";
							$BorderCss2   = "border-right: 1px solid grey;";
						}else{
							$BorderCss    = "border-right: 1px solid grey;";
							$BorderCss2   = "border-right:none;";
						}

						$RowCnt = 0; 	
						$StreamHtml = '';

						$sul=explode("/",$row['subscriber_url']);
						if($sul[5]!='') {
						
							$StreamHtml ="<tr><td style='vertical-align: middle;'>Stream Name</td><td><div class='embedded_wrap'><pre class='snippet'><button class='btncopy' data-clipboard-snippet=''>Copy to clipboard</button><pre><code class='php' style='padding: 10px 0px 0px 0px;'>$sul[5]</code></pre></div></td></tr>";
						}
						
						$EmbedUrl    = "rtmp://54.201.230.81:1935/live/";
						$PublishUrl  = urldecode($row['publisher_url']);

						$IframeHtmlWrap = "<tr class='encoder_wrap_row'>
								<td colspan='4' style='display:none;height:100px;' data-id='embedded_$i' class='toggledropcont'>
								<div class='embedded_wrap'><pre class='snippet'><button class='btncopy' data-clipboard-snippet=''>Copy to clipboard</button><pre><code class='php'>&lt;iframe src='http://805stats.com/demolive/Player/?jStream=$PublishUrl' name='player' id='player' style='width: 1100px; height:  619px; max-width:100%;' allowfullscreen='true' allowScriptAccess='always' &gt;&lt;/iframe&gt;</code></pre></div>
								</td></tr>
								<tr class='encoder_wrap_row'><td colspan='4' style='display:none;height:100px;' data-id='encoder_$i' class='toggledropcont encoder_wrap'><table class=table' style='width: 50%;float: right;'>$StreamHtml<tr><td style='vertical-align: middle;'>Server</td><td><div class='embedded_wrap'><pre class='snippet'><button class='btncopy' data-clipboard-snippet=''>Copy to clipboard</button><pre><code class='php' style='padding: 10px 0px 0px 0px;'>$EmbedUrl</code></pre></div></td></tr></table></td></tr>";

						
						$VideoContHtml  = ''; 
						$AssignTeam  ='';
					if(isset($VideoResArr[$VideoId])){
						foreach($VideoResArr[$VideoId] as $VideoRes)
						{						
							$eid		  = $VideoRes['event_id'];
							$thumbnail	  = $VideoRes['thumbnail'];
							$rtmp		  = $VideoRes['RTMP'];
							$download_url = $VideoRes['download_url'];
							$Record_Id    = $VideoRes['recording_id'];			
							$startDate    = date("M jS, Y h:i A",substr($VideoRes['start'],0,-3));

							$vdrowqry = $conn->prepare("select * from customer_video_info where id='$eid' $DispCondn");	
							$vdrowqry->execute();
							$vdrow=$vdrowqry->fetch();

							$FileName	  = $vdrow['file'];	

							$startDateHtml = '<p class="text-center" style="font-size: 14px;margin-bottom: 0px;line-height: 15px;    margin-left: -12px;">'.$startDate.'</p>';			    

							if($vdrow['category']=='game')
							{	$growqry=$conn->prepare("select * from games_info where id='$vdrow[game_id]'");
								$growqry->execute();
								$grow=$growqry->fetch();
								$date		=	$grow['date'];
								$old_date_timestamp = strtotime($date);
								$new_date	= date('l F jS, Y', $old_date_timestamp);
								$visrowqry=$conn->prepare("select team_name from teams_info where id='$grow[visitor_team_id]'");

								$visrowqry->execute();
								$visrow=$visrowqry->fetch();
								$homerowqry=$conn->prepare("select team_name from teams_info where id='$grow[home_team_id]'");
								$homerowqry->execute();
								$homerow=$homerowqry->fetch();
								

								$name		= $visrow['team_name']." vs ".$homerow['team_name'];
								$time		= $grow['time'];
											
							}
							if($vdrow['category']!='game') 
							{
								$date		=$vdrow['date'];
								$old_date_timestamp = strtotime($date); 
								$new_date	= date('l F jS, Y', $old_date_timestamp);
								$name		= $vdrow['name'];
								$time		= $vdrow['time'];
							}
							
					/****** Video List HTML Appended in $VideoContHtml ( Start) *****/
							$ThumImgHtml = '';
							if($row['type']!="manual"){ 
								$ThumImgHtml = "<img src='$thumbnail' class='left' style='object-fit: cover; margin-top: 20px; position: absolute;'/>";
							}

							if($RowCnt==3) {
								$VideoContHtml .= '<div class="row"><div class="col-md-12">';
								$RowCnt=0;
							}
							
							if(!$AssigneByCust){
								$DeleteBtns  ="<input class='deletevideobtn ' type='button' value='Delete &nbsp;&nbsp;&nbsp;&nbsp;' onclick=\"delete_live_videos($VideoId,'$download_url','$Record_Id','$download_url');\" />";
							}

							$VideoContHtml .= "<div class='col-md-4 col-sm-12 col-xs-12 dwnldvideolistwrap e_n_f_video_play'>
								<div class='mainvideowrap'>
									<div class='col-md-7'>	
										$ThumImgHtml
										<div class='overlay' style='padding-top: 15px;'>
											<a class='fancybox' data-fancybox-type='iframe' href='videoplayback.php?id=$rtmp'>
												<img src='assets/custom/css/videos_page/play.png' class='e_f_play_img_final'/>
											</a>
										</div>
									</div>
									<div class='col-md-5'>
										<div class='text-left'>	
											$startDateHtml
											<a href='$download_url' class='down_urllink'><input class='downloadvideobtn videos_btns' type='button' value='Download' style='margin-top: 10px;'/></a><br/>$DeleteBtns							
										</div>
									</div>
									<div style='clear:both;'></div>
								</div>
							</div>";		
												
							if($RowCnt==3) {
								$VideoContHtml .= '</div></div>';
							}
							$RowCnt++;
						}
					}
					if($row['category']=='game')
					{ 
						$gameid=$row['game_id'];
						$game=1;
						$growqry = $conn->prepare("select * from games_info where id='$gameid'");
						
						$growqry->execute();
						$grow=$growqry->fetch();
						$date=$grow['date'];
						$old_date_timestamp = strtotime($date);
						$new_date = date('l F jS, Y', $old_date_timestamp);
						$visrowqry = $conn->prepare("select team_name from teams_info where id='$grow[visitor_team_id]'");
						$visrowqry->execute();
						$visrow=$visrowqry->fetch();
						$homerowqry = $conn->prepare("select team_name from teams_info where id='$grow[home_team_id]'");
						$homerowqry->execute();
						$homerow=$homerowqry->fetch();
						$HomeTeamId		= $grow['home_team_id'];
						$VisitTeamId	= $grow['visitor_team_id'];

						$FinalizeCss='';
						if($row['final']!=1 && $VideoCnt>1){
							// $FinalizeCss = "padding-left:46px;";
							$FinalizeCss = "padding-left:25px;";
						}
						
						$assignto  = $row["assign_to"];
						$AssignArr = array_filter(explode('#',$assignto));
						
						if(isset($AssignArr[1])){
							if($grow['home_team_id']==$AssignArr[1]){
								$AssignClass1 = 'deassignteambtn';
								$Assigned1  ='Deassign';
								$ToolTipMsg1 = 'Click here to deassign';
							}else{	
								
								$AssignClass1 = 'assignteambtn';
								$Assigned1  ='Assign';
								$ToolTipMsg1 = 'Click here to assign';
							}
						}else{
								$AssignClass1 = 'assignteambtn';
								$Assigned1  ='Assign';
								$ToolTipMsg1 = 'Click here to assign';
						}
						
						if(isset($AssignArr[2])){
							
							if($grow['visitor_team_id']==$AssignArr[2]){
								$AssignClass2 = 'deassignteambtn';
								$Assigned2	  ='Deassign';
								$ToolTipMsg2 = 'Click here to deassign';
							}else{			
								$AssignClass2 = 'assignteambtn';
								$Assigned2  ='Assign';
								$ToolTipMsg2 = 'Click here to assign';
							}
						}else{
							$AssignClass2 = 'assignteambtn';
							$Assigned2  ='Assign';
							$ToolTipMsg2 = 'Click here to assign';							
						}
						 if($TeamManId==''){
								$AssignTeam  = '<td style="padding-top: 8px !important;padding-left:15px !important;"><a href="javascript:void(0);" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="click here to assign or deassign stream"><div class="assigntobtn" data-toggle="modal" data-target="#myModal_'.$VideoId.'">Assign </div></a>
								<div id="myModal_'.$VideoId.'" class="modal fade assignvideomodel" role="dialog" data-backdrop="static" data-keyboard="false">
								  <div class="modal-dialog">
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">ASSIGN THIS STREAM TO TEAM</h4>
									  </div>
									  <div class="modal-body">
										<div class="statusmsg"></div>
										<table class="table" data-gameid="'.$VideoId.'"><tr data-teamid='.$grow['home_team_id'].' data-team="1"><td><p class="e_n_f_good_buys">'.$homerow['team_name'].'</p></td><td class="e_n_f_deass_is"><a href="javascript:void(0);" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="'.$ToolTipMsg1.'"><span class=" '.$AssignClass1.'">'.$Assigned1.'</span></a><!-- <span class="deassignteambtn">Deassign</span> --></td></tr>
										<tr data-teamid='.$grow['visitor_team_id'].' data-team="2"><td><p class="e_n_f_good_buys">'.$visrow['team_name'].'</p></td><td class="e_n_f_deass_is"><a href="javascript:void(0);" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="'.$ToolTipMsg2.'"><span class="'.$AssignClass2.'">'.$Assigned2.'</span></a><!-- <span class="deassignteambtn">Deassign</span> --></td></tr>
										</table>
									  </div>
									  <div class="modal-footer">
										<button type="button" class="btn e_n_f_save_popup_btn" data-dismiss="modal">Close</button>
									  </div>
									</div>
								  </div>
								</div></td>';
							} 
							if($TeamManId!=''){
								$AssigbyCust =($AssigneByCust)?'<a href="javascript:void(0);" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="This stream assign by customer"><div class="assigntobtn">Assigned by Customer</div></a>':'';					
								$AssignTeam="<td>$AssigbyCust</td>";
							}
					?>
					<tr class="manageiconswrap sorttablerow">
							
						<td nowrap style="padding-top: 13px !important;"><?php if($row['type']=="manual"){ ?><a href="javascript:void(0);" data-toggle="tooltip" title="Manually uploaded video" data-placement="bottom"><img src="assets/custom/css/videos_page/manual-upload.png"  alt="hide" style="width: 40px;"></a> <?php } ?><?php echo $visrow['team_name'];?> vs <?php echo $homerow['team_name']; ?></td>
						<td class="hidden-xs" style="padding-top: 13px !important;"><?php echo $new_date; ?>, <?php echo $grow['time']; ?></td>
						<?php echo $AssignTeam;?>
						<?php //if($row['type']=="manual"){$finalize_class='finalize_manual';}else{$finalize_class='finalize_class';}?>
						<?php
						
						if($row['final']!=1 && $VideoCnt>1 && $AssigneByCust===false){$finalize_class='margin-left: 25px !important;padding-left: 100px !important;padding-right: 0 !important;';}else{$finalize_class='text-align:center;margin-left: 15px !important;padding-right: 0px !important;';}
						?>
						<td class="actioncont e_n_f_new_sy" style="<?php echo $finalize_class;?>">

							<?php if($AssigneByCust===false){					
							 if($row['hidden']==1) { ?>
								<a href="javascript:void(0);" data-toggle="tooltip" title="Show this video" data-placement="bottom" class="actioncontlink" style="<?php echo $FinalizeCss;?>"><img src="assets/custom/css/videos_page/view-disable.png"  onclick="disp_video(<?php echo $row['id'] ?>,'show');" alt="hide"></a>
							<?php }else{ ?>
								<a href="javascript:void(0);" data-toggle="tooltip" title="Hide this video" data-placement="bottom" class="actioncontlink" style="<?php echo $FinalizeCss;?>"><img src="assets/custom/css/videos_page/view-enable.png"  onclick="disp_video(<?php echo $row['id'] ?>,'hide');" alt="hide"></a>
							<?php }
							
							}
								
							if($VideoCnt>0){
								$DoDelCheck = "donotdelete"; 
								$Opacity    = "opacity: 0.6;";
							}else{
								$DoDelCheck = $row['id'];
								$Opacity    = "";
							}
							if($AssigneByCust===true){
								$BorderCss2 ='border-right:0px solid grey;';
							}
							if($AssigneByCust===false){	?>
							<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" data-placement="bottom" class="actioncontlink deletestreamlink" style="<?php echo $Opacity;?>" ><img src="assets/custom/css/videos_page/delete.png" onclick="deletee('<?php echo $DoDelCheck; ?>');" alt="Delete"></a>			<?php } ?>
							
							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Videos" data-placement="bottom" class="actioncontlink" style="padding-right:8px;"><img src="assets/custom/css/videos_page/videos.png" class="videoslisticon <?php if($VideoCnt>0){ ?>slidetogglemain <?php } ?>"  data-name='videoslist' data-row='<?php echo $i;?>' style="padding-right: 10px;padding-left: 2px; <?php if($VideoCnt==0){ echo "opacity: 0.6;";}?>" alt="videoslist"><sup style="font-size:13px;margin-left: -12px"><?php echo $VideoCnt;?></sup></a>

							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Encoder Code" data-placement="top" class="actioncontlink"><img src="assets/custom/css/videos_page/encoder.png" class="getencodeurlicon slidetogglemain" data-name='encoder' data-row='<?php echo $i;?>' style="padding-left: 2px;"></a>
							
							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Embedded Code" data-placement="top" class="actioncontlink"><img src="assets/custom/css/videos_page/embedded.png" class="getembeddedicon slidetogglemain" data-name='embedded' data-row='<?php echo $i;?>' style="<?php echo $BorderCss2;?>"></a>
							
							<?php if($AssigneByCust===false){ ?>					
							<?php if($row['final']!=1 && $VideoCnt>1){ ?>					
							<a href="javascript:void(0);" data-toggle="tooltip" title="Finalize" data-placement="bottom" class="actioncontlink"><img src="assets/custom/css/videos_page/finalize.png" class="" onclick="final(<?php echo $cid; ?>,<?php echo $row['id']; ?>);" style="<?php echo $BorderCss;?>"></a>
							<?php } 					
							}
							?>
							
						</td>
						
				</tr>	
			    <?php echo $IframeHtmlWrap;?>		
			
				<tr class="encoder_wrap_row">		
					<td colspan="4" style='display:none;height:100px;' data-id='videoslist_<?php echo $i;?>' class='toggledropcont encoder_wrap'>
						<div class="col-md-12">
								<?php echo $VideoContHtml;?>	
						</div>
					</td>				
				</tr> 
				<?php	}
						else{
								$date=$row['date'];
								$old_date_timestamp = strtotime($date);
								$new_date = date('l F jS, Y', $old_date_timestamp); 
								if($row['type']=="manual"){
									$FinalizeCss = "";
									$BorderCss= $BorderCss2= "border-right:none;";
								}
								if($VideoCnt>0){
									$DoDelCheck = "donotdelete"; 
									$Opacity    = "opacity: 0.6;";
								}else{
									$DoDelCheck = $row['id'];
									$Opacity    = "";
								}
								if($AssigneByCust===true){
									$BorderCss2 ='border-right:0px solid grey;';
							}
				?>
				<tr class="manageiconswrap sorttablerow">
						<td style="padding-top: 13px !important;"><?php if($row['type']=="manual"){ ?><a href="javascript:void(0);" data-toggle="tooltip" title="Manually uploaded video" data-placement="bottom"><img src="assets/custom/css/videos_page/manual-upload.png"  alt="hide" style="width: 40px;"></a> <?php }  echo $row['name'];?> </td>

						<td class="hidden-xs" style="padding-top: 13px !important;"><?php echo $new_date; ?>, <?php echo $row['time']; ?></td>
						<?php echo "<td>".$AssignTeam.'</td>';?>
						<?php if($row['final']!=1 && $VideoCnt>1 && $AssigneByCust===false){$finalize_class='margin-left: 25px !important;padding-left: 100px !important;padding-right: 0 !important;';}else{$finalize_class='text-align:center;margin-left: 15px !important;padding-right: 0px !important;';}?>
						<td class="actioncont e_n_f_new_sy" style="<?php echo $finalize_class;?>">

							<?php if($row['hidden']==1) { ?>
								<a href="javascript:void(0);" data-toggle="tooltip" title="Show this video" data-placement="bottom" class="actioncontlink" style="<?php echo $FinalizeCss;?>"><img src="assets/custom/css/videos_page/view-disable.png"  onclick="disp_video(<?php echo $row['id'] ?>,'show');" alt="hide"></a>
							<?php }else{ ?>
								<a href="javascript:void(0);" data-toggle="tooltip" title="Hide this video" data-placement="bottom" class="actioncontlink" style="<?php echo $FinalizeCss;?>"><img src="assets/custom/css/videos_page/view-enable.png"  onclick="disp_video(<?php echo $row['id'] ?>,'hide');" alt="hide"></a>
							<?php }?>
							
							<?php if($AssigneByCust===false){	?>
							<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" data-placement="bottom" class="actioncontlink" style="<?php echo $Opacity;?>" ><img src="assets/custom/css/videos_page/delete.png" onclick="deletee('<?php echo $DoDelCheck; ?>');" alt="Delete"></a>			
							<?php } ?>


							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Videos" data-placement="bottom" class="actioncontlink" style="padding-right:8px;"><img src="assets/custom/css/videos_page/videos.png" class="videoslisticon <?php if($VideoCnt>0){ ?>slidetogglemain <?php } ?>"  data-name='videoslist' data-row='<?php echo $i;?>' style="padding-right: 10px;padding-left: 2px; <?php if($VideoCnt==0){ echo "opacity: 0.6;";}?>" alt="videoslist"><sup style="font-size:13px;margin-left: -12px"><?php echo $VideoCnt;?></sup></a>

							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Encoder Code" data-placement="top" class="actioncontlink"><img src="assets/custom/css/videos_page/encoder.png" class="getencodeurlicon slidetogglemain" data-name='encoder' data-row='<?php echo $i;?>' style="padding-left: 2px;"></a>

							<a href="javascript:void(0);" data-toggle="tooltip" title="Show Embedded Code" data-placement="top" class="actioncontlink"><img src="assets/custom/css/videos_page/embedded.png" class="getembeddedicon slidetogglemain" data-name='embedded' data-row='<?php echo $i;?>' style="<?php echo $BorderCss2;?>"></a>			
							<?php if($AssigneByCust===false){ ?>					
							<?php if($row['final']!=1 && $VideoCnt>1){ ?>					
							<a href="javascript:void(0);" data-toggle="tooltip" title="Finalize" data-placement="bottom" class="actioncontlink e_n_f_bor_left_final"><img src="assets/custom/css/videos_page/finalize.png" class="" onclick="final(<?php echo $cid; ?>,<?php echo $row['id']; ?>);" style="<?php echo $BorderCss;?>"></a>
							<?php } 					
							}
							?>
						</td>
				</tr>	
				<?php echo $IframeHtmlWrap;?>
						<tr class="encoder_wrap_row e_n_f_bg_color_set">		
							<td colspan="4" style='display:none;height:100px;' data-id='videoslist_<?php echo $i;?>' class='toggledropcont encoder_wrap'>
								<div class="col-md-12">
										<?php echo $VideoContHtml;?>		
								</div>
							</td>			
						</tr>
					<?php 
						}
							$i++;
							 }
						}else{
							 echo "<tr><td  class='text-center' colspan='4'> No record(s) found</td></tr>";
					}?>
             </tbody>
		 </table>
							
                        </div>
                    </div>
					
                    <!-- END EXAMPLE TABLE PORTLET-->                
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php include_once('footer.php'); ?>

<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="assets/custom/js/eventform.js" type="text/javascript" ></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- <script src="assets/global/scripts/datatable.js" type="text/javascript"></script> -->
<!-- <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- <script src="assets/pages/scripts/table-datatables-colreorder.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL SCRIPTS -->

<!-- <link href="assets/custom/css/customerlist.css" rel="stylesheet" type="text/css" />
 --><script src="assets/custom/js/highlight.pack.min.js"></script>
<script src="assets/custom/js/clipboard.min.js"></script>
<script src="assets/custom/js/demos.js"></script>
<script src="assets/custom/js/snippets.js"></script>
<script src="assets/custom/js/tooltips.js"></script>
<link href="assets/custom/css/videos_page/github.css" rel="stylesheet" type="text/css">
<link href="assets/custom/css/videos_page/main.css" rel="stylesheet" type="text/css">

<script>
 
$("button[data-toggle='model']").hover(
  function () {
    $(this).addClass("assigntobtn");
  },
  function () {
   $(this).addClass("assigntobtn");
  }
);

$(document).ready(function(){
	$(document).on("click",".assignteambtn,.deassignteambtn",function(){
		var StreamId  = $(this).closest("table").attr("data-gameid");		
		var TeamId    = $(this).closest("tr").attr("data-teamid");		
		var TeamCnt	  = $(this).closest("tr").attr("data-team");	
		var AssignSts  ='';
		var $thisBtn   = $(this);
		
		if($thisBtn.hasClass('assignteambtn')){
			AssignSts ='assigntoteam';
		}else{
			AssignSts ='deassigntoteam';
		}

		$.ajax({
			data:{streamid:StreamId,teamid:TeamId,teamcnt:TeamCnt,assignsts:AssignSts},
			method:'POST',
			url:'assignstream.php',
			success:function(data){
				var StatusArr = data.split('###');				
				if(StatusArr[1]=='success'){
					var UpdatedMsg = '';
					if(StatusArr[0]=='assigned'){
						UpdatedMsg = 'Stream assigned to team successfully';
						$thisBtn.text('Dessign');
						$thisBtn.addClass('deassignteambtn').removeClass('assignteambtn');
					}else{
						UpdatedMsg = 'Stream deassigned to team successfully';
						$thisBtn.text('Assign');
						$thisBtn.addClass('assignteambtn').removeClass('deassignteambtn');
					}

					$('.statusmsg').empty().append('<div class="col-md-8" style="margin:auto;float:none;"><div style="margin-top:0px;margin-bottom:10px;" class="alert alert-success  fade in redirect custom_alert"><a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">X</a><strong>'+UpdatedMsg+'</strong></div></div>');
				}else{
					console.log(data);
					
				}
			}
		});
	});

	$(document).on("click",'.assigntobtn',function(){
			$('.statusmsg').empty();

	});
});
</script>
